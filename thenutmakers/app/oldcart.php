<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OldCart extends Model
{
    public $products=null;
    public $quantity;
    public $total_amount;

    public function __construct($oldCart){
      if($oldCart!=null){
        $this->products = $oldCart->products;
        $this->quantity = $oldCart->quantity;
        $this->total_amount = $oldCart->total_amount;
      }
    }

    public function checkStock($available_quantity,$given_quantity,$quantity_in_cart=0){
      if($available_quantity < ($given_quantity+$quantity_in_cart)){
        return false;
      }else{
        return true;
      }
    }

    public function checkStockRemove($available_quantity,$final_quantity,$quantity_in_cart=0){
      if($available_quantity < $final_quantity){
        return false;
      }else{
        return true;
      }
    }

    public function addToCart($item,$id,$quantity=1)
    {
      $key=$id;
      var_dump($key);
      exit;
      $cartItem=['quantity' => $quantity,'price' => $quantity * ($item->price), 'id' => $id];
      if($this->products!=null){
        
          if(array_key_exists($key,$this->products)){
            if(!$this->checkStock($item->stock,$cartItem['quantity'],$this->products[$key]['quantity'])){
              return false;
            }
            $this->products[$key]['quantity'] += $cartItem['quantity'];
            $this->products[$key]['price'] += $cartItem['price'] ;
          }else{
            if(!$this->checkStock($item->stock,$cartItem['quantity'])){
              return false;
            }
            $this->products[$key]['quantity'] = $cartItem['quantity'];
            $this->products[$key]['price'] = $cartItem['price'] ;
            
          }
        
      }else{
        if(!$this->checkStock($item->stock,$cartItem['quantity'])){
          return false;
        }
        
          $this->products[$key]['quantity'] = $cartItem['quantity'];
          $this->products[$key]['price'] = $cartItem['price'] ;     
      }
      
        $this->products[$key]['id'] = $id ;
        $this->products[$key]['name'] = $item->product_name;
        $this->products[$key]['image'] = $item->preview_image;
        
      
      return true;
    }

    public function removeCart($item,$id,$quantity)
    {
      if($quantity>0){
        $cartItem=['quantity' => $quantity,'originalprice' => $this->products[$id]['price'] / $this->products[$id]['quantity'],'price' => $quantity *  ($this->products[$id]['price'] / $this->products[$id]['quantity']), 'id' => $id];
        if($this->products!=null){
            $itemquantity=$this->products[$id]['quantity'];
            $itemprice=$this->products[$id]['price'];
            if(array_key_exists($id,$this->products)){
              if(!$this->checkStockRemove($item->stock,$cartItem['quantity'],$this->products[$id]['quantity'])){
                return false;
              }
              $this->products[$id]['quantity'] = $cartItem['quantity'];
              $this->products[$id]['price'] = $cartItem['price'] ;
            }else{
              return false;
            }
            // if($itemquantity < $quantity){
            //   $quantitytoadd = $quantity - $itemquantity;
            //   $pricetoadd = ($cartItem['originalprice'] * $quantitytoadd)+(($cartItem['originalprice'] * $quantitytoadd)*($item->igst/100));
            //   $this->total_amount += $pricetoadd;
            //   $this->quantity += $quantitytoadd;
            // }else if($itemquantity > $quantity){
            //   $quantitytosub = $itemquantity - $quantity;
            //   $pricetosub = ($cartItem['originalprice'] * $quantitytoadd)+(($cartItem['originalprice'] * $quantitytoadd)*($item->igst/100));
            //   $this->total_amount -= $pricetosub;
            //   $this->quantity -= $quantitytosub;
            // }
        }
      }else{

        unset($this->products[$id]);

      }
      return true;
    }

    public function deleteCart($id)
    {
        if($this->products!=null){

          unset($this->products[$id]);

        }
    }

}

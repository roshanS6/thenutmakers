<?php
    use Illuminate\Support\Facades\DB;

    function getRandomString($length=10) {
        $lowerAlpha = range('a', 'z');
        $upperAlpha = range('A', 'Z');
        $numeric = range('0', '9');

        $workArray = array_merge($numeric, array_merge($lowerAlpha, $upperAlpha));
        $returnString = "";

        for ($i = 0; $i < $length; $i++) {
            $character = $workArray[rand(0, 61)];
            $returnString .= $character;
        }

        return $returnString;
    }

    function getOtp($length=4) {
        $numeric = range('0', '9');

        $workArray = array_merge($numeric);
        $returnString = "";

        for ($i = 0; $i < $length; $i++) {
            $character = $workArray[rand(0, 9)];
            $returnString .= $character;
        }

        return $returnString;
    }

    function welcomeNewUser($mobile) {
        $message = urlencode("Welcome to The Nut Makers, we are happy to have you onboard. Enjoy your shopping");
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => "http://api.msg91.com/api/sendhttp.php?sender=NUTMAK&route=4&mobiles=$mobile&authkey=257488A6EYhpwBS715c431a0b&country=91&message=$message",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_POSTFIELDS => "",
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSL_VERIFYPEER => 0,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
        return $err;
        }else {
        return 1;
        }
    }

    function welcomeNewUserEmail($email_id) {
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom("info@thenutmakers.com", "The Nut Makers");
        $email->setSubject("Welcome to The Nut Makers");
        $email->addTo($email_id);
        $path = "http://tnm.shadesix.in/images/logo.png";
        $greeting = "Dear customer,";
        $regards = "Regards,";
        $sign = "The Nut Makers";
        $content = "Welcome to The Nut Makers, we are happy to have you onboard. Enjoy your shopping.";
        $email->addContent(
            "text/html", "<pre><center><img src='$path' width=100 height=100></center><strong>".$greeting."</strong>"."<br><br>"."&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;".$content."<br><br>".$regards."<br>".$sign."</pre>"
        ); 
        $sendgrid = new \SendGrid('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');
        try {
            $response = $sendgrid->send($email);
            $error_code = $response->statusCode();
            return true;
            // print_r($response->headers());
            print $response->body() . "\n";
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
            return false;
        }
    }

    function sendEmailOtp($email_id, $otp) {
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom("info@thenutmakers.com", "The Nut Makers");
        $email->setSubject("Forgot Password OTP");
        $email->addTo($email_id);
        $path = "http://tnm.shadesix.in/images/logo.png";
        $greeting = "Dear customer,";
        $regards = "Regards,";
        $sign = "The Nut Makers";
        $content = "Here your account's OTP <strong>$otp</strong> for forgot password. Use this code to reset your password.";
        $email->addContent(
            "text/html", "<pre><center><img src='$path' width=100 height=100></center><strong>".$greeting."</strong>"."<br><br>"."&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;".$content."<br><br>".$regards."<br>".$sign."</pre>"
        );
        $sendgrid = new \SendGrid('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');
        try {
            $response = $sendgrid->send($email);
            $error_code = $response->statusCode();
            return true;
            // print_r($response->headers());
            print $response->body() . "\n";
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
            return false;
        }
    }

    function sendNewOrderEmail($order_id) {
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom("info@thenutmakers.com", "The Nut Makers");
        $email->setSubject("New Order Alert");
        $email->addTo("info@thenutmakers.com");
        $email->addContent(
            "text/html", "You have New Order (ID: <strong>$order_id</strong>) placed at www.thenumakers.com. kindly, Take a Look on it."
        );
        $sendgrid = new \SendGrid('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');
        try {
            $response = $sendgrid->send($email);
            $error_code = $response->statusCode();
            return true;
            // print_r($response->headers());
            print $response->body() . "\n";
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
            return false;
        }
    }

    function sendOrderEmail($email_id, $order_id, $order_status) {
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom("info@thenutmakers.com", "The Nut Makers");
        $email->setSubject("Order"." ".$order_status);
        $email->addTo($email_id);
        $path = "http://tnm.shadesix.in/images/logo.png";
        $greeting = "Dear customer,";
        $regards = "Regards,";
        $sign = "The Nut Makers";
        $content = "Your order has been $order_status, Order ID: <strong>$order_id</strong>.";
        $email->addContent(
            "text/html", "<pre><center><img src='$path' width=100 height=100></center><strong>".$greeting."</strong>"."<br><br>"."&emsp;&emsp;&emsp;&emsp;".$content."<br><br>".$regards."<br>".$sign."</pre>"
        );
        $sendgrid = new \SendGrid('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');
        try {
            $response = $sendgrid->send($email);
            $error_code = $response->statusCode();
            return true;
            // print_r($response->headers());
            print $response->body() . "\n";
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
            return false;
        }
    }
    
    function newEnquiryMail() {
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom("info@thenutmakers.com", "The Nut Makers");
        $email->setSubject("New Enquiry");
        $email->addTo("info@thenutmakers.com");
        $email->addContent(
            "text/html", "You have a new enquiry at www.thenutmakers.com. kindly, respond it!"
        );
        $sendgrid = new \SendGrid('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');
        try {
            $response = $sendgrid->send($email);
            $error_code = $response->statusCode();
            return true;
            // print_r($response->headers());
            print $response->body() . "\n";
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
            return false;
        }
    }

    function enquiryAckMail($email_id) {
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom("info@thenutmakers.com", "The Nut Makers");
        $email->setSubject("Enquiry Acknowledgement");
        $email->addTo($email_id);
        $path = "http://tnm.shadesix.in/images/logo.png";
        $greeting = "Dear customer,";
        $regards = "Regards,";
        $sign = "The Nut Makers";
        $content = "Your enquiry was resolved. Continue Shopping!";
        $email->addContent(
            "text/html", "<pre><center><img src='$path' width=100 height=100></center><strong>".$greeting."</strong>"."<br><br>"."&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;".$content."<br><br>".$regards."<br>".$sign."</pre>"            
        );
        $sendgrid = new \SendGrid('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');
        try {
            $response = $sendgrid->send($email);
            $error_code = $response->statusCode();
            return true;
            // print_r($response->headers());
            print $response->body() . "\n";
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
            return false;
        }
    }

    function newSubscribtionMail($email_id) {
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom("info@thenutmakers.com", "The Nut Makers");
        $email->setSubject("Subscription Mail");
        $email->addTo($email_id);
        $path = "http://tnm.shadesix.in/images/logo.png";
        $greeting = "Dear customer,";
        $regards = "Regards,";
        $sign = "The Nut Makers";
        $content = "Thank You for Subscribing us, Stay tuned for our updates.";
        $email->addContent(
            "text/html", "<pre><center><img src='$path' width=100 height=100></center><strong>".$greeting."</strong>"."<br><br>"."&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;".$content."<br><br>".$regards."<br>".$sign."</pre>"
        );
        $sendgrid = new \SendGrid('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');
        try {
            $response = $sendgrid->send($email);
            $error_code = $response->statusCode();
            return true;
            // print_r($response->headers());
            print $response->body() . "\n";
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
            return false;
        }
    }

    function sendOrderSms($mobile, $order_id, $order_status) {
        $message = urlencode("Your order has been $order_status, Order ID: $order_id.");
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => "http://api.msg91.com/api/sendhttp.php?sender=NUTMAK&route=4&mobiles=$mobile&authkey=257488A6EYhpwBS715c431a0b&country=91&message=$message",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_POSTFIELDS => "",
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSL_VERIFYPEER => 0,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
        return $err;
        }else {
        return 1;
        }
    }

?>
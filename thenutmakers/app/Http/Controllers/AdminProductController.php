<?php
namespace App\Http\Controllers;
use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;

class AdminProductController extends Controller {

  public function getProduct($id,Request $request) {
    if($request->session()->has('admin_id')) {
      $sql = "select * from products where id=?";
      $variety = "select * from product_variety where product_id=?";
      $products = DB::select($sql,[$id]);
      $product_variety = DB::select($variety,[$id]);
      // echo "<pre>"; print_r($product_variety);
      // exit;
      $sql = "select * from major_categories";
      $category = DB::select($sql);
      return view('admin.product',['category'=> $category,'products'=> $products[0], 'product_variety'=> $product_variety, 'id'=>$id]);
    }else {
      return redirect('/admin/login');
    }
  }

  public function products(Request $request) {
    if($request->session()->has('admin_id')) {
      $sql = "SELECT * from products ORDER BY priority ASC";
      $products = DB::select($sql);
      $sql = "select * from major_categories";
      $category = DB::select($sql);
      return view('admin.products',['category'=> $category,'products'=> $products]);
    }else {
      return redirect('/admin/login');
    }
  }

  public function addProduct(Request $request) {
    if($request->session()->has('admin_id')) {
      $sql = "select * from major_categories";
      $sql2= "select category_name,id from products";
      $category = DB::select($sql);
      $products = DB::select($sql2);
      return view('admin.addproducts',['category'=> $category,'products'=>$products]);
    }else {
      return redirect('/admin/login');
    }
  }

  public function insertProduct(Request $request) {
    try {
      $product_name = ucfirst($request->input('product_name'));
      $category_name = $request->input('category_name');
      $short_description = $request->input('short_description');
      $long_description = $request->input('long_description');
      $price = $request->input('price');
      $stock = $request->input('stock');
      $package_size = $request->input('package_size');
      $priority = $request->input('priority');
      $featured_product = $request->input('featured_product');
      $destinationPath = 'images/products/';
      $image = $request->file('image'); // main image upload
      $fileName = $image->getClientOriginalName();
      $file_path = str_replace(" ","-",$fileName);
      $upload_success = $image->move($destinationPath, $file_path);
      $main_img = $destinationPath."".$file_path;
      // $banner_image = $request->file('banner_image');// banner upload
      // if($banner_image!=NULL) {
      //   $banner_image_name = $banner_image->getClientOriginalName();
      //   $banner_image_path = str_replace(" ","-",$banner_image_name);
      //   $upload_success = $image1->move($destinationPath, $banner_image_path);
      //   $banner_img = $destinationPath."".$banner_image_path;
      // }else {
      //   $banner_img = NULL;
      // }
      $image1 = $request->file('image1');// image1 upload
      if($image1!=NULL) {
        $fileName1 = $image1->getClientOriginalName();
        $file_path1 = str_replace(" ","-",$fileName1);
        $upload_success = $image1->move($destinationPath, $file_path1);
        $img1 = $destinationPath."".$file_path1;
      }else {
        $img1 = NULL;
      }
      $image2 = $request->file('image2');
      if($image2!=NULL) {
        $fileName2 = $image2->getClientOriginalName();
        $file_path2 = str_replace(" ","-",$fileName2);
        $upload_success = $image2->move($destinationPath, $file_path2);
        $img2 = $destinationPath."".$file_path2;
      }else {
        $img2 = NULL;
      }
      $image3 = $request->file('image3');
      if($image3!=NULL) {
        $fileName3 = $image3->getClientOriginalName();
        $file_path3 = str_replace(" ","-",$fileName3);
        $upload_success = $image3->move($destinationPath, $file_path3);
        $img3 = $destinationPath."".$file_path3;
      }else {
        $img3 = NULL;
      }
      
      // if($featured_product == 1) {
      //   $sql = DB::select('select id, featured_product from products where featured_product = 1');
      //   if(count($sql)<=1){
      //     $replace_value = 0;
      //     $replace_id = $sql[0]->id;
      //     $update1 = DB::update('update products set featured_product=? where id=?',[$replace_value, $replace_id]);
      //     $products = DB::insert('INSERT INTO products (product_name, category_name, short_description, long_description, price, stock, package_size, featured_product, image_path, image_1, image_2, image_3) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [$product_name, $category_name, $short_description, $long_description, $price, $stock, $package_size, $featured_product, $main_img, $img1, $img2, $img3]);
      //   }
      // }else {
        $products = DB::insert('INSERT INTO products (product_name, category_name, short_description, long_description, price, stock, priority, package_size, featured_product, image_path, image_1, image_2, image_3) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [$product_name, $category_name, $short_description, $long_description, $price, $stock, $priority, $package_size, $featured_product, $main_img, $img1, $img2, $img3]);
      // }
      if ($products) {
        $get_id = DB::select('SELECT id from products where product_name=? and category_name=? and short_description=? and long_description=? and package_size=?', [$product_name, $category_name, $short_description, $long_description, $package_size]);
        $product_id = $get_id[0]->id;
        $package_size_1 = $request->input('package_size_1');
        if(isset($package_size_1)) {
          $stock_1 = $request->input('stock_1');
          $price_1 = $request->input('price_1');
          $variety_1 = DB::insert('INSERT INTO product_variety (product_id, package_size, price, stock) values (?, ?, ?, ?)', [$product_id, $package_size_1, $price_1, $stock_1]);
        }
        $package_size_2 = $request->input('package_size_2');
        if(isset($package_size_2)) {
          $stock_2 = $request->input('stock_2');
          $price_2 = $request->input('price_2');
          $variety_2 = DB::insert('INSERT INTO product_variety (product_id, package_size, price, stock) values (?, ?, ?, ?)', [$product_id, $package_size_2, $price_2, $stock_2]);
        } 
        $package_size_3 = $request->input('package_size_3');
        if(isset($package_size_3)) {
          $stock_3 = $request->input('stock_3');
          $price_3 = $request->input('price_3');
          $variety_3 = DB::insert('INSERT INTO product_variety (product_id, package_size, price, stock) values (?, ?, ?, ?)', [$product_id, $package_size_3, $price_3, $stock_3]);
        }
        $package_size_4 = $request->input('package_size_4');
        if(isset($package_size_4)) {
          $stock_4 = $request->input('stock_4');
          $price_4 = $request->input('price_4');
          $variety_4 = DB::insert('INSERT INTO product_variety (product_id, package_size, price, stock) values (?, ?, ?, ?)', [$product_id, $package_size_4, $price_4, $stock_4]);
        } 
        return redirect('admin/products')->with('message', 'Product added Successfully');
      }else{
        return redirect('admin/products')->with('message', 'Product Could not be added');
      }
    }catch(Exception $e) {
      return view('admin.error');
    }
  }

  public function updateFeatures($value, $id,Request $request){

    if($value == 0 || $value == 2){
      $product_id = $id;
      $featured_product = $value;
      $update = DB::update('update products set featured_product=? where id=?',[$featured_product, $product_id]);
      Session::flash('message', 'Feature Updated Successfully');
      return back();
    }elseif($value== 1) {
      $product_id = $id;
      $featured_product = $value;
      // $sql = DB::select('select id, featured_product from products where featured_product = 1');
      // if(count($sql)>0) {
      //   $replace_value = 0;
      //   $replace_id = $sql[0]->id;
      //   $update1 = DB::update('update products set featured_product=? where id=?',[$replace_value, $replace_id]);
      //   $update = DB::update('update products set featured_product=? where id=?',[$featured_product, $product_id]);
      //   Session::flash('message', 'Banner Product replaced Successfully');
      //   return back();
      // }elseif(count($sql)==0) {
        $update = DB::update('update products set featured_product=? where id=?',[$featured_product, $product_id]);
        Session::flash('message', 'Banner setted Successfully');
        return back();
    }
  }

  public function insertProductVariety(Request $request) {
    $product_id = $request->input('product_id');
    $stock = $request->input('stock');
    $price = $request->input('price');
    $package_size = $request->input('package_size');
    $sql = DB::insert('INSERT INTO product_variety (product_id, stock, price, package_size) VALUES (?, ?, ?, ?)', [$product_id, $stock, $price, $package_size]);
    if ($sql) {
      return back()->with('message', 'Product Variety inserted Successfully');
    } else {
      return back()->with('error', "can't Successfully");
    }
  }

  public function updateProductVariety($id, Request $request) {
    $product_id = $request->input('product_id');
    $stock = $request->input('stock_1');
    $price = $request->input('price_1');
    $package_size = $request->input('package_size_1');
    $sql = DB::update('UPDATE product_variety set stock=?, price=?, package_size=? where id=? and product_id=?',[$stock, $price, $package_size, $id, $product_id]);
    if ($sql) {
      return back()->with('message', 'Product Variety updated Successfully');
    } else {
      return back()->with('error', "can't Successfully");      
    }
  }

  public function deleteProductVariety($id) {
    try{
      $sql = "delete from product_variety where id = ?";
      DB::delete($sql,[$id]);
      return back()->with('message', 'Variety Deleted Successfully');
    }catch(Exception $e) {
      return view('admin.error');
    }
  }

  public function  updateProduct(Request $request){
    try {
      $id = $request->input('id');
      $product_name =  ucfirst($request->input('product_name'));
      $short_description = $request->input('short_description');
      $long_description = $request->input('long_description');
      $category_name = $request->input('category_name');
      $price = $request->input('price');
      $priority = $request->input('priority');
      $stock = $request->input('stock');
      $package_size = $request->input('package_size');
      $sql = "update products set product_name=?, category_name=?, short_description=?, long_description=?, priority=?, price=?, stock=?, package_size=? where id=?";
      DB::update($sql, [$product_name, $category_name, $short_description, $long_description, $priority, $price, $stock, $package_size, $id]);
      $image = $request->file('category_image');//update of main_image
      if ($image!=null) {
        $destinationPath = 'images/products/';
        $fileName = $image->getClientOriginalName();
        $file_path = str_replace(" ","-",$fileName);
        $upload_success = $image->move($destinationPath, $file_path);
        DB::update("update products set image_path=? where id=?", [$destinationPath."".$file_path, $id]);
      }
      // $banner_image = $request->file('banner_image');
      // if ($banner_image!=null) {
      //   $destinationPath = 'images/products/';
      //   $banner_image_name = $banner_image->getClientOriginalName();
      //   $file_path = str_replace(" ","-",$banner_image_name);
      //   $upload_success = $banner_image->move($destinationPath, $file_path);
      //   DB::update("update products set banner_image=? where id=?", [$destinationPath."".$file_path, $id]);
      // }
      $image_1 = $request->file('image_1');
      if ($image_1!=null) {
        $destinationPath = 'images/products/';
        $fileName = $image_1->getClientOriginalName();
        $file_path = str_replace(" ","-",$fileName);
        $upload_success = $image_1->move($destinationPath, $file_path);
        DB::update("update products set image_1=? where id=?", [$destinationPath."".$file_path, $id]);
      }
      $image_2 = $request->file('image_2');
      if ($image_2!=null) {
        $destinationPath = 'images/products/';
        $fileName = $image_2->getClientOriginalName();
        $file_path = str_replace(" ","-",$fileName);
        $upload_success = $image_2->move($destinationPath, $file_path);
        DB::update("update products set image_2=? where id=?", [$destinationPath."".$file_path, $id]);
      }
      $image_3 = $request->file('image_3');
      if ($image_3!=null) {
        $destinationPath = 'images/products/';
        $fileName = $image_3->getClientOriginalName();
        $file_path = str_replace(" ","-",$fileName);
        $upload_success = $image_3->move($destinationPath, $file_path);
        DB::update("update products set image_3=? where id=?", [$destinationPath."".$file_path, $id]);
      }
      return redirect('/admin/products')->with('message', 'Product Updated Successfully');
    }catch(Exception $e) {
      return back();
    }
  }

  public function deleteProduct($id) {
    try{
      $sql = "delete from products where id = ?";
      DB::delete($sql,[$id]);
      return redirect('admin/products')->with('message', 'Product Deleted Successfully');
    }catch(Exception $e) {
      return view('admin.error');
    }
  }

  public function deleteProductImage($id,$name,Request $request){
    try{
      if($name=='image_1') {
        $sql = "update products set image_1='' where id = ?";
        $result = DB::update($sql,[$id]);
      }
      if ($name=='image_2') {
        $sql = "update products set image_2='' where id = ?";
        $result = DB::update($sql,[$id]);
      }
      if ($name=='image_3') {
        $sql = "update products set image_3='' where id = ?";
        $result = DB::update($sql,[$id]);
      }
      if($result){
        return redirect()->back()->with('message', 'Product Image Deleted Successfully');
      }else {
        return redirect()->back()->with('message', 'Product Image could not be deleted');
      }
    }catch(Exception $e) {
      return view('admin.error');
    }
  }
}
?>
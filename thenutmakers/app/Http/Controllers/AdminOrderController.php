<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use NumberToWords\NumberToWords;
use App\Helper;
use Session;
use Validator;
use Response;

class AdminOrderController extends Controller {

    public function allOrders(Request $request) {

        if($request->session()->has('admin_id')) {
            $sub_title= "All Orders";
            $order = DB::select('select *, COUNT(o.order_id) AS NumberOfOrders, GROUP_CONCAT(p.product_name) as group_product, GROUP_CONCAT(o.quantity) as group_quantity from products p 
                            INNER JOIN orders o on p.id = o.product_id where o.transaction_status = 1 group by o.order_id ORDER BY o.order_time DESC');
                            // echo "<pre>"; print_r($order);
                            // exit;
            return view('admin.allorders',['orders'=> $order,'sub_title'=>$sub_title]);
        }else {
        return redirect('/admin/login');
        }
    }

    public function cancelledOrders(Request $request) {
        if($request->session()->has('admin_id')) {
            $order_status=0;
            $sub_title= "Cancelled Orders";
            $order = DB::select('select *, COUNT(o.order_id) AS NumberOfOrders, GROUP_CONCAT(p.product_name) as group_product, GROUP_CONCAT(o.quantity) as group_quantity from products p 
                            INNER JOIN orders o on p.id = o.product_id where order_status=? and transaction_status = 1 group by o.order_id ORDER BY o.order_time DESC', [$order_status]);
                            // echo "<pre>"; print_r($order);
                            // exit;
            return view('admin.cancelledorders',['orders'=> $order,'sub_title'=>$sub_title]);
        }else {
        return redirect('/admin/login');
        }
    }
       
    public function placedOrders(Request $request) {
        if($request->session()->has('admin_id')) {
            $order_status=1;
            $sub_title= "Placed Orders";
            $order = DB::select('select *, COUNT(o.order_id) AS NumberOfOrders, GROUP_CONCAT(p.product_name) as group_product, GROUP_CONCAT(o.quantity) as group_quantity from products p 
                        INNER JOIN orders o on p.id = o.product_id where order_status=? group by o.order_id ORDER BY o.order_time DESC', [$order_status]);
            return view('admin.allorders',['orders'=> $order,'sub_title'=>$sub_title]);
        }else{
            return redirect('/admin/login');
        }
    }

    public function confirmedOrders(Request $request) {
        if($request->session()->has('admin_id')) {
            $order_status=2;
            $sub_title= "Confirmed Orders";
            $order = DB::select('select *, COUNT(o.order_id) AS NumberOfOrders, GROUP_CONCAT(p.product_name) as group_product, GROUP_CONCAT(o.quantity) as group_quantity from products p 
                        INNER JOIN orders o on p.id = o.product_id where order_status=? and transaction_status = 1 group by o.order_id ORDER BY o.order_time DESC', [$order_status]);
            return view('admin.allorders',['orders'=> $order,'sub_title'=>$sub_title]);
        }else {
            return redirect('/admin/login');
        }
    }

    public function dispatchedOrders(Request $request) {
        if($request->session()->has('admin_id')) {
            $order_status=3;
            $sub_title= "Dispatched Orders";
            $order = DB::select('select *, COUNT(o.order_id) AS NumberOfOrders, GROUP_CONCAT(p.product_name) as group_product, GROUP_CONCAT(o.quantity) as group_quantity from products p 
                        INNER JOIN orders o on p.id = o.product_id where order_status=? and transaction_status = 1 group by o.order_id ORDER BY o.order_time DESC', [$order_status]);
            return view('admin.allorders',['orders'=> $order,'sub_title'=>$sub_title]);
        }else {
            return redirect('/admin/login');
        }
    }

    public function deliveredOrders(Request $request) {
        if($request->session()->has('admin_id')) {
            $order_status=4;
            $sub_title= "Delivered Orders";
            $order = DB::select('select *, COUNT(o.order_id) AS NumberOfOrders, GROUP_CONCAT(p.product_name) as group_product, GROUP_CONCAT(o.quantity) as group_quantity from  products p 
                        INNER JOIN orders o on p.id = o.product_id where order_status=? and transaction_status = 1 group by o.order_id ORDER BY o.order_time DESC', [$order_status]);
            return view('admin.allorders',['orders'=> $order,'sub_title'=>$sub_title]);
        }else {
            return redirect('/admin/login');
        }
    }

    public function returnedOrders(Request $request) {
        if($request->session()->has('admin_id')) {
            $order_status=5;
            $sub_title= "Returned Orders";
            $order = DB::select('select *, COUNT(o.order_id) AS NumberOfOrders, GROUP_CONCAT(p.product_name) as group_product, GROUP_CONCAT(o.quantity) as group_quantity from products p 
                        INNER JOIN orders o on p.id = o.product_id where order_status=? and transaction_status = 1 group by o.order_id ORDER BY o.order_time DESC', [$order_status]);
            return view('admin.returnedorders',['orders'=> $order,'sub_title'=>$sub_title]);
        }else {
            return redirect('/admin/login');
        }
    }

    public function failedOrders(Request $request) {
        if($request->session()->has('admin_id')) {
            $order_status = 0;
            $sub_title= "Returned Orders";
            $order = DB::select('SELECT *, COUNT(o.order_id) AS NumberOfOrders, GROUP_CONCAT(p.product_name) as group_product, GROUP_CONCAT(o.quantity) as group_quantity from products p 
                        INNER JOIN orders o on p.id = o.product_id
                        LEFT JOIN user on user.user_id = o.user_id
                        where o.order_status=? and o.transaction_status = 0 group by o.order_id  ORDER BY o.order_time DESC', [$order_status]);
            // echo "<pre>"; print_r($order);
            // exit;
            return view('admin.failedorders',['orders'=> $order,'sub_title'=>$sub_title]);
        }else {
            return redirect('/admin/login');
        }
    }

    public function cancelOrder($value,$id,$reason, Request $request) {

        $order_id = $id;
        $order_status = $value;
        $order_status_email = "cancelled";
        $time = date('Y-m-d H:i:s');
        if($order_status == 0) {
            $declined_reason = $reason;
            $user_details = DB::select('SELECT phone, email FROM user
                                        INNER JOIN orders on user.user_id = orders.user_id where orders.order_id=?',[$order_id]);
            $user_email = $user_details[0]->email;
            $user_phone = $user_details[0]->phone;
            $update = DB::update('update orders set order_status=?, cancelled_time=?, declined_reason=? where order_id=?',[$order_status, $time, $declined_reason, $order_id]);
            if($update) {
                sendOrderEmail($user_email, $order_id, $order_status_email);
                sendOrderSms($user_phone, $order_id, $order_status_email);
                Session::flash('message', 'Status Updated Successfully');
                return back();
            }else {
                Session::flash('message', 'Unsuccessful');
                return back();
            }
        }
    }
    
    public function updateOrderStatus($value,$id,Request $request) {
                
        $order_id = $id;
        $order_status = $value;
        $time = date('Y-m-d H:i:s');
        if($order_status == 0)
            {
                $order_status_email = "cancelled";
                $declined_reason = $reason;
                $update = DB::update('update orders set order_status=?, cancelled_time=?, declined_reason=? where order_id=?',[$order_status, $time, $order_id, $declined_reason]);
                $user_details = DB::select('SELECT phone, email FROM user
                                        INNER JOIN orders on user.user_id = orders.user_id where orders.order_id=?',[$order_id]);
                $user_email = $user_details[0]->email;
                $user_phone = $user_details[0]->phone;
                sendOrderEmail($user_email, $order_id, $order_status_email);
                sendOrderSms($user_phone, $order_id, $order_status_email);
                Session::flash('message', 'Status Updated Successfully');
                return back();
            }        
        elseif($order_status == 2)
            {
                $order_status_email = "confirmed";
                $update = DB::update('update orders set order_status=?, processed_time=? where order_id=?',[$order_status, $time, $order_id]);
                $user_details = DB::select('SELECT phone, email FROM user
                                        INNER JOIN orders on user.user_id = orders.user_id where orders.order_id=?',[$order_id]);
                $user_email = $user_details[0]->email;
                $user_phone = $user_details[0]->phone;
                sendOrderEmail($user_email, $order_id, $order_status_email);
                sendOrderSms($user_phone, $order_id, $order_status_email);
                Session::flash('message', 'Status Updated Successfully');
                return back();
            }
        elseif($order_status == 3)
            {
                $order_status_email = "Dispatched";
                $update = DB::update('update orders set order_status=?, shipped_time=? where order_id=?',[$order_status, $time, $order_id]);
                $user_details = DB::select('SELECT phone, email FROM user
                                    INNER JOIN orders on user.user_id = orders.user_id where orders.order_id=?',[$order_id]);
                $user_email = $user_details[0]->email;
                $user_phone = $user_details[0]->phone;
                sendOrderEmail($user_email, $order_id, $order_status_email);
                sendOrderSms($user_phone, $order_id, $order_status_email);
                Session::flash('message', 'Status Updated Successfully');
                return back();
            }
        elseif($order_status == 4)
            {
                $order_status_email = "Delivered";
                $update = DB::update('update orders set order_status=?, delivery_time=? where order_id=?',[$order_status, $time, $order_id]);
                $user_details = DB::select('SELECT phone, email FROM user
                                    INNER JOIN orders on user.user_id = orders.user_id where orders.order_id=?',[$order_id]);
                $user_email = $user_details[0]->email;
                $user_phone = $user_details[0]->phone;
                sendOrderEmail($user_email, $order_id, $order_status_email);
                sendOrderSms($user_phone, $order_id, $order_status_email);
                Session::flash('message', 'Status Updated Successfully');
                return back();
            }
    }

    public function orderInvoice($id,Request $request) {
        if($request->session()->has('admin_id')){
            $sql="SELECT * FROM orders o
            INNER JOIN products p on o.product_id = p.id
            INNER JOIN user u on o.user_id = u.user_id
            INNER JOIN shipping s on o.order_id = s.order_id  WHERE o.order_id=? and o.transaction_status = 1";
            $bill_add ="SELECT * FROM address a
                        INNER JOIN orders o on o.billing_address = a.address_id where o.order_id=?";
            $del_add ="SELECT * FROM address a
                    INNER JOIN orders o on o.delivery_address = a.address_id where o.order_id=?";    
            $orders=DB::select($sql,[$id]);
            if($orders) {
                $bill_address=DB::select($bill_add,[$id]);
                $del_address=DB::select($del_add,[$id]);
                $constant = DB::select('select * from constants');
                // echo "<pre>"; print_r($orders);
                // exit;
                return view('admin.orderinvoice',['orders'=>$orders,'billing_address'=>$bill_address, 'delivery_address'=>$del_address, 'constants'=>$constant]);
            } else {
                return back()->with('message', "Failed Transaction doesn't have invoice");
            }
        }else {
            return redirect('/admin/login');
        }
    }
    
    public function invoice($id,Request $request) {
        if($request->session()->has('admin_id')) {
            $sql="SELECT * FROM orders o
            INNER JOIN products p on o.product_id = p.id
            INNER JOIN user u on o.user_id = u.user_id
            INNER JOIN shipping s on o.order_id = s.order_id  WHERE o.order_id=? and o.transaction_status = 1";
            $bill_add ="SELECT * FROM address a
                        INNER JOIN orders o on o.billing_address = a.address_id where o.order_id=?";
            $del_add ="SELECT * FROM address a
                    INNER JOIN orders o on o.delivery_address = a.address_id where o.order_id=?";    
            $orders=DB::select($sql,[$id]);
            $bill_address=DB::select($bill_add,[$id]);
            $del_address=DB::select($del_add,[$id]);
            $constant = DB::select('select * from constants');    
            //  echo "<pre>";  print_r($orders);exit;
            return view('admin/invoice',['orders'=>$orders,'billing_address'=>$bill_address, 'delivery_address'=>$del_address, 'constants'=>$constant]);
        } else {
            return redirect('/admin/login');
        }
    }
}

?>
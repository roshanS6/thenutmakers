<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;

class Controller extends BaseController {

  public function home(Request $request) {
    // $banner = DB::select('SELECT * from products where featured_product=1');
    $banner = DB::select('SELECT * from banners');
    $slider = DB::select('SELECT * from products where featured_product=2 ORDER BY category_name ASC');
    $aboutus = DB::SELECT('SELECT * FROM about_us');
    $constant = DB::select('SELECT * from constants');
    $testimonial = DB::select('SELECT * from testimonial where status = 1');
    return view('home',['banner'=>$banner,'testimonial'=>$testimonial, 'constants'=>$constant, 'sliders'=>$slider, 'aboutus'=>$aboutus[0]]);
  }

  public function search(Request $request) {
    $key = $request->input('key');
    $sql = "SELECT * FROM major_categories";
    $categories = DB::select($sql);
    $products = DB::select("select * from products where product_name like ? or category_name like ? or short_description like ? or package_size like ?", ["%$key%", "%$key%", "%$key%", "%$key%"]);
    $constant = DB::select('select * from constants');
    return view('search',['categories'=> $categories,'products'=>$products,'constants'=>$constant]);
  }

  public function product() {
    $sql = "SELECT * FROM major_categories";
    $categories = DB::select($sql);
    $products = DB::select('SELECT * from products inner join major_categories where products.category_name = major_categories.category_name ORDER BY priority ASC');
    $constant = DB::select('select * from constants');
    return view('product',['categories'=> $categories,'products'=>$products,'constants'=>$constant]);
  }
  
  public function contact(Request $request) {
    try{
      $name = ucfirst($request->input('name'));
      $location = $request->input('location');
      $email = $request->input('email');
      $phone = $request->input('phone');
      $message = $request->input('message');
      $products = DB::insert('INSERT INTO contact_info (fullname, location, email, phone, message) VALUES (?, ?, ?, ?, ?)', [$name, $location, $email, $phone, $message]);
      Session::flash('message','Product added Successfully');
      return back();
    }catch(Exception $e) {
      return view('admin.error');
    }
  }
  
  public function product_details($id, Request $request) {
    $sql = "select * from products where id=?";
    $variety = "select * from product_variety where product_id=?";
    $products = DB::select($sql,[$id]);
    $product_variety = DB::select($variety,[$id]);
    $constant = DB::select('select * from constants');
    return view('product_details',['constants'=>$constant,'product_variety'=> $product_variety ,'products'=> $products]);
  }

  public function orderInvoice($id,Request $request) {
    if($request->session()->has('user_id')) {
      $user_id = $request->session()->get('user_id');
      $sql="SELECT * FROM orders o
      INNER JOIN products p on o.product_id = p.id
      INNER JOIN user u on o.user_id = u.user_id
      INNER JOIN shipping s on o.order_id = s.order_id  WHERE o.order_id=? and o.user_id=? and o.transaction_status = 1";
      $bill_add ="SELECT * FROM address a
                  INNER JOIN orders o on o.billing_address = a.address_id where o.order_id=? and o.user_id=?";
      $del_add ="SELECT * FROM address a
              INNER JOIN orders o on o.delivery_address = a.address_id where o.order_id=? and o.user_id=?";    
      $orders=DB::select($sql,[$id, $user_id]);
      if($orders) {
        $bill_address=DB::select($bill_add,[$id, $user_id]);
        $del_address=DB::select($del_add,[$id, $user_id]);
        $constant = DB::select('select * from constants');
        // echo "<pre>";  print_r($orders);exit;
        return view('orderinvoice',['orders'=>$orders,'billing_address'=>$bill_address, 'delivery_address'=>$del_address, 'constants'=>$constant]);
      } else {
        return redirect('/signin');
      }
    } else {
      return redirect('/signin');
    }
  }

  public function invoice($id, Request $request) {
    if($request->session()->has('user_id')) {
      $user_id = $request->session()->get('user_id');
      $sql="SELECT * FROM orders o
      INNER JOIN products p on o.product_id = p.id
      INNER JOIN user u on o.user_id = u.user_id
      INNER JOIN shipping s on o.order_id = s.order_id  WHERE o.order_id=? and o.user_id=? and o.transaction_status = 1";
      $bill_add ="SELECT * FROM address a
                  INNER JOIN orders o on o.billing_address = a.address_id where o.order_id=? and o.user_id=?";
      $del_add ="SELECT * FROM address a
              INNER JOIN orders o on o.delivery_address = a.address_id where o.order_id=? and o.user_id=?";    
      $orders=DB::select($sql,[$id, $user_id]);
      if($orders) {
        $bill_address=DB::select($bill_add,[$id, $user_id]);
        $del_address=DB::select($del_add,[$id, $user_id]);
        $constant = DB::select('select * from constants');
      //  echo "<pre>";  print_r($orders);exit;
        return view('invoice',['orders'=>$orders,'billing_address'=>$bill_address, 'delivery_address'=>$del_address, 'constants'=>$constant]);
      } else {
        return redirect('/signin');
      }        
    } else {
      return redirect('/signin');
    }
}

  public function signup() {
    return view('userregister');
  }

  public function register(Request $request) {
    $first_name = ucfirst($request->input('first_name'));
    $last_name = ucfirst($request->input('last_name'));
    $email = $request->input('email');
    $phone = $request->input('phone');
    $password = md5($request->input('password'));
    $check_email = DB::select('select email from user where email=?',[$email] );
    $check_phone = DB::select('select email from user where email=?',[$email] );
    $check_subscribtion = DB::select('select email from subscribers where email=?',[$email] );
    if(count($check_email)==0) {
      $sql = DB::insert('insert into subscribers (email) values (?)', [$email]);
    }
    if(count($check_phone)>0) {
      $request->session()->put('error',"Phone number already registered with another account");
      return back();
    }
    if(count($check_email)==0) {
      $sql = DB::insert('insert into user (first_name,last_name,email,phone,password) values (?, ?, ?, ?, ?)', [$first_name, $last_name, $email, $phone, $password]);
      if($sql) {
        welcomeNewUser($phone);
        welcomeNewUserEmail($email);
        $request->session()->put('message',"Registered, now login");
        return back();
      }else {
        $request->session()->put('error',"Can't add try again");
        return back();
      }
    }else {
      $request->session()->put('error',"Email id already exists, try to login");
      return back();
    }
  }

  public function signin(Request $request) {
    if(!$request->session()->has('user_id')) {
      $constant = DB::select('select * from constants');
      return view('userlogin',['constants'=>$constant]);
    } else {
      return redirect('profile');
    }
  }

  public function terms() {
    $constant = DB::select('select * from constants');
    return view('terms',['constants'=>$constant]);
  }

  public function privacy() {
    $constant = DB::select('select * from constants');
    return view('privacy',['constants'=>$constant]);
  }

  public function refundpolicy() {
    $constant = DB::select('select * from constants');
    return view('refundpolicy',['constants'=>$constant]);
  }

  public function disclaimer() {
    $constant = DB::select('select * from constants');
    return view('disclaimer',['constants'=>$constant]);
  }
  
  public function faqs() {
    $constant = DB::select('select * from constants');
    return view('faqs',['constants'=>$constant]);
  }

  public function login(Request $request) {
    $email = $request->input('email');
    $password = md5($request->input('password'));
    $sql= DB::select('select * from user where email=?', [$email]);
    if(count($sql)!=0) {
      if($password == $sql[0]->password) {
        $request->session()->put('message',"Login successfully");
        $request->session()->put('user_id', $sql[0]->user_id);
        $user_id = $sql[0]->user_id;
        $cart_data = DB::select('SELECT * from cart car
                                inner join products product on car.product_id = product.id
                                where user_id=? and variety IS NULL', [$user_id]);
        if(count($cart_data) > 0) {
          $request->session()->put('cart_count', true);
        } else {
          $request->session()->put('cart_count', null);
        }
        return redirect('/profile');
      }else {
        $request->session()->put('error',"Invalid Password");
        return redirect('/signin');
      }
    }else {
      $request->session()->put('error',"Invalid User");
      return back();
    }
  }

  public function generateotp(Request $request) {
    $email = $request->input('email');
    $sql = DB::select('SELECT * from user where email = ?', [$email]);
    if ($sql) {
      $otp = getOtp(4);
      $updateotp = DB::update('UPDATE user set otp = ? where email = ?', [$otp, $email]);
      if ($updateotp) {
        $mail = sendEmailOtp($email, $otp);
        return view('forgotpassword', ['email'=>$email])->with('message', "OTP Generated successFull");
      } else {
        return back()->with('error', "Something went wrong!");
      }
    } else {
      return back()->with('error', "No Such Email id");
    }
  }

  public function resendotp($value, Request $request) {
    $email = decrypt($value);
    $otp = getOtp(4);
    $updateotp = DB::update('UPDATE user set otp = ? where email = ?', [$otp, $email]);
    if ($updateotp) {
      $mail = sendEmailOtp($email, $otp);
      return view('forgotpassword', ['email'=>$email])->with('message', "OTP Generated successFull");
    } else {
      return back()->with('error', "Something went wrong!");
    }
  }

  public function changepassword(Request $request) {
    $email = $request->input('email');
    $otp = $request->input('otp');
    $sql = DB::select('SELECT otp From user where email = ?', [$email]);
    if ($sql[0]->otp == $otp) {
      $password = md5($request->input('cpassword'));
      $null = null;
      $update = DB::update('UPDATE user set password = ?, otp = ? where email = ?', [$password, $null, $email]);
      if ($update) {
        return redirect('/signin')->with('message', "Password has changed, Try login!");
      } else {
        return redirect('/signin')->with('error', "Something Went wrong!");
      }
    } else {
      return back()->with('error', "Otp Doesn't matches");
    }
  }

  public function logout(Request $request) {
    $request->session()->forget('user_id');
    $request->session()->flush();
    return redirect('/signin');
  }

  public function profile(Request $request) {
    if($request->session()->has('user_id')) {
      $user_id =$request->session()->get('user_id');
      $cancelled = 0;
      $delivered = 4;
      $k=0;
      $sql = DB::select('select * from user where user_id = ?',[$user_id]);
      $orders_data = DB::select('SELECT *, COUNT(orders.Order_id) AS NumberOfOrders, GROUP_CONCAT(products.product_name) as group_product, GROUP_CONCAT(orders.quantity) as group_quantity from orders 
                              inner join products on orders.product_id = products.id
                              inner join user on user.user_id = orders.user_id
                              inner join product_variety on product_variety.product_id = products.id
                              where orders.user_id = ? and orders.order_status =? and orders.transaction_status = 1 group by orders.order_id ORDER BY orders.order_time DESC',[$user_id, $delivered]);
      // $orders_variety_data = DB::select('SELECT *, COUNT(orders.Order_id) AS NumberOfOrders, GROUP_CONCAT(products.product_name) as group_product, GROUP_CONCAT(orders.quantity) as group_quantity from orders 
      //                                         inner join products on orders.product_id = products.id
      //                                         inner join user on user.user_id = orders.user_id
      //                                         inner join product_variety on product_variety.id = orders.variety
      //                                         where orders.user_id = ? and orders.transaction_status = 1 group by orders.order_id ORDER BY orders.order_time DESC',[$user_id]);
      $orders_variety_data = DB::select('SELECT *, COUNT(orders.Order_id) AS NumberOfOrders, GROUP_CONCAT(products.product_name) as group_product, GROUP_CONCAT(orders.quantity) as group_quantity from orders 
                                      inner join products on orders.product_id = products.id
                                      inner join user on user.user_id = orders.user_id
                                      inner join product_variety on product_variety.product_id = products.id and product_variety.package_size = orders.variety
                                      where orders.user_id = ? and orders.order_status =? and orders.transaction_status = 1 group by orders.order_id ORDER BY orders.order_time DESC',[$user_id, $delivered]);
      // echo "<pre>";
      // print_r($orders_data);
      // exit;
      $orders = array();
      for($i=0; $i<count($orders_data);$i++) {
        $orders[$i]= $orders_data[$i];
        $k = $i;
        if ($i == (count($orders_data)-1)) {
          $k++;
        }
      }
      for($i=0 ; $i < count($orders_variety_data) ; $i++) {
        $orders[$k]= $orders_variety_data[$i];
        $k++;
      }
      $present_orders_data = DB::select('SELECT *, COUNT(orders.Order_id) AS NumberOfOrders, GROUP_CONCAT(products.product_name) as group_product, GROUP_CONCAT(orders.quantity) as group_quantity from orders
                                    inner join products on orders.product_id = products.id
                                    inner join user on user.user_id = orders.user_id
                                    where orders.user_id=? and orders.order_status != ? and orders.order_status !=? and orders.transaction_status = 1 group by orders.order_id ORDER BY orders.order_time DESC',[$user_id, $delivered, $cancelled] );
      $present_orders_variety_data = DB::select('SELECT *, COUNT(orders.Order_id) AS NumberOfOrders, GROUP_CONCAT(products.product_name) as group_product, GROUP_CONCAT(orders.quantity) as group_quantity from orders
                                              inner join products on orders.product_id = products.id
                                              inner join user on user.user_id = orders.user_id
                                              inner join product_variety on product_variety.product_id = orders.product_id and product_variety.package_size = orders.variety
                                              where orders.user_id=? and orders.order_status != ? and orders.order_status !=? and orders.transaction_status = 1 group by orders.order_id ORDER BY orders.order_time DESC',[$user_id, $delivered, $cancelled] );    
      $present_orders = array();
      $k = 0;
      for($i=0; $i<count($present_orders_data);$i++) {
        $present_orders[$i]= $present_orders_data[$i];
        $k = $i;
        if ($i == (count($present_orders_data)-1)) {
          $k++;
        }
      }
      for($i=0 ; $i < count($present_orders_variety_data) ; $i++) {
        $present_orders[$k]= $present_orders_variety_data[$i];
        $k++;
      }  
      
      // echo "<pre>";  print_r($present_orders_variety_data);exit;
      $address = DB::select('select * from address where user_id=?',[$user_id]);
      $constant = DB::select('select * from constants');
      return view('profile',['user'=>$sql,'constants'=>$constant,'address'=>$address,'past_orders'=>$orders,'present_orders'=>$present_orders]);
    }else {
      return redirect('/signin');
    }
  }

  public function trackOrder($id, Request $request) {
    if($request->session()->has('user_id')) {
      $user_id =$request->session()->get('user_id');
      $sql = DB::select('select * from user where user_id = ?',[$user_id]);
      $track_order = DB::select('select * from orders where order_id =? and user_id =? and transaction_status = 1',[$id, $user_id]);
      if($track_order) {
        $address = DB::select('select * from address where user_id=?',[$user_id]);
        $constant = DB::select('select * from constants');
        return view('track_order',['track_order'=>$track_order,'address'=>$address,'user'=>$sql,'constants'=>$constant]);
      } else {
        return redirect('/signin');
      }
    }else {
      return redirect('/signin');
    }
  }

  public function aboutus(Request $request) {
    $aboutus = DB::SELECT('SELECT * FROM about_us');
    $constant = DB::select('select * from constants');
    return view('aboutus',['aboutus'=>$aboutus[0],'constants'=>$constant]);
  }  

  public function contactus(Request $request) {
    // $aboutus = DB::SELECT('SELECT * FROM about_us');
    $constant = DB::select('select * from constants');
    return view('contactus',['constants'=>$constant]);
  }

  public function saveaddress(Request $request) {
    $fname = $request->input('first_name');
    $lname = $request->input('last_name');
    $phone = $request->input('phone');
    $address_line_1 = $request->input('address_line_1');
    $address_line_2 = $request->input('address_line_2');
    $country = $request->input('country');
    $city = $request->input('city');
    $state = $request->input('state');
    $zip = $request->input('zip');
    $user_id = $request->session()->get('user_id');
    $active=1;
    $active_address = DB::select('select address_id, active_address from address where active_address=? and user_id=?',[$active, $user_id]);
  
    if(count($active_address)==0) {
      $sql = DB::insert('insert into address (address_fname, address_lname, address_line_1, address_line_2, country, city, state, zip, phone, active_address, user_id) values (?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',[$fname, $lname, $address_line_1, $address_line_2, $country, $city, $state, $zip, $phone, $active, $user_id]);
      if($sql) {
        $request->session()->put('message',"Address added");
        return redirect('/checkout');
      }else {
        $request->session()->put('message','Error in adding');
        return redirect('/checkout');
      }
    }else {   
      $active =  0;
      $sql = DB::insert('insert into address (address_fname, address_lname, address_line_1, address_line_2, country, city, state, zip, phone, active_address, user_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',[$fname, $lname, $address_line_1, $address_line_2, $country, $city, $state, $zip, $phone, $active, $user_id]);
      if($sql) {
        $request->session()->put('message',"Address added");
        return redirect('/checkout');
      }else {
        $request->session()->put('message','Error in adding');
        return redirect('/checkout');
      }
    }
  }

  public function enquiry(Request $request) {    
    $recaptcha = $request->input('g-recaptcha-response');
    if(isset($recaptcha) && !empty($recaptcha))
    {
      $secret = '6LeKJokUAAAAAIPuYAoZEKm2whFRs1t3_8-Typ3Z';
      $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$recaptcha);
      $responseData = json_decode($verifyResponse);
      if($responseData->success) {
        $name = $request->input('name');
        $email = $request->input('email');
        $subject = $request->input('subject');
        $message = $request->input('message');
        $sql = DB::insert('INSERT into enquiries (name, email, subject, message ) values (?, ?, ?, ?)', [$name, $email, $subject, $message]);
        if ($sql) {
          $request->session()->put('message',"Enquiry mail was sent");
          newEnquiryMail();
          return back();
        } else {
          $request->session->put('message', "Something Went Wrong");
          return back();
        }
      } else {
        $request->session()->put('error',"Invalid Captcha");
        return back();
      }
    }else{
      $request->session()->put('error',"Captcha Error");
      return back();
    }
  }

  public function subscribe(Request $request) {
    $email = $request->input('email');
    $check_subscribtion = DB::select('select email from subscribers where email=?',[$email] );
    if(count($check_subscribtion)==0) {
      $sql = DB::insert('insert into subscribers (email) values (?)', [$email]);
      if($sql) {
        newSubscribtionMail($email);
        return back()->with('message', 'Thank you for subscribing us!');
      }
    } else {
      return back()->with('message', 'You are already subscribed us!');
    }
  }

  public function testsms(Request $request) {
    $test = sendEmailOtp("harisvar00c@gmail.com", "9686");
    var_dump($test);
  }
}

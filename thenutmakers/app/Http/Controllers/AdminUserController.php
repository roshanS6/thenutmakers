<?php
namespace App\Http\Controllers;
use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class AdminUserController extends Controller {
  public function LoginPage(Request $request) {
    if($request->session()->has('admin_id')){
      return redirect('/admin/dashboard');
    }else {
      return view('admin.login');
    }
  }
  
  public function processlogin(Request $request) {
    $mail = $request->input('mail');
    $password = $request->input('password');
    $result = DB::select('select * from admin where user_name=?',[$mail]);
    if(count($result)>0){
      if($password == $result[0]->password) {
        $request->session()->put('admin_id',$result[0]->id);
        return redirect('/admin/dashboard');
      }else{
        return redirect('/admin/login');
      }
    }else{
      return redirect('/admin/login');
    }
  }

  public function logout(Request $request) {
    $request->session()->flush();
    return redirect('/admin/login');
  }

  public function getUsers(Request $request) {
    if($request->session()->has('admin_id')){
      $users = DB::select("select id,name,email,phone,cast(created_at as date) joined_date from users");
      return view('admin.users',['users'=>$users]);
    }else {
      return redirect('/admin/login');
    }
  }

  public function getUserInfo($id,Request $request) {
    if($request->session()->has('admin_id')){
      $user = DB::select("select id,name,email,phone,cast(created_at as date) joined_date from users where id=?",[$id]);
      $orders = DB::select("select transactions.id,extras,order_status,payment_mode,
                  (select product_name from products where id=transactions.product_id) as product_name, quantity,price,order_id,order_status,date_format(order_time,'%d-%b-%Y') as order_date,date_format(order_time,'%T') as order_time,tracking_id
                  ,a1.name as billing_name,a1.street_name as billing_street_name,a1.pincode as billing_pincode,
                  (select city_name from city where id=a1.city) as billing_city,
                  (select state_name from state where id=a1.state) as billing_state,
                  (select country_name from country where id=a1.country) as billing_country,
                  a2.name as delivery_name,a2.street_name as delivery_street_name,a2.pincode as delivery_pincode,
                  (select city_name from city where id=a2.city) as dcity,
                  (select state_name from state where id=a2.state) as dstate,
                  (select country_name from country where id=a2.country) as dcountry
                  from transactions,address a1,address a2 where transactions.user_id=? and a1.id=transactions.billing_address and a2.id=transactions.delivery_address",[$id]);
      $reviews = DB::select("select id,content,(select product_name from products where id=product_review.product_id) as product_name,approved from product_review where created_by=?",[$id]);
      return view('admin.user',['user'=>$user[0],'orders'=>$orders,'reviews'=>$reviews]);
    }else {
      return redirect('/admin/login');
    }
  }

}

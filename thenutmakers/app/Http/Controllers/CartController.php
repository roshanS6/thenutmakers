<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Cart;
use App\Products;

class CartController extends Controller {

  public function addProductToCart(Request $request) {
    try {
      if(!$request->session()->get('user_id')){
        return redirect('/signin');
      }else {
        $product_id = $request->input('product_id');
        $variety = $request->input('variety');
        $quantity = $request->input('quantity');
        $user_id = $request->session()->get('user_id');
        if($variety == 'null') {
          $product_stock = DB::select('select stock from products where id=?',[$product_id]);
          if($quantity<=$product_stock) {
            $cart_data = DB::select('SELECT * from cart where product_id=? and variety IS NULL and user_id=?',[$product_id,$user_id]);
            if(count($cart_data)>0) {
              $update_cart = DB::update('update cart set cart_quantity=? where product_id=? and user_id=?',[$quantity,$product_id,$user_id]);
              $request->session()->put('cart_count', true);
              $request->session()->put('message',"Added to the cart");
              return redirect($request->headers->get('referer'));
            }else {
              $add_cart = DB::insert('insert into cart (product_id, cart_quantity, user_id) values (?, ?, ?)',[$product_id, $quantity, $user_id]);
              $request->session()->put('cart_count', true);
              $request->session()->put('message',"Added to the cart");
              return redirect($request->headers->get('referer'));
            }
          }else {
            $request->session()->put('message',"Greater than cart");
          }
        } else {
          $product_stock = DB::select('select stock from product_variety where id=?',[$variety]);
          if($quantity<=$product_stock) {
            $cart_data = DB::select('select * from cart where product_id=? and variety=? and user_id=?',[$product_id, $variety, $user_id]);
            if(count($cart_data)>0) {
              $update_cart = DB::update('update cart set cart_quantity=? where product_id=? and variety=? and user_id=?',[$quantity, $product_id, $variety, $user_id]);
              $request->session()->put('message',"Added to the cart");
              $request->session()->put('cart_count', true);
              return redirect($request->headers->get('referer'));
            }else {
              $add_cart = DB::insert('insert into cart (product_id, cart_quantity, variety, user_id) values (?, ?, ?, ?)',[$product_id, $quantity, $variety, $user_id]);
              $request->session()->put('message',"Added to the cart");
              $request->session()->put('cart_count', true);
              return redirect($request->headers->get('referer'));
            }
          }else {
            $request->session()->put('message',"Greater than cart");
          }
        }

      }
    }catch(Exception $e) {
      return view('error');
    }
  }

  public function deleteCart($id,Request $request) {
    try {
      $product_id = $id;
      $user_id = $request->session()->get('user_id');
      $delete = DB::delete('delete from cart where product_id=? and user_id=?', [$product_id, $user_id]);
      return redirect('/cart');
    }catch (Exception $e) {
      return view('error');
    }
  }

  public function deleteCartVariety($id, $variety, Request $request) {
    try {
      $product_id = $id;
      $user_id = $request->session()->get('user_id');
      $variety = $variety;
      // var_dump($variety, $product_id);
      // exit;
      $delete = DB::delete('delete from cart where product_id=? and variety=? and user_id=?', [$product_id, $variety, $user_id]);
      return redirect('/cart');
    }catch (Exception $e) {
      return view('error');
    }
  }

  public function viewCart(Request $request) {
    try{
      if($request->session()->has('user_id')){
        $k=0;
        $user_id = $request->session()->get('user_id');
        $cart_data = DB::select('SELECT * from cart car
                                inner join products product on car.product_id = product.id
                                where user_id=? and variety IS NULL', [$user_id]);
        $cart_variety_data = DB::select('SELECT * from cart car
                                        inner join products product on car.product_id = product.id
                                        inner join product_variety variety on car.variety = variety.id
                                        where user_id=?', [$user_id]);
        $cart = array();
        for($i=0; $i<count($cart_data);$i++) {
          $cart[$i]= $cart_data[$i];
          $k = $i;
          if ($i == (count($cart_data)-1)) {
            $k++;
          }
        }
        for($i=0 ; $i < count($cart_variety_data) ; $i++) {
          $cart[$k]= $cart_variety_data[$i];
          $k++;
        }
        if(count($cart)>0) {
          $request->session()->put('cart_count', true);
          return view('/cart',['cart_products'=>$cart]);
        } else {
          $request->session()->put('cart_count', null);
          return view('/cart',['cart_products'=>null]);
        }
      }else {
        return redirect('/signin');
      }
    }catch (Exception $e) {
      return view('error');
    }
  }

  // $response["product_array"] = array();

    // $items = $request->session()->get('Cart');


    // if (is_array($items) || is_object($items))
    // {
    //     foreach ($items as $item)
    //     {
    //       foreach($item as $key=>$product)
    //       {
    //         $products = Products::find($product['id']);
    //         $products['quantity'] = $product['quantity'];

    //         array_push($response["product_array"],$products);
    //       }
    //       break;
    //     }
    // }


    // if($request->session()->has('user_id'))
    // {
    //   $user_id=$request->session()->get('user_id');
      
    //   $addresses=DB::select("select * from address where user_id=?",[$user_id]);
    //   return view('cart',['addresses'=>$addresses,'response' => $response]);
    // }
    // else
    // {
    //   return view('cart',['addresses'=>array(),'response' => $response]);
    // }

  // public function refreshViewCart(Request $request){
  //   if($request->session()->has('user')){
  //     $user=$request->session()->get('user');
  //     $id=$user[0]->id;
  //     $addresses=DB::select("select * from address where user_id=?",[$id]);
  //     return view('ajax.viewcart',['addresses'=>$addresses]);
  //   }else{
  //     return view('ajax.viewcart',['addresses'=>array()]);
  //   }
  // }

  public function increaseCartQuantity($value, $id, Request $request) {
    if($request->session()->has('user_id')) {
      $user_id = $request->session()->get('user_id');
      $product_id = $id;
      $quantity = $value;
      $select = DB::select('select stock from products where id=?',[$product_id]);
      $stock = $select[0]->stock;
      if($quantity < $stock){
        ++$quantity;       
        $update = DB::update('UPDATE cart set cart_quantity=? where product_id=? and variety IS NULL and user_id=?',[$quantity, $product_id, $user_id]);
        return redirect('/cart');
      }else {
        return redirect('/cart')->with('error',"Can't increase! This is the maximum stock of the product");
      }
    }
  }

  public function increaseCartVarietyQuantity($value, $id, $variety, Request $request) {
    if($request->session()->has('user_id')) {
      $user_id = $request->session()->get('user_id');
      $product_id = $id;
      $quantity = $value;
      $select = DB::select('select stock from product_variety where id=? and product_id=?',[$variety, $product_id]);
      $stock = $select[0]->stock;
      if($quantity < $stock){
        ++$quantity;
        $update = DB::update('UPDATE cart set cart_quantity=? where product_id=? and variety=? and user_id=?',[$quantity, $product_id, $variety, $user_id]);
        return redirect('/cart');
      }else {
        return redirect('/cart')->with('error',"Can't increase! This is the maximum stock of the product");
      }
    }
  }

  public function decreaseCartQuantity($value, $id, Request $request) {
    if($request->session()->has('user_id')){
      $user_id = $request->session()->get('user_id');
      $product_id = $id;
      $quantity = $value;
      if($quantity > 1){
        --$quantity;       
        $update = DB::update('UPDATE cart set cart_quantity=? where product_id=? and variety IS NULL and user_id=?',[$quantity, $product_id, $user_id]);
      }
      return redirect('/cart');
    }
  }

  public function decreaseCartVarietyQuantity($value, $id, $variety, Request $request) {
    if($request->session()->has('user_id')){
      $user_id = $request->session()->get('user_id');
      $product_id = $id;
      $quantity = $value;
      if($quantity > 1){
        --$quantity;
        $update = DB::update('UPDATE cart set cart_quantity=? where product_id=? and variety=? and user_id=?',[$quantity, $product_id, $variety, $user_id]);
      }
      return redirect('/cart');
    }
  }

  public function checkout(Request $request) {
    if(!$request->session()->get('user_id')){
      return redirect('/signin');
    }else{
      $k = 0;
      $user_id=$request->session()->get('user_id');
      $cart_data = DB::select('SELECT * from cart car
                                inner join products product on car.product_id = product.id
                                and car.cart_quantity <= product.stock
                                where user_id=? and variety IS NULL', [$user_id]);
      $cart_variety_data = DB::select('SELECT * from cart car
                                inner join products product on car.product_id = product.id
                                inner join product_variety variety on car.variety = variety.id
                                and car.cart_quantity <= variety.stock
                                where user_id=?', [$user_id]);
      $cart = array();
      for($i=0; $i<count($cart_data);$i++) {
        $cart[$i]= $cart_data[$i];
        $k = $i;
        if ($i == (count($cart_data)-1)) {
          $k++;
        }
      }
      for($i=0 ; $i < count($cart_variety_data) ; $i++) {
        $cart[$k]= $cart_variety_data[$i];
        $k++;
      }
      // echo "<pre>";
      // print_r($cart);
      // exit;
      $constant = DB::select('select * from constants');
      if(count($cart)>0) {
        $addresses=DB::select("select * from address where user_id=? and billing_address = 0",[$user_id]);
        $user_detail = DB::select('select * from user where user_id=?',[$user_id]);
        session()->put('$cart_data');
        // echo "<pre>";
        // print_r($cart);
        // exit;
        return view('checkout',['constants'=>$constant, 'addresses'=>$addresses,'cart_products'=>$cart, 'user_details'=>$user_detail]);
      }else {
        return redirect('/cart')->with('error','Add the valid products');
      }
    }
  }

  public function payWithRazorpay(Request $request) {
    if($request->input('billing_checkbox')!=null) {
      $user_id = $request->session()->get('user_id');
      $active = 1;
      $k=0;
      $delivery_address_id = DB::select('SELECT address_id from address where active_address=? and user_id=?',[$active, $user_id]);
      $constant = DB::select('select igst_percent from constants');
      $igst_percent = $constant[0]->igst_percent;
      $product_igst_value = $igst_percent/100;
      $grand_total = 0;
      $payment_mode="online";
      if(count($delivery_address_id)>0) {
        // $billing_address = $request->input('billing_address');
        $billing_address = $delivery_address_id[0]->address_id;
        $delivery_address_id = $delivery_address_id[0]->address_id;
        $order_id= getRandomString(10);
        $tracking_id = getRandomString(10);
        $order_status = 0;
        $trasaction_status = 0;
        $cart_data= DB::select('SELECT * from cart car
                                inner join products product on car.product_id = product.id
                                and car.cart_quantity <= product.stock
                                where user_id=? and variety IS NULL', [$user_id]);
        $cart_variety_data = DB::select('SELECT * from cart car
                                        inner join products product on car.product_id = product.id
                                        inner join product_variety variety on car.variety = variety.id
                                        and car.cart_quantity <= variety.stock
                                        where user_id=?', [$user_id]);
        $cart = array();
        for($i=0; $i<count($cart_data);$i++) {
          $cart[$i]= $cart_data[$i];
          $k = $i;
          if ($i == (count($cart_data)-1)) {
            $k++;
          }
        }
        for($i=0 ; $i < count($cart_variety_data) ; $i++) {
          $cart[$k]= $cart_variety_data[$i];
          $k++;
        }
        if(count($cart)>0) {
          foreach($cart as $cart_product) {
            if($cart_product->cart_quantity <= $cart_product->stock) {
              $product_id = $cart_product->product_id;
              $stock = $cart_product->stock;
              $variety = $cart_product->package_size;
              $quantity = $cart_product->cart_quantity;
              $price = $cart_product->price;
              $page_total_amt = $request->input('page_total_amt');
              $delivery_cost = $request->input('delivery_cost');
              $igst_delivery = 0;
              $product_price = $price * $quantity;
              if ($page_total_amt > 450) {
                $delivery_cost = 0;
              }
              if ($product_id == 31) {
                $igst_product = $product_price * (5/100);
              } else {
                $igst_product = $product_price * $product_igst_value;
              }
              // $grand_total += $product_price + $igst_product;
              $grand_total += $product_price;
              $remaining_stock = $stock-$quantity;

              $sql = DB::insert('insert into orders (user_id, product_id, delivery_address, billing_address, variety, tracking_id, quantity, order_price, igst_product, grand_total, order_id, order_status, payment_mode, transaction_status)
                              values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
                              [$user_id, $product_id, $delivery_address_id, $billing_address, $variety, $tracking_id, $quantity, $product_price, $igst_product, $grand_total, $order_id, $order_status, $payment_mode,$trasaction_status] );
              // $delete = DB::delete('delete from cart where user_id=? and product_id=?',[$user_id, $product_id]);
            }
          }
          $grand_total += $delivery_cost + $igst_delivery;
          $round_grand_total = round($grand_total);
          $amount_in_paise = $round_grand_total * 100;
          $postdata = http_build_query(
            array(
                'amount' => $amount_in_paise,
                'currency' => 'INR',
                'payment_capture' => '1'
              )
          );
          $opts = array('http' =>
              array(
                  'method'  => 'POST',
                  'header'  => 'Content-type: application/x-www-form-urlencoded',
                  'content' => $postdata
              )
          );
    
          $context  = stream_context_create($opts);
      
          $result = file_get_contents('https://rzp_live_Nxc2lBQAAvL2ij:ZETrc7mMBs2L2wst8FEd3xwH@api.razorpay.com/v1/orders', false, $context);
          $razorpay_res = json_decode($result);
          
          $transaction = DB::update('update orders set razorpay_order_id=? where order_id=?',[$razorpay_res->id,$order_id]);
          if($transaction) {
            $delievery = DB::insert('insert into shipping (order_id, delivery_cost, igst_delivery) values(?,?,?)',[$order_id,$delivery_cost,$igst_delivery]);
            $order_data = DB::select('SELECT * from orders
                                    inner join products on orders.product_id = products.id
                                    where orders.order_id=?', [$order_id]);
            $addresses=DB::select("select * from address where user_id=?",[$user_id]);
            $user_detail = DB::select('select * from user where user_id=?',[$user_id]);
            $request->session()->put('success','Order Placed Successfully');
            // echo "<pre>";
            // print_r($order_data);
            // exit;
            return view('paywithrazorpay', ['order_data'=>$order_data, 'address'=>$addresses, 'user_data'=>$user_detail]);
          }else {
            $request->session()->put('message', 'Failed to checkout');
            return back();
          }
        }else {
          return redirect ('/cart')->with('message',"Add valid product to the cart");
        }
      }else {
        return redirect ('/checkout')->with('message',"You doesn't select any address");
      }
    } else {
      $fname = $request->input('billing_first_name');
      $lname = $request->input('billing_last_name');
      $phone = $request->input('billing_phone');
      $address_line_1 = $request->input('billing_address_line_1');
      $address_line_2 = $request->input('billing_address_line_2');
      $country = $request->input('billing_country');
      $city = $request->input('billing_city');
      $state = $request->input('billing_state');
      $zip = $request->input('billing_zip');
      $user_id = $request->session()->get('user_id');
      $billing_address = 1;
      $active = 1;
      $k=0;
      $delivery_address_id = DB::select('SELECT address_id from address where active_address=? and user_id=?',[$active, $user_id]);
      $constant = DB::select('select igst_percent from constants');
      $igst_percent = $constant[0]->igst_percent;
      $product_igst_value = $igst_percent/100;
      $grand_total = 0;
      $payment_mode="online";      
      $sql = DB::insert('insert into address (address_fname, address_lname, address_line_1, address_line_2, country, city, state, zip, phone, billing_address, user_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',[$fname, $lname, $address_line_1, $address_line_2, $country, $city, $state, $zip, $phone, $billing_address, $user_id]);
      if($sql) {
        $billing_sql = DB::select('SELECT address_id from address ORDER BY address_id DESC limit 1');
      }
      if(count($delivery_address_id)>0) {
        // $billing_address = $request->input('billing_address');
        $billing_address_id = $billing_sql[0]->address_id;
        $delivery_address_id = $delivery_address_id[0]->address_id;
        $order_id= getRandomString(10);
        $tracking_id = getRandomString(10);
        $order_status = 0;
        $trasaction_status = 0;
        $cart_data= DB::select('SELECT * from cart car
                                inner join products product on car.product_id = product.id
                                and car.cart_quantity <= product.stock
                                where user_id=? and variety IS NULL', [$user_id]);
        $cart_variety_data = DB::select('SELECT * from cart car
                                        inner join products product on car.product_id = product.id
                                        inner join product_variety variety on car.variety = variety.id
                                        and car.cart_quantity <= variety.stock
                                        where user_id=?', [$user_id]);
        $cart = array();
        for($i=0; $i<count($cart_data);$i++) {
          $cart[$i]= $cart_data[$i];
          $k = $i;
          if ($i == (count($cart_data)-1)) {
            $k++;
          }
        }
        for($i=0 ; $i < count($cart_variety_data) ; $i++) {
          $cart[$k]= $cart_variety_data[$i];
          $k++;
        }
        if(count($cart)>0) {
          foreach($cart as $cart_product) {
            if($cart_product->cart_quantity <= $cart_product->stock) {
              $product_id = $cart_product->product_id;
              $stock = $cart_product->stock;
              $variety = $cart_product->package_size;
              $quantity = $cart_product->cart_quantity;
              $price = $cart_product->price;
              $page_total_amt = $request->input(('page_total_amt'));
              $delivery_cost = $request->input('delivery_cost');
              $igst_delivery = 0;
              $product_price = $price * $quantity;
              if ($page_total_amt > 450) {
                $delivery_cost = 0;
              }
              $igst_product = $product_price * $product_igst_value;
              // $grand_total += $product_price + $igst_product;
              $grand_total += $product_price;
              $remaining_stock = $stock-$quantity;

              $sql = DB::insert('insert into orders (user_id, product_id, delivery_address, billing_address, variety, tracking_id, quantity, order_price, igst_product, grand_total, order_id, order_status, payment_mode, transaction_status)
                              values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
                              [$user_id, $product_id, $delivery_address_id, $billing_address_id, $variety, $tracking_id, $quantity, $product_price, $igst_product, $grand_total, $order_id, $order_status, $payment_mode,$trasaction_status] );
              // $delete = DB::delete('delete from cart where user_id=? and product_id=?',[$user_id, $product_id]);
            }
          }
          $grand_total += $delivery_cost + $igst_delivery;
          $round_grand_total = round($grand_total);
          $amount_in_paise = $round_grand_total * 100;
          $postdata = http_build_query(
            array(
                'amount' => $amount_in_paise,
                'currency' => 'INR',
                'payment_capture' => '1'
              )
          );
          $opts = array('http' =>
              array(
                  'method'  => 'POST',
                  'header'  => 'Content-type: application/x-www-form-urlencoded',
                  'content' => $postdata
              )
          );
    
          $context  = stream_context_create($opts);
      
          $result = file_get_contents('https://rzp_live_Nxc2lBQAAvL2ij:ZETrc7mMBs2L2wst8FEd3xwH@api.razorpay.com/v1/orders', false, $context);
          $razorpay_res = json_decode($result);
          
          $transaction = DB::update('update orders set razorpay_order_id=? where order_id=?',[$razorpay_res->id,$order_id]);
          if($transaction) {
            $delievery = DB::insert('insert into shipping (order_id, delivery_cost, igst_delivery) values(?,?,?)',[$order_id,$delivery_cost,$igst_delivery]);
            $order_data = DB::select('SELECT * from orders
                                    inner join products on orders.product_id = products.id
                                    where orders.order_id=?', [$order_id]);
            $addresses=DB::select("select * from address where user_id=?",[$user_id]);
            $user_detail = DB::select('select * from user where user_id=?',[$user_id]);
            $request->session()->put('success','Order Placed Successfully');
            return view('paywithrazorpay', ['order_data'=>$order_data, 'address'=>$addresses, 'user_data'=>$user_detail]);
          }else {
            $request->session()->put('message', 'Failed to checkout');
            return back();
          }
        }else {
          return redirect ('/cart')->with('message',"Add valid product to the cart");
        }
      }else {
        return redirect ('/checkout')->with('message',"You doesn't select any address");
      }
    }
  }

  public function ordercheckout(Request $request) {
    $user_id=$request->session()->get('user_id');
    $razorpay_payment_id = $request->input('razorpay_payment_id');
    $razorpay_order_id = $request->input('razorpay_order_id');
    $razorpay_signature = $request->input('razorpay_signature');
    $order_id = $request->input('order_id');
    $order_status_email = "placed";
    $order_status = 1;
    $k=0;
    $update_variety = false;
    $delete_variety = false;
    $update = false;
    $delete = false;
    $transaction_status = 1;
    $user_detail = DB::select('SELECT * from user where user_id=?', [$user_id]);
    $user_email = $user_detail[0]->email;
    $order_update = DB::update('update orders set transaction_status=? ,order_status=?, razorpay_payment_id=?, payment_signature=? where order_id =?', [$transaction_status, $order_status, $razorpay_payment_id, $razorpay_signature, $order_id]);
    $cart_data= DB::select('SELECT * from cart car
                              inner join products product on car.product_id = product.id
                              and car.cart_quantity <= product.stock
                              where user_id=? and variety IS NULL', [$user_id]);
    $cart_variety_data = DB::select('SELECT * from cart car
                              inner join products product on car.product_id = product.id
                              inner join product_variety variety on car.variety = variety.id
                              and car.cart_quantity <= variety.stock
                              where user_id=?', [$user_id]);
    $cart_products = array();
    for($i=0; $i<count($cart_data);$i++) {
      $cart_products[$i]= $cart_data[$i];
      $k = $i;
      if ($i == (count($cart_data)-1)) {
        $k++;
      }
    }
    for($i=0 ; $i < count($cart_variety_data) ; $i++) {
      $cart_products[$k]= $cart_variety_data[$i];
      $k++;
    }         
    if(count($cart_products)>0) {
      foreach( $cart_products as $cart_product) {
        $product_id = $cart_product->product_id;
        $variety = $cart_product->variety;
        $stock = $cart_product->stock;
        $quantity = $cart_product->cart_quantity;
        $remaining_stock = $stock-$quantity;
        if ($variety == 'null' || $variety == null){
          $update = DB::update('UPDATE products set stock=? where id=?', [$remaining_stock, $product_id]);
          $delete = DB::delete('DELETE from cart where user_id=? and product_id=? and variety is null',[$user_id, $product_id]);
        } else {
          $update_variety = DB::update('UPDATE product_variety set stock=? where id =? and product_id =?', [$remaining_stock, $variety, $product_id]);
          $delete_variety = DB::delete('DELETE from cart where user_id=? and product_id=? and variety = ?',[$user_id, $product_id, $variety]);
        }
      }
      if(($update && $delete) || ($update_variety && $delete_variety)) {
        sendOrderEmail($user_email, $order_id, $order_status_email);
        sendNewOrderEmail($order_id);
        $request->session()->put('success','Order Placed Successfully');
        $request->session()->put('order_id',$order_id);
        return redirect('/placedorders');
      }else {
        $request->session()->put('message', 'Failed to checkout');
        return back();
      }
    }else {
      return redirect ('/cart')->with('message',"Add valid product to the cart");
    }
  }

  public function changedeliveryaddress(Request $request) {
    $user_id = $request->session()->get('user_id');
    $address_id = $request->input('del_address');
    $active = 1;
    $active_address = DB::select('select address_id, active_address from address where active_address=? and user_id=?',[$active, $user_id]);
    if(count($active_address)<=1) {
      if(count($active_address)!=0) {
        $replace_value =  0;
        $replace_id = $active_address[0]->address_id;
        $update = DB::update('update address set active_address=? where address_id=?',[$replace_value, $replace_id]);
      }
      $update2 = DB::update('update address set active_address=? where address_id=?',[$active, $address_id]);
      return redirect('/checkout');
    }
  }

  // public function test (Request $request) {
  //   sendOrderEmail('harisvar@shade6.com', '9COv88fEKO', 'Placed');
  //   var_dump('sent');
  //   exit;
  // }

  public function placedorders(Request $request) {
    $user_id=$request->session()->get('user_id');
    $order_id = $request->session()->get('order_id');

    $order_status = 1;
    $k=0;
    $order_data= DB::select('SELECT * From products p 
                      INNER JOIN orders o on p.id = o.product_id where user_id=? and o.order_id=?',[$user_id, $order_id]);
    // $order_variety= DB::select('SELECT * From products p
    //                   INNER JOIN orders o on p.id = o.product_id
    //                   INNER JOIN product_variety on o.variety = product_variety.id
    //                   where user_id=? and o.order_id=?',[$user_id, $order_id]);
    // $orders = array();
    // for($i=0; $i<count($order_data);$i++) {
    //   $orders[$i]= $order_data[$i];
    //   $k = $i;
    //   if ($i == (count($order_data)-1)) {
    //     $k++;
    //   }
    // }
    // for($i=0 ; $i < count($order_variety) ; $i++) {
    //   $orders[$k]= $order_variety[$i];
    //   $k++;
    // } 
    $shipping = DB::select('select * from shipping where order_id=?', [$order_id]);
    $constants = DB::select('select * from constants');
    // echo "<pre>";
    // print_r($order_data); 
    // print_r($shipping);
    // exit;
    if(count($order_data)>0) {
      $request->session()->forget('order_id');
      return view('placedorders',['orders'=>$order_data,'constants'=>$constants, 'shipping'=>$shipping]);
    }else {
      $request->session()->put('message','no orders');
      return redirect('/cart');
    }
  }

}
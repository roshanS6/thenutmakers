<?php
namespace App\Http\Controllers;
use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;

class ProductController extends Controller {

  public function products(Request $request) {
    $sql1  = "select id, category_name from major_categories";
    $sql2  = "select id, category_name, major_id from sub_categories";
    $sql3  = "select * from products";
    $major_categories = DB::select($sql1);
    $sub_categories = DB::select($sql2);
    $products = DB::select($sql3);
    $constant = DB::select('select * from constants');
    return view('products', ['major_categories'=>$major_categories,'constants'=>$constant, 'sub_categories'=>$sub_categories, 'products'=>$products]);
  }
}

<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Session;
use Validator;
use Response;

class AdminController extends Controller {

  public function MainPage(Request $request) {      
    if($request->session()->has('admin_id')){
      $categories = DB::select("SELECT count(cat_id) as count FROM major_categories");
      $products = DB::select("SELECT count(id) as count FROM products");
      $enquiry = DB::select("SELECT count(id) as count FROM enquiries");
      $orders = DB::select("SELECT count(id) as count FROM orders where transaction_status = 1");
      $latestorders = DB::select(
        "SELECT orders.id, orders.order_id, orders.order_time, orders.order_status, user.first_name, user.last_name, user.user_id, GROUP_CONCAT(orders.order_price) as order_price, GROUP_CONCAT(products.product_name) as group_product, orders.transaction_status FROM `orders`
        Left JOIN user on user.user_id = orders.user_id 
        inner join products on products.id = orders.product_id where orders.transaction_status = 1 group by orders.order_id ORDER BY orders.id DESC limit 10");
      return view('admin.main',['categories'=> $categories[0],'products'=> $products[0],'enquiry'=> $enquiry[0],'orders'=>$orders[0],'latestorders'=>$latestorders]);
    }else {
      return redirect('/admin/login');
    }
  }

  public function add_banner(Request $request) {
    if($request->session()->has('admin_id')) {
      return view ('admin.add_banner');
    }else {
      return redirect('/admin/login');
    }
  }

  public function insert_banner(Request $request) {
    if($request->session()->has('admin_id')) {
      $title = $request->input('title');
      $description = $request->input('description');
      $destinationPath = 'images/products/';
      $banner_image = $request->file('banner_image'); // main image upload
      $banner_image_name = $banner_image->getClientOriginalName();
      $file_path = str_replace(" ","-",$banner_image_name);
      $upload_success = $banner_image->move($destinationPath, $file_path);
      $banner_img = $destinationPath."".$file_path;
      $products = DB::insert('INSERT INTO banners (title, description, banner_image) VALUES (?, ?, ?)', [$title, $description, $banner_img]);
      if($products) {
        return redirect('admin/add_banner')->with('message', 'Banner added Successfully');
      } else {
        return redirect('admin/add_banner')->with('error', 'Banner Could not be added');
      }
    }else {
      return redirect('/admin/login');
    }
  }

  public function banners(Request $request) {
    if($request->session()->has('admin_id')) {
      $banners = DB::select('SELECT * from banners');
      return view ('admin.banners', ['banners'=>$banners]);
    }else {
      return redirect('/admin/login');
    }
  }

  public function get_banner($id, Request $request) {
    if ($request->session()->has('admin_id')) {
      $banner = DB::select('SELECT * from banners where id=?',[$id]);
      return view ('admin.banner', ['banner'=>$banner]);
    }else {
      return redirect('/admin/login');
    }
  }

  public function update_banner(Request $request) {
    if ($request->session()->has('admin_id')) {
      $id = $request->input('id');
      $title = $request->input('title');
      $description = $request->input('description');
      $img = $request->file('banner_image');
      if ($img != null){ 
        $destinationPath = 'images/products/';
        $banner_image = $request->file('banner_image'); // main image upload
        $banner_image_name = $banner_image->getClientOriginalName();
        $file_path = str_replace(" ","-",$banner_image_name);
        $upload_success = $banner_image->move($destinationPath, $file_path);
        $banner_img = $destinationPath."".$file_path;
        $update = DB::update('UPDATE banners set title=?, description=?, banner_image=? where id=?', [$title, $description, $banner_img, $id]);
        if ($update) {
          return redirect('/admin/banners')->with('message', 'Banner Update Successful');
        } else {
          return redirect('/admin/banner/get/'.$id)->with('error', "Can't update Banner");
        }
      }
      $update = DB::update('UPDATE banners set title=?, description=? where id=?', [$title, $description, $id]);
      if ($update) {
        return redirect('/admin/banners')->with('message', 'Banner Update Successful');
      } else {
        return redirect('admin/banner/get/'.$id)->with('error', 'Banner Update Successful');
      }
    } else {
      return redirect('/admin/login');
    }
  }

  public function delete_banner($id, Request $request) {
    $delete = DB::DELETE('DELETE FROM banners WHERE id = ?',[$id]);
    if ($delete) {
      return redirect('/admin/banners')->with('message', "Deleted");
    } else {
      return redirect('/admin/banners')->with('error', "Can't Delete");
    }
  }

  public function categories(Request $request) {
    if($request->session()->has('admin_id')){
      $sql = "SELECT * FROM major_categories";
      $category = DB::select($sql);
      return view('admin.categories',['category'=> $category]);
    }else {
      return redirect('/admin/login');
    }
  }

  public function addmaincategory(Request $request) {
    if($request->session()->has('admin_id')){
      return view('admin.addmaincategory');
    }else {
      return redirect('/admin/login');
    }
  }

  public function insertmaincategory(Request $request) {
    $maincategory = ucfirst($request->input('main_category'));
    $description = $request->input('description');
    $categories = DB::insert('insert into major_categories (category_name,description) values(?,?)',[$maincategory,$description]);    
    return redirect('/admin/maincategory/add')->with('message','Main category added Successfully');
  }

  public function getCategory($id, Request $request) {
    try {
      if($request->session()->has('admin_id')){
        $categories = DB::SELECT('SELECT * FROM major_categories WHERE cat_id=?',[$id]);
          if (count($categories)>0) {
            return view('admin.category', ['category'=>$categories[0]]);
          }else{
            return view('admin.404');
          }
      }else {
        return redirect('/admin/login');
      }
    }catch (\Exception $e) {
      return view('admin.404');
    }
  }

  public function updateCategory($id, Request $request) {
    try {
      $category_name = $request->input('category_name');
      $old_category_name = $request->input('old_category_name');
      $category_description = $request->input('category_description');
      $saved = DB::UPDATE('UPDATE `major_categories` SET `category_name`= ?,`description`=? WHERE cat_id = ?',[$category_name,$category_description,$id]);
      $products = DB::update('UPDATE products set category_name = ? where category_name=?',[$category_name, $old_category_name]);
      if ($saved) {
        return redirect('admin/maincategory')->with('message', 'Category Updated Successfully');
      }else {
        return redirect('admin/maincategory')->with('message', 'Category Could not be Updated');
      }
    }catch (\Exception $e) {
      return view('admin.404');
    }
  }

  public function deleteCategory($id, Request $request) {
    try {
      $delete = DB::DELETE('DELETE FROM `major_categories` WHERE cat_id = ?',[$id]);
      if ($delete) {
        return redirect('admin/maincategory')->with('message', 'Category Deleted Successfully');
      }else {
        return redirect('admin/maincategory')->with('message', 'Category Could not be Deleted');
      }
    } catch (\Exception $e) {
      return view('admin.404');
    }
  }

  public function viewUser(Request $request) {
    if($request->session()->has('admin_id')){
      $sql = "select * from user";
      $user = DB::select($sql);
      return view('admin.viewuser',['users'=> $user]);    
    }else{
      return redirect('/admin/login');
    }
  }

  public function viewSubscribers(Request $request) {
    if($request->session()->has('admin_id')){
      $sql = "select * from subscribers";
      $user = DB::select($sql);
      return view('admin.viewsubscribers',['users'=> $user]);    
    }else{
      return redirect('/admin/login');
    }
  }

  public function userDetails($id, Request $request) {
    if($request->session()->has('admin_id')){
      $sql="select * from user where user_id=?";
      $ad = "select * from address where user_id=?";
      $display = DB::select($sql,[$id]);
      $address = DB::select($ad,[$id]);

      // $sql1="select a.address_line_1 from address as a
      //         inner join orders as o on o.user_id = a.user_id where a.user_id=?";
      // $display1 = DB::select($sql1,[$id]);
      
      // $sql2="select * from orders as o
      //         inner join products as p on o.product_id = p.id
      //         where user_id=? ";
      // $display2 = DB::select($sql2,[$id]);
      
      $history = DB::select("SELECT *, COUNT(orders.Order_id) AS NumberOfOrders, GROUP_CONCAT(products.product_name) as group_product, GROUP_CONCAT(orders.quantity) as group_quantity from products
                    INNER JOIN orders on products.id = orders.product_id where orders.user_id=?
                    group by orders.order_id ORDER BY orders.order_time DESC",[$id]);
    
      $delivery_address = DB::select("Select address.address_id, address.address_line_1, address.address_line_2 from address INNER JOIN orders on orders.delivery_address = address.address_id where address.user_id=?",[$id]);

      $billing_address = DB::select("Select address.address_id, address.address_line_1, address.address_line_2 from address INNER JOIN orders on orders.billing_address = address.address_id where address.user_id=?",[$id]);

      return view('admin.userdetails',['users'=> $display, 'user_address'=>$address, 'orderdetails'=>$history,'delivery_address'=>$delivery_address,"billing_address"=>$billing_address]);
  
    }else{
      return redirect('/admin/login');
    }

  }

  public function constant(Request $request) {
    if($request->session()->has('admin_id')) {
      $sql = DB::select('select * from constants');    
      return view('admin.constants', ['constant'=>$sql]);
    }else{
      return redirect('/admin/login');
    }
  }

  public function updateConstant($id, Request $request) {
    $email = $request->input('email');
    $phone = $request->input('phone');
    $address = $request->input('address');
    $shipping_cost = $request->input('shipping');
    $igst_percent = $request->input('igst_percentage');
    $state = $request->input('state');
    $sql = DB::update('update constants set email=?, phone=?, address=?, shipping_cost=?, state=?, igst_percent=? where id= ?',[$email, $phone, $address, $shipping_cost, $state, $igst_percent, $id]);
    $constant = DB::select('select * from constants');
    return redirect('admin/constants')->with('message',"Updated");
  }

  public function insertConstant(Request $request) {
    $email = $request->input('email');
    $phone = $request->input('phone');
    $address = $request->input('address');
    $shipping_cost = $request->input('shipping');
    $igst_percent = $request->input('igst_percentage');
    $state = $request->input('state');
    $sql = DB::insert('insert into constants (email,phone,address,shipping_cost,state,igst_percent)
                                              values (?,?,?,?,?,?)',[$email, $phone,$address, $shipping_cost, $state, $igst_percent]);
    $constant = DB::select('select * from constants');
    return redirect('admin/constants')->with('message',"Inserted");    
  }
    
  public function viewtestimonial(Request $request) {
    if($request->session()->has('admin_id')){
      $sql = DB::select('select * from testimonial');
      if($sql){
        return view('admin.viewtestimonial',['testimonials'=>$sql]);
      }
    }else{
      return redirect('/admin/login');
    }
  }

  public function addtestimonial(Request $request) {
    if($request->session()->has('admin_id')){
      return view('admin.addtestimonial');
    }else{
      return redirect('/admin/login');
    }
  }

  public function insertTestimonial(Request $request) {    
    $status = $request->input('status');
    $content = $request->input('content');
    $name = $request->input('name');
    $sql = DB::insert('insert into testimonial (content, status, name) values (?, ?, ?)',[$content, $status, $name]);
    if($sql){
      $request->session()->put('message',"Testimonial Added");
      return redirect('admin/testimonial');
    }
  }

  public function updateTestimonial($value, $id, Request $request) {    
    $sql = DB::update('UPDATE testimonial set status = ? where id =?',[$value, $id]);
    if($sql){
      $request->session()->put('message',"Testimonial Added");
      return redirect('admin/testimonial');
    }
  }


  /*  public function insertUser(Request $request)
    {
      $input=$request->all();
      $images=array();
      if($files=$request->file('images')){
        foreach($files as $file){
          $name=$file->getClientOriginalName();

          $filename = pathinfo($name, PATHINFO_FILENAME);
          $extension = pathinfo($name, PATHINFO_EXTENSION);
          $ext = strtolower($extension);
          $file_name = str_replace(" ","-",$filename);
          $file_name = $file_name.".".$ext;
          $file->move("images/works",$file_name);
          $file_name = "images/works/".$file_name;

          // try {
          //     // Use the Tinify API client.
          //     \Tinify\setKey("W4c6qytuQCoYgxjOONesxXE3qiHWOheh");
          //     $source = \Tinify\fromFile($file_name);
          //     $source->toFile($file_name);
          // } catch(\Tinify\AccountException $e) {
          //     print("The error message is: " . $e->getMessage());
          //     // Verify your API key and account limit.
          // } catch(\Tinify\ClientException $e) {
          //     // Check your source image and request options.
          // } catch(\Tinify\ServerException $e) {
          //     // Temporary issue with the Tinify API.
          // } catch(\Tinify\ConnectionException $e) {
          //     // A network connection error occurred.
          // } catch(Exception $e) {
          //     // Something else went wrong, unrelated to the Tinify API.
          // }
          if ($ext=='jpeg' || $ext=='png' || $ext=='jpg' ) {
            $type="image";
          }else{
            $type="video";
          }
          $category_name = $input['category_name'];
          $work_name = $input['workname'];
          $insert =  DB::insert('insert into our_works (category_name, type, work_name, url) values(?,?,?,?)',[$category_name,$type,$work_name,$file_name]);
        }
      }
      if($insert){
        return back()->with('message', 'Work Inserted Successfully');
      }else{
        return back()->with('message','Work could not be added');
      }
    }*/


  public function aboutUs(Request $request) {
    if($request->session()->has('admin_id')) {
      $aboutus = DB::SELECT('SELECT * FROM about_us');
      return view('admin.aboutus', ['aboutus'=>$aboutus[0]]);
    }else{
      return redirect('/admin/login');
    }
  }

  public function  updateAboutUs($id,Request $request) {
    try{
      // $single_about = $request->input('single_about');
      $aboutus_temp = $request->input('aboutus');
      $aboutus = str_replace("color: #333333","color: #8A8A8A", $aboutus_temp);
      $about_image = $request->file('about_image');
      if($about_image==null) {
        $sql = "update about_us set about_us=? where id=?";
        DB::update($sql, [$aboutus, $id]);
      }else {
        $destinationPath = 'images/';
        $fileName = $about_image->getClientOriginalName();
        $file_path = str_replace(" ","-",$fileName);
        $upload_success = $about_image->move($destinationPath, $file_path);
        $sql = "update about_us set about_us=?, image_path=? where id=?";
        DB::update($sql, [$aboutus, $destinationPath."".$file_path, $id]);
      }
      return redirect('/admin/aboutus')->with('message','About detail has updated Successfully');
    }
    catch(Exception $e) {
      return back();
    }
  }

  public function enquiries(Request $request) {
    if ($request->session()->has('admin_id')) {
      $all_enquiries = DB::SELECT('SELECT * FROM enquiries');
      return view('admin.enquiries', ['enquiries'=>$all_enquiries]);
    } else {
      return redirect('/admin/login');
    }
  }

  public function updateEnquiryStatus($value, $id, Request $request) {
    if ($request->session()->has('admin_id')) {
      $update_enquiry = DB::update('UPDATE enquiries set status=? where id=?', [$value, $id]);
      $select = DB::select('SELECT * from enquiries where id=?', [$id]);
      if ($update_enquiry) {
        if ($value == 1) {
          enquiryAckMail($select[0]->email);
        }
        return back()->with('message', 'Updated the enquiry status');
      } else {
        return back()->with('error', "Can't update");
      }
    } else {
      return redirect('/admin/login');
    }
  }  
}

<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="{{ URL::asset('css/view_bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/view_style.css') }}">
  <link href="https://fonts.googleapis.com/css?family=Bai+Jamjuree|Source+Serif+Pro" rel="stylesheet">
  <title>The Nut Makers</title>
</head>

<body>
  <header class="header-section" id="header-1">
    <!-- Fixed navbar -->
    <nav class="navbar navbar-expand-md navbar-dark fixed-top header header-nav" id="myHeader">
      <a class="navbar-brand" href="{{ url('/') }}">
        <img src="{{ url::asset('images/logo.png') }}" width="100" height="100">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      <div class="collapse navbar-collapse justify-content-end" id="navbarCollapse">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/') }}">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/product') }}">Shop</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/aboutus') }}">About Us</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Contact</a>
          </li>
          <li class="nav-item svg-icon search-button">
            <a href="#" class="search-toggle" data-selector="#header-1">
                  <img src="{{ URL::asset('images/search.svg') }}">
              </a>
          </li>
          <li class="nav-item svg-icon">
            <?php
            if(!Session::has('user_id')){
             ?>
              <a class="nav-link" href="{{ url('/profile') }}">
              <img src="{{ URL::asset('images/user.svg') }}">
            </a>
            <?php
            }
            else{
              ?>            
            <div class="user-area dropdown">
            <a href="#" class="nav-link" data-toggle="dropdown" aria-expanded="false">
                     <img src="{{ URL::asset('images/user.svg') }}" >
                     </a> 
                     <div class="user-menu dropdown-menu">
                     <a class="nav-link" href="{{ url('/profile') }}" style="color:black">My profile</a>                      
                        <a class="nav-link" href="{{ url('/logout') }}" style="color:black">Logout</a>
                     </div>
            </div>
            <?php
            }
            ?>
          </li>
          <li class="nav-item svg-icon active">
            <a class="nav-link" href="{{ url('/cart') }}">
              <img src="{{ URL::asset('images/shopper.svg') }}">
            </a>
            <?php
            if(Session::has('cart_count')) {
             ?>
            <span class="dot"> </span>
            <?php 
              }
            ?>
          </li>
        </ul>
        <form action="{{ url('search') }}" class="search-box">
          <input type="text" class="text search-input" name="key" placeholder="Type here to search..." />
        </form>
      </div>
    </nav>
  </header>
  <main role="main" class="wrapper">
    <div class="container-fluid">

      <div class="row justify-content-center wrapper-section">
        <div class="col-10 col-lg-6 col-md-10 col-sm-10">
          <div class="order-title-section">
            <img src="{{ url::asset('images/success.svg') }}" width="30" height="30">
            <h2>Order Placed</h2>
            <p>Thank you for your order. Your order has been successfully placed.</p>
            <h3 style="color: #b48a34; font-family: 'Source Serif Pro', serif;">Order id: #{{ $shipping[0]->order_id }}</h3>
          </div>
          <!-- <div class="row">
            <div class="col-6 col-lg-3 col-md-3 col-sm-3">
              <div class="order-product">
                <img src="images/Salted-Flavour.png" class="w-100">
              </div>
            </div>
          </div> -->
          <div class="row">
          <div class="shopping-cart">

            <div class="column-labels row">
              <label class="product-image">Image</label>
              <label class="product-details">Product</label>
              <label class="product-price">Price</label>
              <label class="product-quantity" style="text-align:center;">QTY</label>
              <label class="product-line-price">Total(with tax)</label>
              <label class="product-removal">Remove</label>
            </div>
            @php
            $total_amount=0;
            $grand_total=0;
            $igst_product=0;
            $igst_shipping=0;
            $shipping_cost=0;
            @endphp
          @foreach($orders as $order)
          <div class="product row align-items-center">
                <div class="product-image">
                  <img src="{{ url::asset($order->image_path) }}">
                </div>
                <div class="product-details">
                  <div class="product-title">{{ $order->product_name }}</div>
                  <p class="product-description">{{ $order->variety }}</p>
                </div>
                <div class="product-price svg-icon price" id="price"><img src="{{ url::asset('images/rupee.svg') }}">{{ $order->price }}</div>
                
                <div class="product-quantity" style="color:white; text-align:center;">            
                    {{ $order->quantity }}                    
                </div>
                @php 
                $total_price=0;               
                $igst_product = $order->igst_product;
                $total_price+=$order->order_price + $igst_product;
                $total_amount += $total_price;
                @endphp
                <div class="product-line-price svg-icon total" id="total_price"><img class="svg-icon" src="{{ url::asset('images/rupee.svg') }}">{{ round($total_price) }}</div>
    
                
              </div>
          @endforeach

          <div class="product row align-items-center">
                <div class="product-image">
                  
                </div>
                <div class="product-details">
                  <div class="product-title">Shipping Cost</div>
                  <p class="product-description"></p>
                </div>
                <div class="product-price svg-icon price" id="price"><img src="{{ url::asset('images/rupee.svg') }}">{{ $shipping[0]->delivery_cost }}</div>
                
                <div class="product-quantity" style="color:white; text-align:center;"></div>
                @php                
                $igst_shipping = $shipping[0]->igst_delivery;
                $shipping_cost = $shipping[0]->delivery_cost + $igst_shipping;
                $grand_total = $total_amount + $shipping_cost;
                @endphp
                <div class="product-line-price svg-icon total" id="total_price"><img class="svg-icon" src="{{ url::asset('images/rupee.svg') }}">{{ round($shipping_cost) }}</div>
    
                
              </div>
              <div class="totals">
                <div class="totals-item">
                  <label style="width:62%; font-size:15px">Grand total</label>
                  <div class="totals-value svg-icon net" id="cart-subtotal" style="margin-right: 13px; text-align: center; font-size:15px"><img class="svg-icon" src="{{ url::asset('images/rupee.svg') }}">
                 
                  <span class="netval">{{ round($grand_total) }}</span></div>
                </div>
              </div>
          </div>
        </div>
        </div>

      </div>
    </div>
  </main>

{{--   
  <div>
    <table id="bootstrap-data-table" class="table table-striped table-bordered">
                        <thead>
                           <tr>
                              <th>Order_Id</th>
                              <th>Product_name</th>
                              <th>Quantity</th>
                              <th>Order Time</th>
                              <th>Order Status</th>
                              
                             
                           </tr>
                        </thead>
                        <tbody>
                          @foreach ($orders as $order)
                              <tr>
                                <td>{{ $order->order_id }}</td>
                                <td>{{ $order->product_name }}</td>
                                <td>{{ $order->quantity }}</td>
                                <td>{{ $order->order_time }}</td>

                                <td> 
                      @if ($order->order_status==0)
                        <div class="label cancel" >Cancelled</div>
                      @elseif ($order->order_status==1)
                        <div class="label placed">Placed</div>
                      @elseif ($order->order_status==2)
                        <div class="label confirm">Confirmed</div>
                      @elseif ($order->order_status==3)
                        <div class="label dispatched">Dispatched</div>
                      @elseif ($order->order_status==4) 
                        <div class="label delivered">Delivered</div>
                      @elseif ($order->order_status==5)
                        <div class="label returned">Returned</div>
                      @endif
                    </td>
              </tr>
                          @endforeach
                        </tbody>
                     </table>
</div> --}}

  <script src="{{ URL::asset('js/jquery-3.2.1.slim.min.js') }}"></script>
  <script src="{{ URL::asset('js/popper.min.js') }}"></script>
  <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
<script>
    window.onscroll = function() {
      myFunction()
    };

    var header = document.getElementById("myHeader");
    var sticky = header.offsetTop;

    function myFunction() {
      if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
      } else {
        header.classList.remove("sticky");
      }
    }
  </script>
<script>
    $('.header').on('click', '.search-toggle', function(e) {
      var selector = $(this).data('selector');
      $(".search-input").show();
      $(selector).toggleClass('show').find('.search-input').focus();
      $(this).toggleClass('active');

      e.preventDefault();
    });
  </script>
</body>
</html>
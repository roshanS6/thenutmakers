<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="{{ URL::asset('css/view_bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/view_style.css') }}">
  <link href="https://fonts.googleapis.com/css?family=Bai+Jamjuree|Source+Serif+Pro" rel="stylesheet">
  <script src='https://www.google.com/recaptcha/api.js'></script>
  <title>The Nut Makers</title>
</head>

<body>
  <header class="header-section" id="header-1">
    <!-- Fixed navbar -->
    <nav class="navbar navbar-expand-md navbar-dark fixed-top header" id="myHeader">
      <a class="navbar-brand" href="{{ URL('/') }}">
        <img src="{{ URL::asset('images/logo.png') }}" width="100" height="100">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      <div class="collapse navbar-collapse justify-content-end" id="navbarCollapse">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="{{ URL('/') }}">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/product') }}">Shop</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/aboutus') }}">About Us</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/contactus') }}">Contact</a>
          </li>
          <li class="nav-item svg-icon search-button">
            <a href="#" class="search-toggle" data-selector="#header-1">
                  <img src="{{ URL::asset('images/search.svg') }}">
              </a>
          </li>
          <li class="nav-item svg-icon">
            <?php
            if(!Session::has('user_id')){
             ?>
              <a class="nav-link" href="{{ url('/profile') }}">
              <img src="{{ URL::asset('images/user.svg') }}">
            </a>
            <?php
            }
            else{
              ?>
            <div class="user-area dropdown">
            <a href="#" class="nav-link" data-toggle="dropdown" aria-expanded="false">
                     <img src="{{ URL::asset('images/user.svg') }}" >
                     </a>
                     <div class="user-menu dropdown-menu">
                     <a class="nav-link" href="{{ url('/profile') }}" style="color:black">My profile</a>
                        <a class="nav-link" href="{{ url('/logout') }}" style="color:black">Logout</a>
                     </div>
            </div>
            <?php
            }
            ?>
          </li>
          <li class="nav-item svg-icon">
            <a class="nav-link" href="{{ url('/cart') }}">
              <img src="{{ URL::asset('images/shopper.svg') }}">
            </a>
            <?php
            if(Session::has('cart_count')) {
             ?>
            <span class="dot"> </span>
            <?php
              }
            ?>
          </li>
        </ul>
        <form action="{{ url('search') }}" class="search-box">
          <input type="text" class="text search-input" name="key" placeholder="Type here to search..." />
        </form>
      </div>
    </nav>
  </header>




  <main role="main" class="wrapper">
    <div class="message" style="padding-top:1%">
      @if (Session('message'))
        <div class="alert alert-success alert-dismissible fade show" role="alert" style="font-weight: 400;">
            <span class="badge badge-pill" style="color:black">{{ Session('message') }}</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          {{ Session::forget('message') }}
        </div>
      @endif
      @if (Session('error'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert" style="font-weight: 400;">
            <span class="badge badge-pill" style="color:black">{{ Session('error') }}</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          {{ Session::forget('error') }}
        </div>
      @endif
    </div>
    <div class="container">

      <div class="row align-items-center wrapper-section">
        <div class="col">
          <h2>FAQ</h2>
          {{-- <div class="terms-list">
            <h4>Terms and Conditions</h4>
            <div class="terms-text">General Site Usage</div>
            <div class="terms-text">Last Revised: December 16,2017</div>
            <p>Welcom etowww.lorem -ipsum .info.This site is provided as a service to our visitors and may be used for informational purposes only.Because the Terms and Conditions contain legal obligations,please read them carefuly.</p>
          </div> --}}
          <div class="terms-list">
            <ol>
              <li>
                <h3>Availability</h3>
                <ol>
                  <li>
                    <p style="color: #d2d0d0;">Do you have The Nut Makers brand available in physical stores?</p>
                    <p>Yes! We are available across select outlets across the country. We are striving to get our products into more retail stores in the coming future.</p>
                  </li>
                </ol>
              </li>
              <li>
                <h3>Cancellation Policy</h3>
                <ol>
                  <li>
                    <p style="color: #d2d0d0;">How can I cancel my order?</p>
                    <p>If ever you decide to cancel the order, simply log on to your account and proceed with the cancellation on the same day.
                      <br>
                      You can also contact us at +918848950104/ 4742754200 or send a mail to info@thenutmakers.com
                    </p>
                  </li>
                </ol>
              </li>
              <li>
                <h3>Payment</h3>
                <ol>
                  <li>
                    <p style="color: #d2d0d0;">What payment options do you offer?</p>
                    <p>You can use Credit/Debit cards, Net Banking and Mobile Wallet.You can also pay using the UPI application/ QR Codes</p>
                  </li>
                  <li>
                    <p style="color: #d2d0d0;">Do you have a refund policy?</p>
                    <p>If the received product is spoilt or damaged, you can contact us immediately at +918848950104/ 4742754200 or send a mail to info@thenutmakers.com to facilitate a refund
                      <br>
                      For more details, please go through our Refund policy carefully.
                    </p>
                  </li>
                </ol>
              </li>
              <li>
                <h3>Products</h3>
                <ol>
                  <li>
                    <p style="color: #d2d0d0;">Who are the Nut Makers.</p>
                    <p>The Nut Makers is a premium quality brand of nuts from Vijaylaxmi Cashews (VLC). Established in 1957 by Mr K. Ravindranathan Nair, VLC is India’s largest manufacturer and exporter of cashews, renowned for their consistent standards of unsurpassed quality.
                      <br>
                      The Nut Makers, from VLC is a premium product for Nuts, for those seeking superior flavours and high-quality ingredients.
                    </p>
                  </li>
                  <li>
                    <p style="color: #d2d0d0;">Where are the processing and packaging done?</p>
                    <p>From sourcing of the nuts from farms in India, Indonesia and Africa to the processing and packaging in our in-house factory, quality monitoring has been established and constantly monitored.
                      <br>
                      At The Nut Makers, each stage of the process is monitored to ensure only the best arrives on the shelves. Our current processing facility is situated in Kanyakumari District of Tamil Nadu.
                    </p>
                  </li>
                  <li>
                    <p style="color: #d2d0d0;">What is the average shelf life of your products?</p>
                    <p>Our shelf life ranges from 6-12 month depending on the product range.</p>
                  </li>
                  <li>
                    <p style="color: #d2d0d0;">How can I store the products?</p>
                    <p>Store them in a cool and dry place, away from direct sunlight. Once opened, use our convenient zip lock feature to lock it in place to keep away from moisture.
                    </p>
                  </li>
                  <li>
                    <p style="color: #d2d0d0;">Are the products suitable for children?</p>
                    <p>Yes. Cashews can be included as part of the daily diet for children. Children below 3 years may require support as the pieces may be slightly bigger for them to swallow.
                    </p>
                  </li>
                  <li>
                    <p style="color: #d2d0d0;">What are the health benefits of Cashews?</p>
                    <p>Cashews are rich in antioxidants and a host of other important vitamins, minerals and nutrients. Cashews are highly nutritious and rich in important antioxidants and vitamins, they may also increase HDL (good cholesterol) and reduce systolic blood pressure.(from a study published in The Journal of Nutrition
                    </p>
                  </li>
                </ol>
              </li>
              <li>
                <h3>Gifting Options</h3>
                <ol>
                  <li>
                    <p style="color: #d2d0d0;">What all gifting options are available?</p>
                    <p>We can offer our products for festivals, weddings, corporate events and personal gifting. Please contact us at +918848950104/ 4742754200 or send a mail to info@thenutmakers.com for more details
                    </p>
                  </li>
                </ol>
              </li>
              <li>
                <h3>Shipping</h3>
                <ol>
                  <li>
                    <p style="color: #d2d0d0;">How long will it take to receive the delivery?</p>
                    <p>The normal delivery time is within 3-7working days.
                    </p>
                  </li>
                  <li>
                    <p style="color: #d2d0d0;">Can I track my order?</p>
                    <p>We will inform you via an email to notify you when the order gets dispatched. We can also provide the tracking ID of your order, as and when requested. Please call us on +918848950104/ 4742754200 or drop a mail to info@thenutmakers.com
                    </p>
                  </li>
                </ol>
              </li>
            </ol>
          </div>
        </div>

      </div>
    </div>
    <div class="clearfix"></div>
    @extends('layouts.footer')

    <!-- <footer>
      <div class="container" id="contact" style="padding-top:50px;padding-bottom:30px">
        <div class="footer-section">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 align-self-end">
              <form action={{ url('subscribe') }} method="post">
                {{ csrf_field() }}
                <div class="input-group">
                  <input type="text" class="form-control" name="email" placeholder="Enter email to stay in touch" aria-label="Recipient's username" aria-describedby="basic-addon2" required>
                  <div class="input-group-append">
                    <button class="input-group-text" id="basic-addon2" type="submit">SUBSCRIBE</button>
                  </div>
                </div>
              </form>
              <ul class="list-unstyled social-media">
                {{-- <li>Facebook</li>
                <li>Instagram</li> --}}
                <div class="copyright"><p class="pull-right resvered"> <b>2018  Copyright All Rights Reserved. Designed By </b><a href="http://www.shade6.com/" style="color: #b1b0b0;">Shade Six Interactive.</a></div>
                </ul>
            </div>
            <div class="col col-lg-2 col-md-6 col-sm-6">
              <h3>Help</h3>
              <ul class="list-unstyled list-section">
              <li>{{ $constants[0]->phone }}</li>
                <li>{{ $constants[0]->email }}</li>
                <li><a href="#" data-toggle="modal" data-target="#exampleModal">Enquiry</a></li>
              </ul>
            </div>
            <div class="col col-lg-2 col-md-6 col-sm-6">
              <h3>Your Account</h3>
              <ul class="list-unstyled list-section">
                  <?php
                  if(!Session::has('user_id')){
                    ?>
                    <li><a href="{{ url('signin') }}">Sign in</a></li>
                  <?php
                  }
                  ?>
                {{-- <li>Forgot Password</li> --}}
                <li><a href="{{ url('profile') }}">Order History</a></li>
                <li><a href="{{ url('cart') }}">Shopping Bag</a></li>
              </ul>
            </div>
            <div class="col col-lg-2 col-md-6 col-sm-6">
              <h3>Customer Service</h3>
              <ul class="list-unstyled list-section">
                <li><a href="{{ url('terms') }}">Terms & Conditions</a> / <a href="{{ url('privacy') }}">Privacy Policy</a></li>
                <li><a href="{{ url('refund_policy') }}">Shipping and Returns</a></li>
                {{-- <li><a href="{{ url('terms') }}">Terms & Conditions</a></li> --}}
                <li><a href="{{ url('faqs') }}">Faqs</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </footer> -->
  </main>
  <div class="modal fade modal-style" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Have A Question</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body form-section">
          <form id="form" action="{{ url('/enquiry') }}" method="post">
            {{ csrf_field() }}
            <div class="form-group">
              <input type="name" name="name" class="form-control" id="exampleInputname" aria-describedby="emailHelp" placeholder="Name" required>
            </div>
            <div class="form-group">
              <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Email" required>
            </div>
            <div class="form-group">
              <input type="subject" name="subject" class="form-control" id="exampleInputEmail1" placeholder="Subject" required>
            </div>
            <div class="form-group">
              <textarea class="form-control" name="message" id="exampleFormControlTextarea1" placeholder="Message" rows="3" required></textarea>
            </div>
            <span class="msg-error error"></span>
            <div class="g-recaptcha" data-sitekey="6LeKJokUAAAAANZ9WFXmv7QlGC-2_AwY2_yUW-YU"></div>
            <div class="modal-footer">
              <button type="submit" class="btn primary-button" style="padding-top:3%; padding-bottom:3%">SEND</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>





  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="{{ URL::asset('js/jquery-3.2.1.slim.min.js') }}"></script>
  <script src="{{ URL::asset('js/popper.min.js') }}"></script>
  <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
  <script>
    window.onscroll = function() {
      myFunction()
    };

    var header = document.getElementById("myHeader");
    var sticky = header.offsetTop;

    function myFunction() {
      if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
      } else {
        header.classList.remove("sticky");
      }
    }
  </script>
  <script type="text/javascript">
    function validateForm() {

      var $captcha = $( '#recaptcha' ),response = grecaptcha.getResponse();

      if (response.length === 0)
      {
        $( '.msg-error').text( "reCAPTCHA is mandatory" );
        if( !$captcha.hasClass( "error" ) ){
          $captcha.addClass( "error" );
        }
        return false;
      }
      else
      {
        $( '.msg-error' ).text('');
        $captcha.removeClass( "error" );
      }

    }
  </script>
   <script>
    $('.header').on('click', '.search-toggle', function(e) {
      var selector = $(this).data('selector');
      $(".search-input").show();
      $(selector).toggleClass('show').find('.search-input').focus();
      $(this).toggleClass('active');

      e.preventDefault();
    });
  </script>
</body>

</html>

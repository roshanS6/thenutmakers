<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="{{ URL::asset('css/view_bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/view_style.css') }}">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Bai+Jamjuree|Source+Serif+Pro" rel="stylesheet">
  <style>
  input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    margin: 0; 
}
  </style>
  <title>The Nut Makers</title>
</head>

<body>

  <header class="header-section" id="header-1">
    <!-- Fixed navbar -->
    <nav class="navbar navbar-expand-md navbar-dark fixed-top header" id="myHeader">
      <a class="navbar-brand" href="{{ url('/') }}">
        <img src="{{ url::asset('images/logo.png') }}" width="100" height="100">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      <div class="collapse navbar-collapse justify-content-end" id="navbarCollapse">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/') }}">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/product') }}">Shop</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/aboutus') }}">About Us</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/contactus') }}">Contact</a>
          </li>
          <li class="nav-item svg-icon search-button">
            <a href="#" class="search-toggle" data-selector="#header-1">
                  <img src="{{ URL::asset('images/search.svg') }}">
              </a>
          </li>
          <li class="nav-item svg-icon active">
            <a class="nav-link" href="{{ url('/profile') }}">
            <img src="{{ URL::asset('images/user.svg') }}">
          </a>
          </li>
          <li class="nav-item svg-icon">
            <a class="nav-link" href="{{ url('/cart') }}">
              <img src="{{ URL::asset('images/shopper.svg') }}">
            </a>
            <?php
            if(Session::has('cart_count')) {
             ?>
            <span class="dot"> </span>
            <?php 
              }
            ?>
          </li>
        </ul>
        <form action="{{ url('search') }}" class="search-box">
          <input type="text" class="text search-input" name="key" placeholder="Type here to search..." />
        </form>
      </div>
    </nav>
  </header>
  <main role="main" class="wrapper">
    <div class="message" style="padding-top:1%">
      @if (Session('message'))
        <div class="alert alert-success alert-dismissible fade show" role="alert" style="font-weight: 400;">
            <span class="badge badge-pill" style="color:black">{{ Session('message') }}</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          {{ Session::forget('message') }}
        </div>
      @endif
      @if (Session('error'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert" style="font-weight: 400;">
            <span class="badge badge-pill" style="color:black">{{ Session('error') }}</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          {{ Session::forget('error') }}
        </div>
      @endif
    </div> 
    <div class="container-fluid">

      <div class="row align-items-center wrapper-section">

        <div class="col-md-2">
          <!--<div id="turn-around">
            <ol>
              <li><a href="">Healthy and delicious</a></li>
              <li><a href="">All-new fun flavours</a></li>
            </ol>
          </div>-->
        </div>
        <div class="col-md-4 align-self-start form-section">
          <h2>Login</h2>
          <form action="{{ url('login') }}" method="post">
          {{ csrf_field() }}
            <div class="form-group">
              <label for="exampleInputEmail1">Email address</label>
              <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Password</label>
              <input type="password" name="password" class="form-control" id="exampleInputPassword1">
            </div>

            <button type="submit" class="btn primary-button">
                <a>Sign IN</a>
                </button>
            <div class="info"><a href="#" data-toggle="modal" data-target="#exampleModal">Forgot Password</a></div>
          </form>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 form-section mb-formpadding">
          <h2>Create an account</h2>
          {{-- <p>Lorem ipsum dolor sit amet,consectetur adipielit, sed do eiusm odtem porincid iduntut laboreet dolore reprehenderit in iatis</p> --}}
          <form id="form" action="{{ url('register') }}" method="post">
          {{ csrf_field() }}
            <div class="form-group">
              <label for="exampleInputEmail1">First Name*</label>
              <input type="text" class="form-control" name="first_name" id="first_name" aria-describedby="emailHelp">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Last Name*</label>
              <input type="text" class="form-control" name="last_name" id="last_name" aria-describedby="emailHelp">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Email*</label>
              <input type="email" class="form-control" name="email" id="email" aria-describedby="emailHelp">
              <label id="error"></label>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Phone*</label>
              <input type="number"
              oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
              class="form-control" name="phone" id="phone" maxlength="10" aria-describedby="emailHelp">
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Password*</label>
              <input type="password" class="form-control" name="password" id="password" aria-describedby="emailHelp">
              <label class="error"></label>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Confirm Password*</label>
              <input type="password" class="form-control" name="cpassword" id="cpassword">
              <label class="error"></label>
            </div>

            <button type="submit" class="btn primary-button register">
              <a>Register</a></button>
            <div class="info">By Creating an account, you agree to our
              <a href="{{ url('terms') }}">Terms of use</a> and <a href="{{ url('privacy') }}">Privacy Policy</a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </main>
  <div class="modal fade modal-style" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Forgot Password</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          </div>
          <div class="modal-body form-section">
            <form id="forgotpasswordform" action="{{ url('/generateotp') }}" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <input type="email" name="email" class="form-control" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Email" required>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn primary-button" style="padding-top:3%; padding-bottom:3%">SEND</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    
<!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="{{ URL::asset('js/jquery-3.2.1.slim.min.js') }}"></script>
  <script src="{{ URL::asset('js/popper.min.js') }}"></script>
  <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
  <script>
    window.onscroll = function() {
      myFunction()
    };

    var header = document.getElementById("myHeader");
    var sticky = header.offsetTop;

    function myFunction() {
      if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
      } else {
        header.classList.remove("sticky");
      }
    }
  </script>
  <script>
    var allowsubmit = false;
		$(function(){
      //on keypress 
			$('#cpassword').keyup(function(e){
				//get values 
				var pass = $('#password').val();
				var confpass = $(this).val();
				
				//check the strings
				if(pass == confpass){
					//if both are same remove the error and allow to submit
					$('.error').text('');
					allowsubmit = true;
				}else {
					//if not matching show error and not allow to submit
					$('.error').text('Password not matching');
					allowsubmit = false;
				}
      });
      
			$("#email").blur(function () {
         if($("#email").val()==""){
          $("#email").css("border-color","red");
        } else if (!/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test($("#email").val())){
          $('#error').text('enter the Valid mail-id');
			  allowsubmit = false;
         } else {
            $('#error').text('');
			  allowsubmit = true;
         }
       });

			//jquery form submit
			$('#form').submit(function(){
			
				var pass = $('#password').val();
				var confpass = $('#cpassword').val();
				//just to make sure once again during submit
				//if both are true then only allow submit
				if (pass == confpass) {
          if(!/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test($("#email").val())) {
            $('#error').text('enter the Valid mail-id');
            allowsubmit = false;
          } else {
            allowsubmit = true;
          }
        }
				if(allowsubmit) {
					return true;
				} else {
					return false;
				}
			});
		});
  </script>
    <script>
    $('.header').on('click', '.search-toggle', function(e) {
      var selector = $(this).data('selector');
      $(".search-input").show();
      $(selector).toggleClass('show').find('.search-input').focus();
      $(this).toggleClass('active');

      e.preventDefault();
    });
  </script>
</body>
</html>




<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="{{ URL::asset('css/view_bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/view_style.css') }}">
  <link href="https://fonts.googleapis.com/css?family=Bai+Jamjuree|Source+Serif+Pro" rel="stylesheet">
  <title>The Nut Makers</title>
  <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
  <style>
  input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    margin: 0; 
}
input::placeholder {
  color: white;
}
</style>
</head>

<body>

<div class="message">
@if (Session('message'))
<div class="alert alert-warning">
{{ Session('message') }}
{{ Session::forget('message') }}
</div>
@endif
</div>
  <header class="header-section">
    <!-- Fixed navbar -->
    <nav class="navbar navbar-expand-md navbar-dark fixed-top header" id="myHeader">
      <a class="navbar-brand" href="#">
        <img src="{{ URL::asset('images/logo.png') }}" width="100" height="100">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      <div class="collapse navbar-collapse justify-content-end" id="navbarCollapse">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/') }}">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/product') }}">Shop</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/aboutus') }}">About Us</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Contact</a>
          </li>
          <li class="nav-item svg-icon">
            <a class="nav-link" href="#">
              <img src="{{ URL::asset('images/search.svg') }}">
            </a>
          </li>
          <li class="nav-item svg-icon">
            <?php
            if(!Session::has('user_id')){
             ?>
              <a class="nav-link" href="{{ url('/profile') }}">
              <img src="{{ URL::asset('images/user.svg') }}">
            </a>
            <?php
            }
            else{
              ?>            
            <div class="user-area dropdown">
            <a href="#" class="nav-link" data-toggle="dropdown" aria-expanded="false">
                     <img src="{{ URL::asset('images/user.svg') }}" >
                     </a> 
                     <div class="user-menu dropdown-menu">
                     <a class="nav-link" href="{{ url('/profile') }}" style="color:black">My profile</a>                      
                        <a class="nav-link" href="{{ url('/logout') }}" style="color:black">Logout</a>
                     </div>
            </div>
            <?php
            }
            ?>
          </li>
          <li class="nav-item svg-icon active">
            <a class="nav-link" href="{{ url('/cart') }}">
              <img src="{{ URL::asset('images/shopper.svg') }}">
            </a>
            <?php
            if(Session::has('cart_count')) {
             ?>
            <span class="dot"> </span>
            <?php 
              }
            ?>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  @php
  foreach ($order_data as $key => $order) {  
    $total = round($order->grand_total);
    $amountInPaisa = $total * 100;
  }
  $order_id = $order_data[0]->order_id;
  $razorpay_order_id = $order_data[0]->razorpay_order_id;
  $user_id = $user_data[0]->user_id;
  $userName = $user_data[0]->first_name." ".$user_data[0]->last_name;
  $email = $user_data[0]->email;
  $phone = $user_data[0]->phone;
  @endphp
  <main role="main" class="wrapper">
    <div class="container-fluid">
      <div class="container">
        <div class="row payment-warpper" >
          <form action="{{ url('/ordercheckout') }}" method="post">
            {{ csrf_field() }}
            <!-- Note that the amount is in paise = 50 INR -->
            <script
                src="https://checkout.razorpay.com/v1/checkout.js"
                data-key="rzp_live_Nxc2lBQAAvL2ij"
                data-amount= {{ $amountInPaisa }}
                data-buttontext="Pay with Razorpay"
                data-name="The Nut Makers"
                data-description="Order ID : {{ $order_id }}"
                data-order_id= {{ $razorpay_order_id }}
                data-image="http://aumgifts.com/images/default-avatar.png"
                data-prefill.name= {{ $userName }}
                data-prefill.email= {{ $email }}
                data-prefill.contact= {{ $phone }}
                data-theme.color="#0b60a9"
            ></script>
            <!-- <input type="hidden" value="1" name="payment_capture"> -->
            <input type="hidden" value="{{ $user_id }}" name="user_id">
            <input type="hidden" value="{{ $order_id }}" name="order_id">    
          </form>
        </div>
      </div>
    
    
        <script>
        $(document).ready(function (e) {
          $('.razorpay-payment-button').click();
        });
    
        </script>
      </div>
    </div>
    </div>
  </main>
 </body>

</html>
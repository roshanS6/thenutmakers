<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="{{ URL::asset('css/view_bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/view_style.css') }}">
  <link href="https://fonts.googleapis.com/css?family=Bai+Jamjuree|Source+Serif+Pro" rel="stylesheet">
  <script src='https://www.google.com/recaptcha/api.js'></script>
  <title>The Nut Makers</title>
</head>
 
<body>
  <header class="header-section" id="header-1">
    <!-- Fixed navbar -->
    <nav class="navbar navbar-expand-md navbar-dark fixed-top header" id="myHeader">
      <a class="navbar-brand" href="{{ URL('/') }}">
        <img src="{{ URL::asset('images/logo.png') }}" width="100" height="100">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      <div class="collapse navbar-collapse justify-content-end" id="navbarCollapse">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="{{ URL('/') }}">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/product') }}">Shop</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/aboutus') }}">About Us</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/contactus') }}">Contact</a>
          </li>
          <li class="nav-item svg-icon search-button">
            <a href="#" class="search-toggle" data-selector="#header-1">
                  <img src="{{ URL::asset('images/search.svg') }}">
              </a>
          </li>
          <li class="nav-item svg-icon">
            <?php
            if(!Session::has('user_id')){
             ?>
              <a class="nav-link" href="{{ url('/profile') }}">
              <img src="{{ URL::asset('images/user.svg') }}">
            </a>
            <?php
            }
            else{
              ?>            
            <div class="user-area dropdown">
            <a href="#" class="nav-link" data-toggle="dropdown" aria-expanded="false">
                     <img src="{{ URL::asset('images/user.svg') }}" >
                     </a> 
                     <div class="user-menu dropdown-menu">
                     <a class="nav-link" href="{{ url('/profile') }}" style="color:black">My profile</a>                      
                        <a class="nav-link" href="{{ url('/logout') }}" style="color:black">Logout</a>
                     </div>
            </div>
            <?php
            }
            ?>
          </li>
          <li class="nav-item svg-icon">
            <a class="nav-link" href="{{ url('/cart') }}">
              <img src="{{ URL::asset('images/shopper.svg') }}">
            </a>
            <?php
            if(Session::has('cart_count')) {
             ?>
            <span class="dot"> </span>
            <?php 
              }
            ?>
          </li>
        </ul>
        <form action="{{ url('search') }}" class="search-box">
          <input type="text" class="text search-input" name="key" placeholder="Type here to search..." />
        </form>
      </div>
    </nav>
  </header>




  <main role="main" class="wrapper">
    <div class="message" style="padding-top:1%">
      @if (Session('message'))
        <div class="alert alert-success alert-dismissible fade show" role="alert" style="font-weight: 400;">
            <span class="badge badge-pill" style="color:black">{{ Session('message') }}</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          {{ Session::forget('message') }}
        </div>
      @endif
      @if (Session('error'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert" style="font-weight: 400;">
            <span class="badge badge-pill" style="color:black">{{ Session('error') }}</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          {{ Session::forget('error') }}
        </div>
      @endif
    </div>
    <div class="container">

      <div class="row align-items-center wrapper-section">
        <div class="col">
          <h2>Privacy Policy</h2>
          {{-- <div class="terms-list">
            <h4>Terms and Conditions</h4>
            <div class="terms-text">General Site Usage</div>
            <div class="terms-text">Last Revised: December 16,2017</div>
            <p>Welcom etowww.lorem -ipsum .info.This site is provided as a service to our visitors and may be used for informational purposes only.Because the Terms and Conditions contain legal obligations,please read them carefuly.</p>
          </div> --}}
          <div class="terms-list">
            <ol>
              <li>
                <h3>INFORMATION</h3>
                <p style="color: #d2d0d0;">What do we do with your information?</p>
                <p>When you purchase something from our store, as part of the buying and selling process,
                  we collect the personal information you give us, such as your name, address and email
                  address.
                  <br>
                  With your permission, we may send you emails about our store, new products and other
                  updates.
                </p>
              </li>
              <li>
                <h3>CONSENT</h3>
                <p style="color: #d2d0d0;">How do you get my consent?</p>
                <p>When you provide us with personal information to complete a transaction, verify your credit
                  card, place an order, arrange for a delivery or return a purchase, we imply that you consent
                  to our collecting it and using it for that specific reason only.
                  If we ask for your personal information for a secondary reason, like marketing, we will
                  either ask you directly for your expressed consent, or provide you with an opportunity to say
                  no.
                </p>
                <p style="color: #d2d0d0;">How do I withdraw my consent?</p>
                <p>
                  If after you opt-in, you change your mind, you may withdraw your consent for us to contact
                  you, by using the same email by clicking the discontinue link. You can contact us for support
                  anytime at our email support: info@thenutmaker.com
                </p>                  
              </li>
              <li>
                <h3>DISCLOSURE</h3>
                <p>We may disclose your personal information if we are required by law to do so or if you
                  violate our Terms of Service.                  
                </p>
              </li>
              <li>
                <h3>THIRD-PARTY SERVICES</h3>
                <p>In general, the third-party providers used by us will only collect, use and disclose your
                  information to the extent necessary to allow them to perform the services they provide to us.
                  For these providers, we recommend that you read their privacy policies so you can
                  understand the manner in which your personal information will be handled by these
                  providers. Once you leave our store's website or are redirected to a third-party website or
                  application, you are no longer governed by this Privacy Policy or our website's Terms of
                  Service
                </p>
              </li>
              <li>
                <h3>SECURITY</h3>
                <p>To protect your personal information, we take reasonable precautions and follow industry
                  best practices to make sure it is not inappropriately lost, misused, accessed, disclosed,
                  altered or destroyed. Although no method of transmission over the Internet or electronic
                  storage is 100% secure, we follow highest accepted industry standards to keep the
                  information safe.                  
                </p>
              </li>
              <li>
                <h3>CHANGES TO THIS PRIVACY POLICY</h3>
                <p>We reserve the right to modify this privacy policy at any time, so please review it frequently.
                  Changes and clarifications will take effect immediately upon their posting on the website. If
                  we make material changes to this policy, we will notify you here that it has been updated, so
                  that you are aware of what information we collect.
                </p>
              </li>
              <li>
                <h3>QUESTIONS AND CONTACT INFORMATION</h3>
                <p>If you would like to contact us for any further information. Please email to us at
                  info@thenutmakers.com                 
                </p>
              </li>
            </ol>
          </div>
        </div>

      </div>
    </div>
    <div class="clearfix"></div>
    @extends('layouts.footer')
    <!-- <footer>
      <div class="container" id="contact" style="padding-top:50px;padding-bottom:30px">
        <div class="footer-section">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 align-self-end">
              <form action={{ url('subscribe') }} method="post">
                {{ csrf_field() }}
                <div class="input-group">
                  <input type="text" class="form-control" name="email" placeholder="Enter email to stay in touch" aria-label="Recipient's username" aria-describedby="basic-addon2" required>
                  <div class="input-group-append">
                    <button class="input-group-text" id="basic-addon2" type="submit">SUBSCRIBE</button>
                  </div>
                </div>
              </form>
              <ul class="list-unstyled social-media">
                <li>Facebook</li>
                <div class="copyright"><p class="pull-right resvered"> <b>2018  Copyright All Rights Reserved. Designed By </b><a href="http://www.shade6.com/" style="color: #b1b0b0;">Shade Six Interactive.</a></div>
                </ul>
            </div>
            <div class="col col-lg-2 col-md-6 col-sm-6">
              <h3>Help</h3>
              <ul class="list-unstyled list-section">
              <li>{{ $constants[0]->phone }}</li>
                <li>{{ $constants[0]->email }}</li>
                <li><a href="#" data-toggle="modal" data-target="#exampleModal">Enquiry</a></li>
              </ul>
            </div>
            <div class="col col-lg-2 col-md-6 col-sm-6">
              <h3>Your Account</h3>
              <ul class="list-unstyled list-section">
                  <?php
                  if(!Session::has('user_id')){
                    ?>
                    <li><a href="{{ url('signin') }}">Sign in</a></li>
                  <?php 
                  }
                  ?>
                {{-- <li>Forgot Password</li> --}}
                <li><a href="{{ url('profile') }}">Order History</a></li>
                <li><a href="{{ url('cart') }}">Shopping Bag</a></li>
              </ul>
            </div>
            <div class="col col-lg-2 col-md-6 col-sm-6">
              <h3>Customer Service</h3>
              <ul class="list-unstyled list-section">
                <li><a href="{{ url('terms') }}">Terms & Conditions</a> / <a href="{{ url('privacy') }}">Privacy Policy</a></li>
                <li><a href="{{ url('refund_policy') }}">Shipping and Returns</a></li>
                {{-- <li><a href="{{ url('terms') }}">Terms & Conditions</a></li> --}}
                <li><a href="{{ url('faqs') }}">Faqs</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </footer> -->
  </main>
  <div class="modal fade modal-style" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Have A Question</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body form-section">
          <form id="form" action="{{ url('/enquiry') }}" method="post">
            {{ csrf_field() }}
            <div class="form-group">
              <input type="name" name="name" class="form-control" id="exampleInputname" aria-describedby="emailHelp" placeholder="Name" required>
            </div>
            <div class="form-group">
              <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Email" required>
            </div>
            <div class="form-group">
              <input type="subject" name="subject" class="form-control" id="exampleInputEmail1" placeholder="Subject" required>
            </div>
            <div class="form-group">
              <textarea class="form-control" name="message" id="exampleFormControlTextarea1" placeholder="Message" rows="3" required></textarea>
            </div>
            <span class="msg-error error"></span>
            <div class="g-recaptcha" data-sitekey="6LeKJokUAAAAANZ9WFXmv7QlGC-2_AwY2_yUW-YU"></div>
            <div class="modal-footer">
              <button type="submit" class="btn primary-button" style="padding-top:3%; padding-bottom:3%">SEND</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>




  
  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="{{ URL::asset('js/jquery-3.2.1.slim.min.js') }}"></script>
  <script src="{{ URL::asset('js/popper.min.js') }}"></script>
  <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
  <script>
    window.onscroll = function() {
      myFunction()
    };

    var header = document.getElementById("myHeader");
    var sticky = header.offsetTop;

    function myFunction() {
      if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
      } else {
        header.classList.remove("sticky");
      }
    }
  </script>
  <script type="text/javascript">
    function validateForm() {

      var $captcha = $( '#recaptcha' ),response = grecaptcha.getResponse();

      if (response.length === 0)
      {
        $( '.msg-error').text( "reCAPTCHA is mandatory" );
        if( !$captcha.hasClass( "error" ) ){
          $captcha.addClass( "error" );
        }
        return false;
      }
      else
      {
        $( '.msg-error' ).text('');
        $captcha.removeClass( "error" );
      }

    }
  </script>
   <script>
    $('.header').on('click', '.search-toggle', function(e) {
      var selector = $(this).data('selector');
      $(".search-input").show();
      $(selector).toggleClass('show').find('.search-input').focus();
      $(this).toggleClass('active');

      e.preventDefault();
    });
  </script>
</body>

</html>
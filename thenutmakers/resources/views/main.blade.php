<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>The Nut Makers</title>
      <link rel="icon" href="./images/favicon.png" type="image/gif" sizes="16x16">
     

      		<script src="{{ URL::asset('dist/jquery.carouFredSel-6.0.4-packed.js') }}" type="text/javascript"></script>
      	
   </head> 
   <body>
      <header class="main-header">
        <nav class="nav-top">
          <div class="container-fluid">
           <div class="row" style="padding-top:1%;">
             <div class="col-xs-3 col-md-3">
               <div class="navbar-header">
                  <a href="{{ URL('/') }}" id="logo"><img src="{{ URL::asset('images/logo.png') }}"></a>
               </div>
             </div>
             <div class="col-xs-9">
               
               <ul class="nav navbar-nav hide-in-phones" style="float:right;padding-right: 1%;">
                  <li><a href="{{ url('cart') }}">Cart</a></li>
               </ul>
             </div>

          </div>
         </div>
      </header>
      <div id="fullpage">
<div class="message">
      @if (Session('message'))
        <div class="alert alert-warning" style="margin-top:20px;font-weight: 400;">
          {{ Session('message') }}
          {{ Session::forget('message') }}
        </div>
      @endif
         </div> 

@foreach($categories as $category)

  <ul>
    <li><a href="{{ url('/category') }}/{{ $category->category_name }}">{{ $category->category_name }}</li>
      @if($category->image_path=="null")
        <li>No Image to this product</li>
        @else
        <li><img src="{{ URL::asset($category->image_path) }}" height="100" width="100"></li>
      @endif
      </a>
  </ul>

@endforeach
<div>
</div>
</body>
</html>

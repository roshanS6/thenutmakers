<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="{{ URL::asset('css/view_bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/view_style.css') }}">
  <link href="https://fonts.googleapis.com/css?family=Bai+Jamjuree|Source+Serif+Pro" rel="stylesheet">  
  <script src='https://www.google.com/recaptcha/api.js'></script>
  <title>The Nut Makers</title>
  <style>
    a{
      text-decoration: none;
    }
  </style>
</head>

<body>
<header class="header-section" id="header-1">
    <nav class="navbar navbar-expand-sm navbar-dark fixed-top header header-nav" id="myHeader">

      <a class="navbar-brand" href="{{ URL('/') }}">
        <img src="{{ URL::asset('images/logo.png') }}" width="100" height="100">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      <div class="collapse navbar-collapse justify-content-end" id="navbarCollapse">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/') }}">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/product') }}">Shop</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/aboutus') }}">About Us</a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="{{ url('/contactus') }}">Contact</a>
          </li>
          <li class="nav-item svg-icon search-button">
            <a href="#" class="search-toggle" data-selector="#header-1">
                  <img src="{{ URL::asset('images/search.svg') }}">
              </a>
          </li>
          <li class="nav-item svg-icon active">
            <?php
            if(!Session::has('user_id')){
             ?>
              <a class="nav-link" href="{{ url('/profile') }}">
              <img src="{{ URL::asset('images/user.svg') }}">
            </a>
            <?php
            }
            else{
              ?>            
            <div class="user-area dropdown">
            <a href="#" class="nav-link" data-toggle="dropdown" aria-expanded="false">
                     <img src="{{ URL::asset('images/user.svg') }}" >
                     </a> 
                     <div class="user-menu dropdown-menu">    
                     <a class="nav-link" href="{{ url('/profile') }}" style="color:black; border-bottom: 1px solid #fff">My profile</a>                      
                        <a class="nav-link" href="{{ url('/logout') }}" style="color:black; border-bottom: 1px solid #fff;">Logout</a>
                     </div>
            </div>
            <?php
            }
            ?>
          </li>
          <li class="nav-item svg-icon">
            <a class="nav-link" href="{{ url('/cart') }}">
                <img src="{{ URL::asset('images/shopper.svg') }}">
              </a>
              <?php
              if(Session::has('cart_count')) {
               ?>
              <span class="dot"> </span>
              <?php 
                }
              ?>
          </li>
        </ul>
        <form action="{{ url('search') }}" class="search-box">
          <input type="text" class="text search-input" name="key" placeholder="Type here to search..." />
        </form>
      </div>
    </nav>
  </header>

  <main role="main" class="wrapper">
    <div class="message" style="padding-top:1%">
      @if (Session('message'))
        <div class="alert alert-success alert-dismissible fade show" role="alert" style="font-weight: 400;">
            <span class="badge badge-pill" style="color:black">{{ Session('message') }}</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          {{ Session::forget('message') }}
        </div>
      @endif
      @if (Session('error'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert" style="font-weight: 400;">
            <span class="badge badge-pill" style="color:black">{{ Session('error') }}</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          {{ Session::forget('error') }}
        </div>
      @endif
    </div>  
    <div class="container-fluid">

      <div class="row align-items-center wrapper-section justify-content-between track-order">

        <div class="col-2 col-lg-2 col-md-3 col-sm-3 tab-link">
        </div>
        <div class="col-10 col-lg-5 col-md-10 col-sm-10 align-self-start ">
          <div class="row tab-content tab-carousel">
            <div role="tabpanel" class="tab-pane active in form-section" id="first" style="width:100%;">
              <h2>Track Order</h2>
              <ul class="progressbar">
                <li class="active">Processed</li>
                <li>Shipped</li>
                <li>Delivered</li>
              </ul>
              <div class="timeline-section profile-form">
                <div class="timeline-content">
                  <div class="content-item">
                    <div class="dot"></div>
                  </div>
                  <div class="content-body">
                    <div class="field">
                      <label>Order: #{{ $track_order[0]->order_id }}</label>
                      <div class="text-field">@php
                                  $hijri= date('d-m-Y  h:i A', strtotime($track_order[0]->order_time));
                                  @endphp
                                  {{$hijri}}</div>
                      <div class="text-field">Delivery Date: September 19</div>
                    </div>
                  </div>
                </div>
                <div class="timeline-content">
                  <div class="content-item">
                    <div class="dot"></div>
                  </div>
                  <div class="content-body">
                    <div class="field">
                      <label>Shipped(StandardDelivery)</label>
                      <div class="text-field">September 11</div>
                      <div class="text-field">123 Headquarter,Chennai</div>
                    </div>

                  </div>
                </div>
                <div class="timeline-content">
                  <div class="content-item">
                    <div class="dot"></div>
                  </div>
                  <div class="content-body">
                    <div class="field">
                      <label>Out for Delivery</label>
                      <div class="text-field">September 19</div>
                      <div class="text-field">Estimated Delivery Time: 1 P.M </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-10 col-lg-4 col-md-10 col-sm-10 align-self-start form-section">
          <h2>Profile</h2>
          <div class="profile-form">

            <h3>{{ $user[0]->first_name }} {{ $user[0]->last_name }}</h3>
            <div class="field">
              <label>Phone</label>
              <div class="text-field">+91 {{ $user[0]->phone }}</div>
            </div>

            <div class="field">
              <label>Email</label>
              <div class="text-field">{{ $user[0]->email }}</div>
            </div>

            @if (count($address) > 0)
            <div class="field">
              <label>Address1</label>
              <div class="text-field">{{ $address[0]->address_line_1}}</div>
              <div class="text-field">{{ $address[0]->address_line_2}}</div>
            </div>
            @if (count($address)>1)
              <div class="field">
                <label>Address2</label>
                <div class="text-field">{{ $address[1]->address_line_1}}</div>
                <div class="text-field">{{ $address[1]->address_line_2}}</div>
              </div>
            @else
              <div class="field">
                <label>Address2</label>
                <div class="text-field">Nothing</div>
                <div class="text-field"></div>
              </div>
            @endif
          @else
            <div class="field">
              <label>Address1</label>
              <div class="text-field">Nothing</div>
              <div class="text-field"></div>
            </div>

            <div class="field">
              <label>Address2</label>
              <div class="text-field">Nothing</div>
              <div class="text-field"></div>
            </div>
          @endif
          </div>

        </div>
      </div>
    </div>

    <footer>
      <div class="container">
        <div class="footer-section">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 align-self-end">
              <form action={{ url('subscribe') }} method="post">
                {{ csrf_field() }}
                <div class="input-group">
                  <input type="text" class="form-control" name="email" placeholder="Enter email to stay in touch" aria-label="Recipient's username" aria-describedby="basic-addon2" required>
                  <div class="input-group-append">
                    <button class="input-group-text" id="basic-addon2" type="submit">SUBSCRIBE</button>
                  </div>
                </div>
              </form>
              <ul class="list-unstyled social-media">
                {{-- <li>Facebook</li>
                <li>Instagram</li> --}}
                <div class="copyright"><p class="pull-right resvered"> <b>2018  Copyright All Rights Reserved. Designed By </b><a href="http://www.shade6.com/" style="color: #b1b0b0;">Shade Six Interactive.</a></div>
                </ul>
            </div>
            <div class="col col-lg-2 col-md-6 col-sm-6">
              <h3>Help</h3>
              <ul class="list-unstyled list-section">
              <li>{{ $constants[0]->phone }}</li>
                <li>{{ $constants[0]->email }}</li>
                <li><a href="#" data-toggle="modal" data-target="#exampleModal">Enquiry</a></li>
              </ul>
            </div>
            <div class="col col-lg-2 col-md-6 col-sm-6">
              <h3>Your Account</h3>
              <ul class="list-unstyled list-section">
                <?php
                if(!Session::has('user_id')){
                  ?>
                  <li><a href="{{ url('signin') }}">Sign in</a></li>
                <?php 
                }
                ?>
                {{-- <li>Forgot Password</li> --}}
                <li><a href="{{ url('profile') }}">Order History</a></li>
                <li><a href="{{ url('cart') }}">Shopping Bag</a></li>
              </ul>
            </div>
            <div class="col col-lg-2 col-md-6 col-sm-6">
              <h3>Customer Service</h3>
              <ul class="list-unstyled list-section">
                <li><a href="{{ url('terms') }}">Terms & Conditions</a> / <a href="{{ url('privacy') }}">Privacy Policy</a></li>
                <li><a href="{{ url('refund_policy') }}">Shipping and Returns</a></li>
                {{-- <li><a href="{{ url('terms') }}">Terms & Conditions</a></li> --}}
                <li><a href="{{ url('faqs') }}">Faqs</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </footer>
  </main>

  <div class="modal fade modal-style" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Have A Question</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body form-section">
          <form id="form" action="{{ url('/enquiry') }}" method="post">
            {{ csrf_field() }}
            <div class="form-group">
              <input type="name" name="name" class="form-control" id="exampleInputname" aria-describedby="emailHelp" placeholder="Name" required>
            </div>
            <div class="form-group">
              <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Email" required>
            </div>
            <div class="form-group">
              <input type="subject" name="subject" class="form-control" id="exampleInputEmail1" placeholder="Subject" required>
            </div>
            <div class="form-group">
              <textarea class="form-control" name="message" id="exampleFormControlTextarea1" placeholder="Message" rows="3" required></textarea>
            </div>
            <span class="msg-error error"></span>
            <div class="g-recaptcha" data-sitekey="6LeKJokUAAAAANZ9WFXmv7QlGC-2_AwY2_yUW-YU"></div>
            <div class="modal-footer">
              <button type="submit" class="btn primary-button" style="padding-top:3%; padding-bottom:3%">SEND</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>


  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="{{ URL::asset('js/jquery-3.2.1.slim.min.js') }}"></script>
  <script src="{{ URL::asset('js/popper.min.js') }}"></script>
  <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
  <script>
    window.onscroll = function() {
      myFunction()
    };

    var header = document.getElementById("myHeader");
    var sticky = header.offsetTop;

    function myFunction() {
      if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
      } else {
        header.classList.remove("sticky");
      }
    }
  </script>
  <script type="text/javascript">
    function validateForm() {

      var $captcha = $( '#recaptcha' ),response = grecaptcha.getResponse();

      if (response.length === 0)
      {
        $( '.msg-error').text( "reCAPTCHA is mandatory" );
        if( !$captcha.hasClass( "error" ) ){
          $captcha.addClass( "error" );
        }
        return false;
      }
      else
      {
        $( '.msg-error' ).text('');
        $captcha.removeClass( "error" );
      }

    }
  </script>
   <script>
      $('.header').on('click', '.search-toggle', function(e) {
        var selector = $(this).data('selector');
        $(".search-input").show();  
        $(selector).toggleClass('show').find('.search-input').focus();
        $(this).toggleClass('active');
  
        e.preventDefault();
      });
    </script>
  
</body>

</html>
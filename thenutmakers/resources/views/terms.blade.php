<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="{{ URL::asset('css/view_bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/view_style.css') }}">
  <link href="https://fonts.googleapis.com/css?family=Bai+Jamjuree|Source+Serif+Pro" rel="stylesheet">
  <script src='https://www.google.com/recaptcha/api.js'></script>
  <title>The Nut Makers</title>
</head>

<body>
  <header class="header-section" id="header-1">
    <!-- Fixed navbar -->
    <nav class="navbar navbar-expand-md navbar-dark fixed-top header" id="myHeader">
      <a class="navbar-brand" href="{{ URL('/') }}">
        <img src="{{ URL::asset('images/logo.png') }}" width="100" height="100">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      <div class="collapse navbar-collapse justify-content-end" id="navbarCollapse">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="{{ URL('/') }}">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/product') }}">Shop</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/aboutus') }}">About Us</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/contactus') }}">Contact</a>
          </li>
          <li class="nav-item svg-icon search-button">
            <a href="#" class="search-toggle" data-selector="#header-1">
                  <img src="{{ URL::asset('images/search.svg') }}">
              </a>
          </li>
          <li class="nav-item svg-icon">
            <?php
            if(!Session::has('user_id')){
             ?>
              <a class="nav-link" href="{{ url('/profile') }}">
              <img src="{{ URL::asset('images/user.svg') }}">
            </a>
            <?php
            }
            else{
              ?>
            <div class="user-area dropdown">
            <a href="#" class="nav-link" data-toggle="dropdown" aria-expanded="false">
                     <img src="{{ URL::asset('images/user.svg') }}" >
                     </a>
                     <div class="user-menu dropdown-menu">
                     <a class="nav-link" href="{{ url('/profile') }}" style="color:black">My profile</a>
                        <a class="nav-link" href="{{ url('/logout') }}" style="color:black">Logout</a>
                     </div>
            </div>
            <?php
            }
            ?>
          </li>
          <li class="nav-item svg-icon">
            <a class="nav-link" href="{{ url('/cart') }}">
              <img src="{{ URL::asset('images/shopper.svg') }}">
            </a>
            <?php
            if(Session::has('cart_count')) {
             ?>
            <span class="dot"> </span>
            <?php
              }
            ?>
          </li>
        </ul>
        <form action="{{ url('search') }}" class="search-box">
          <input type="text" class="text search-input" name="key" placeholder="Type here to search..." />
        </form>
      </div>
    </nav>
  </header>




  <main role="main" class="wrapper">
    <div class="message" style="padding-top:1%">
      @if (Session('message'))
        <div class="alert alert-success alert-dismissible fade show" role="alert" style="font-weight: 400;">
            <span class="badge badge-pill" style="color:black">{{ Session('message') }}</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          {{ Session::forget('message') }}
        </div>
      @endif
      @if (Session('error'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert" style="font-weight: 400;">
            <span class="badge badge-pill" style="color:black">{{ Session('error') }}</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          {{ Session::forget('error') }}
        </div>
      @endif
    </div>
    <div class="container">

      <div class="row align-items-center wrapper-section">
        <div class="col">
          <h2>Terms Of Service</h2>
          <div class="terms-list">
            <h4>OVERVIEW</h4>
            {{-- <div class="terms-text">General Site Usage</div>
            <div class="terms-text">Last Revised: December 16,2017</div> --}}
            <p>This website is operated by The Nutmakers. Throughout the site, the terms "we", "us" and "our" refer to The Nutmakers.
              The Nutmakers offers this website, including all information, tools and services available from this site to you, the user, conditioned upo n your acceptance
              of all terms, conditions, policies and notices stated here. </p>
            <p>By visiting our site and/ or purchasing something from us, you engage in our "Service" and agree to be bound by the following terms and conditions
              ("Terms of Service", "Terms"), including those additional terms and conditions and policies referenced herein and/or availabl e by hyperlink.
              These Terms of Service apply to all users of the site, including without limitation users who are browsers, vendors, customers, merchants, and/ or
              contributors of content. Please read these Terms of Service carefully before accessing or using our website. By accessing or using any part of the site, you
              agree to be bound by these Terms of Service. If you do not agree to all the terms and conditions of this agreement, then you may not access the website or
              use any services. If these Terms of Service are considered an offer, acceptance is expressly limited to these Terms of Service. Any new features or tools
              which are added to the current store shall also be subject to the Terms of Service. </p>
              <p>We reserve the right to update, change or replace any part of these Terms of Service by posting updates and/or changes to our website. It is your
                responsibility to check this page periodically for changes. Your continued use of or access to the website following the post ing of any changes constitutes
                acceptance of those changes.</p>
          </div>
          <div class="terms-list">
            <ol>
              <li>
                <h3>ONLINE STORE TERMS</h3>
                <p>By agreeing to these Terms of Service, you represent that you are at the legal age to carry out bank transactions. You may not use our products for any
                  illegal or unauthorized purpose nor may you, in the use of the Service, violate any laws in your jurisdiction (including but not limited to copyright laws).
                  You must not transmit any worms or viruses or any code of a destructive nature.
                </p>
              </li>
              <li>
                <h3>GENERAL CONDITIONS</h3>
                <p>You understand that your content (not including credit card information), may be transferred unencrypted and involve (a) transmissions over various
                  networks; and (b) changes to conform and adapt to technical requirements of connecting networks or devices. Credit card information is always encrypted
                  during transfer over networks. You agree not to reproduce, duplicate, copy, sell, resell or exploit any portion of the Service, use of the Service, or access to
                  the Service or any contact on the website through which the service is provided, without express written permission by us. Th e headings used in this
                  agreement are included for convenience only and will not limit or otherwise affect these Terms.
                </p>
              </li>
              <li>
                <h3>MODIFICATIONS TO THE SERVICE AND PRICES</h3>
                <p>Prices for our products are subject to change without notice. We reserve the right at any time to modify or discontinue the Service (or any part or content
                  thereof) without notice at any time. We shall not be liable to you or to any third-party for any modification, price change, suspension or discontinuance of
                  the Service
                </p>
              </li>
              <li>
                <h3>ACCURACY OF BILLING AND ACCOUNT INFORMATION</h3>
                <p>We may, in our sole discretion, limit or cancel quantities purchased per person, per household or per order. These restrictio ns may include orders placed by
                  or under the same customer account, the same credit card, and/or orders that use the same billing and/or shipping address. In the event that we make a
                  change to or cancel an order, we may attempt to notify you by contacting the e-mail and/or billing address/phone number provided at the time the order was
                  made. We reserve the right to limit or prohibit orders that, in our sole judgment, appear to be placed by dealers, resellers or distributors. You agree to
                  provide current, complete and accurate purchase and account information for all purchases made at our store.
                </p>
              </li>
              <li>
                <h3>CHANGES TO TERMS OF SERVICE</h3>
                <p>You can review the most current version of the Terms of Service at any time at this page. We reserve the right, at our sole d iscretion, to update, change or
                  replace any part of these Terms of Service by posting updates and changes to our website. It is your responsibility to check our website periodically for
                  changes. Your continued use of or access to our website or the Service following the posting of any changes to these Terms of Service constitutes
                  acceptance of those changes.
                </p>
              </li>
            </ol>
          </div>
        </div>

      </div>
    </div>
    <div class="clearfix"></div>

    <footer>
      <div class="container" id="contact" style="padding-top:50px;padding-bottom:30px">
        <div class="footer-section">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 align-self-end">
              <form action={{ url('subscribe') }} method="post">
                {{ csrf_field() }}
                <div class="input-group">
                  <input type="text" class="form-control" name="email" placeholder="Enter email to stay in touch" aria-label="Recipient's username" aria-describedby="basic-addon2" required>
                  <div class="input-group-append">
                    <button class="input-group-text" id="basic-addon2" type="submit">SUBSCRIBE</button>
                  </div>
                </div>
              </form>
              <ul class="list-unstyled social-media">
                {{-- <li>Facebook</li>
                <li>Instagram</li> --}}
                <a href="https://www.facebook.com/thenutmakers/?modal=admin_todo_tour" target="_blank"><img src="https://img.icons8.com/color/100/000000/facebook.png" style="width: 7%"></a>
                <div class="copyright"><p class="pull-right resvered"> <b>2018  Copyright All Rights Reserved. Designed By </b><a href="http://www.shade6.com/" style="color: #b1b0b0;">Shade Six Interactive.</a></div>
                </ul>
            </div>
            <div class="col col-lg-2 col-md-6 col-sm-6">
              <h3>Help</h3>
              <ul class="list-unstyled list-section">
              <li>{{ $constants[0]->phone }}</li>
                <li>{{ $constants[0]->email }}</li>
                <li><a href="#" data-toggle="modal" data-target="#exampleModal">Enquiry</a></li>
              </ul>
            </div>
            <div class="col col-lg-2 col-md-6 col-sm-6">
              <h3>Your Account</h3>
              <ul class="list-unstyled list-section">
                  <?php
                  if(!Session::has('user_id')){
                    ?>
                    <li><a href="{{ url('signin') }}">Sign in</a></li>
                  <?php
                  }
                  ?>
                {{-- <li>Forgot Password</li> --}}
                <li><a href="{{ url('profile') }}">Order History</a></li>
                <li><a href="{{ url('cart') }}">Shopping Bag</a></li>
              </ul>
            </div>
            <div class="col col-lg-2 col-md-6 col-sm-6">
              <h3>Customer Service</h3>
              <ul class="list-unstyled list-section">
                <li><a href="{{ url('terms') }}">Terms & Conditions</a></li>
                <li><a href="{{ url('privacy') }}">Privacy Policy</a></li>
                <li><a href="{{ url('refund_policy') }}">Shipping and Returns</a></li>
                {{-- <li><a href="{{ url('terms') }}">Terms & Conditions</a></li> --}}
                <li><a href="{{ url('faqs') }}">Faqs</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </footer>
  </main>
  <div class="modal fade modal-style" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Have A Question</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body form-section">
          <form id="form" action="{{ url('/enquiry') }}" method="post">
            {{ csrf_field() }}
            <div class="form-group">
              <input type="name" name="name" class="form-control" id="exampleInputname" aria-describedby="emailHelp" placeholder="Name" required>
            </div>
            <div class="form-group">
              <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Email" required>
            </div>
            <div class="form-group">
              <input type="subject" name="subject" class="form-control" id="exampleInputEmail1" placeholder="Subject" required>
            </div>
            <div class="form-group">
              <textarea class="form-control" name="message" id="exampleFormControlTextarea1" placeholder="Message" rows="3" required></textarea>
            </div>
            <span class="msg-error error"></span>
            <div class="g-recaptcha" data-sitekey="6LeKJokUAAAAANZ9WFXmv7QlGC-2_AwY2_yUW-YU"></div>
            <div class="modal-footer">
              <button type="submit" class="btn primary-button" style="padding-top:3%; padding-bottom:3%">SEND</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>





  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="{{ URL::asset('js/jquery-3.2.1.slim.min.js') }}"></script>
  <script src="{{ URL::asset('js/popper.min.js') }}"></script>
  <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
  <script>
    window.onscroll = function() {
      myFunction()
    };

    var header = document.getElementById("myHeader");
    var sticky = header.offsetTop;

    function myFunction() {
      if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
      } else {
        header.classList.remove("sticky");
      }
    }
  </script>
  <script type="text/javascript">
    function validateForm() {

      var $captcha = $( '#recaptcha' ),response = grecaptcha.getResponse();

      if (response.length === 0)
      {
        $( '.msg-error').text( "reCAPTCHA is mandatory" );
        if( !$captcha.hasClass( "error" ) ){
          $captcha.addClass( "error" );
        }
        return false;
      }
      else
      {
        $( '.msg-error' ).text('');
        $captcha.removeClass( "error" );
      }

    }
  </script>
   <script>
    $('.header').on('click', '.search-toggle', function(e) {
      var selector = $(this).data('selector');
      $(".search-input").show();
      $(selector).toggleClass('show').find('.search-input').focus();
      $(this).toggleClass('active');

      e.preventDefault();
    });
  </script>
</body>

</html>

<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <script src='https://www.google.com/recaptcha/api.js'></script>
  <link rel="stylesheet" href="{{ URL::asset('css/view_bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/view_style.css') }}">
  <link href="https://fonts.googleapis.com/css?family=Bai+Jamjuree|Source+Serif+Pro" rel="stylesheet">
  <title>The Nut Makers</title>
  <style>
    .arrow-style {
      margin: 0% !important;  
      margin-top: 0% !important;
      margin-right: 3% !important;
    }
  </style>
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="50" class="scroll">

  <header class="header-section" id="header-1">
    <nav class="navbar navbar-expand-sm navbar-dark fixed-top header header-nav" id="myHeader">

      <a class="navbar-brand" href="{{ URL('/') }}">
        <img src="{{ URL::asset('images/logo.png') }}" width="100" height="100">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      <div class="collapse navbar-collapse justify-content-end" id="navbarCollapse">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" id="home" href="#home">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/product') }}">Shop</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#about">About Us</a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="{{ url('/contactus') }}">Contact</a>
          </li>
          <li class="nav-item svg-icon search-button">
            <a href="#" class="search-toggle" data-selector="#header-1">
                  <img src="{{ URL::asset('images/search.svg') }}">
              </a>
          </li>
          <li class="nav-item svg-icon">
            <?php
            if(!Session::has('user_id')){
             ?>
              <a class="nav-link" href="{{ url('/profile') }}">
              <img src="{{ URL::asset('images/user.svg') }}">
            </a>
            <?php
            }
            else{
              ?>            
            <div class="user-area dropdown">
              <a href="#" class="nav-link" data-toggle="dropdown" aria-expanded="false">
                <img src="{{ URL::asset('images/user.svg') }}" >
              </a> 
              <div class="user-menu dropdown-menu">
                <a class="nav-link" href="{{ url('/profile') }}" style="color:black">My profile</a>                      
                <a class="nav-link" href="{{ url('/logout') }}" style="color:black">Logout</a>
              </div>
            </div>
            <?php
            }
            ?>
          </li>
          <li class="nav-item svg-icon">
            <a class="nav-link" href="{{ url('/cart') }}">
                <img src="{{ URL::asset('images/shopper.svg') }}">
              </a>
            <?php
            if(Session::has('cart_count')) {
             ?>
            <span class="dot"> </span>
            <?php 
              }
            ?>
          </li>
        </ul>
        <form action="{{ url('search') }}" class="search-box">
          <input type="text" class="text search-input" name="key" placeholder="Type here to search..." />
        </form>
      </div>
    </nav>
  </header>
  <main role="main" class="wrapper">
    <div class="message" style="padding-top:2%">
      @if (Session('message'))
        <div class="alert alert-success alert-dismissible fade show" role="alert" style="margin-top:1%;font-weight: 400;">
          <span class="badge" style="color:black">{{ Session('message') }}</span>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          {{ Session::forget('message') }}
        </div>
      @endif 
      @if (Session('error'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert" style="margin-top:1%;font-weight: 400;">
          <span class="badge badge-pill" style="color:black">{{ Session('error') }}</span>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          {{ Session::forget('error') }}
        </div>
      @endif
    </div> 
    <div class="container-fluid" id="home" style="padding-top:50px;padding-bottom:30px">
      <div class="row align-items-center wrapper-section">
        <div class="col-1">
          <div id="turn-around">
            <ol>
              {{-- <li><a href="">Healthy and delicious</a></li>
              <li><a href="">All-new fun flavours</a></li> --}}
            </ol>
          </div>
        </div>
        <div id="carouselExampleControls-banner" class="carousel slide home-carousel" data-ride="carousel">
            <div class="carousel-inner">
              @php                      
              $is_active = true;
              $i = 0;
              @endphp
              @for($k=0; $k < count($banner); $k++)
              <div class="carousel-item <?php if ($k == 0) echo 'active' ?>" id="banner">
                <div class="row">
                  <div class="col-10 col-lg-6 col-md-11 col-sm-11">
                    @if($banner[$k]->banner_image != null)
                    <img class="d-block w-100" src="{{ URL::asset($banner[$k]->banner_image) }}" alt="First slide">
                    @endif
                  </div>
                  <div class="col-lg-6 col-md-12 col-sm-12">
                    <div class="content slide_animation slide--left slide--in-view content-margin">
                      <h1 class="animation-element slide-left">{{ $banner[$k]->title }}</h1>
                      <p class="animation-element slide-right" style="font-color:#fff;">{{ $banner[$k]->description }}</p>
                      {{-- <div style="margin-top:30px;">
                        <a href="{{ url('/product_details') }}/{{ $banner[$k]->id }}"><button type="btn" class="primary-btn">BUY NOW</button></a>
                      </div> --}}
                    </div>
                  </div>
                </div>
              </div>
              @endfor
              {{-- <div class="carousel-item">
                <div class="row">
                  <div class="col-10 col-lg-6 col-md-11 col-sm-11">
                    <img class="d-block w-100" src="images/Black-Chilli-Cheese.png" alt="First slide">
                  </div>
                  <div class="col-lg-6 col-md-12 col-sm-12">
                    <div class="content slide_animation slide--left slide--in-view content-margin">
                      <h1 class="animation-element slide-left">Premium Quality Cashews</h1>
                      <p class="animation-element slide-right">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p>
                      <div style="margin-top:30px;">
                        <button type="btn" class="primary-btn">BUY NOW</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div> --}}
            </div>
            <div class="home-banner-button">
              <a href="#carouselExampleControls-banner" role="button" data-slide="prev">
                <img src="images/prev.svg" class="arrow-style" style="margin-right:3%"> <span class="sr-only">Previous</span>
              </a>
              <a href="#carouselExampleControls-banner" role="button" data-slide="next">
                <img src="images/next.svg" class="arrow-style"> <span class="sr-only">Next</span>
              </a>
            </div>
          </div>
        </div>
        
    <div class="clearfix"></div>
    <div class="container-fluid" id="about" style="padding-top: 20vh";>
      <div class="row align-items-center">
        <div class="col-1">

        </div>
        <div class="col-10 col-lg-6 col-md-11 col-sm-11">
          <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img class="d-block w-100" src="{{ URL::asset($aboutus->image_path) }}" alt="About">
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-5 col-md-12 col-sm-12">
          <div class="content ">
            <h1 class="animation-element slide-left">{{ $aboutus->title }}</h1>
            @php
              $about_us = $aboutus->about_us;
            @endphp
            <div style="color:#959595 !important">
            <p class="animation-element tinymce slide-right"> {!! $about_us !!}</p>
            </div>
            <a href="#products"><img src="{{ URL::asset('images/down.svg') }}" class="arrow-style"></a>
          </div>
        </div>

      </div>
    </div>

    <div class="container-fluid product-bg" id="products">
      <div class="row align-items-center wrapper-section">
        <div class="col col-1">
        </div>
        <div class="col">
          <h2>Try our all-new flavours</h2>
          <div id="carouselExampleControls-product" class="carousel slide product-carousel" data-ride="carousel">
            <div class="carousel-inner">
              @php                      
              $is_active = true;
              $i = 0;
              @endphp
              @for($k=0; $k< count($sliders); $k++)
                <?php if ($i % 4 == 0):?>
                <div class="carousel-item<?php if ($is_active) echo ' active'?>">
                  <div class="row home-product-section">
                    <?php endif?>                            
                    @for($l=0; $l< 4; $l++)
                      @if(isset($sliders[$k]->id))                                                                  
                            <div class="">
                              <div class="product-card">                       
                                <a href="{{ url('/product_details') }}/{{ $sliders[$k]->id }}">
                                  <img class="d-block w-100" src="{{ url::asset($sliders[$k]->image_path) }}" alt="Product">                              
                                  <div class="product-content">
                                  <div class="product-title animation-element slide-left">{{  $sliders[$k]->product_name }}</div>
                                  <div class="product-details animation-element slide-right">{{ $sliders[$k]->short_description }}</div>
                                  </div>
                                </a>
                              </div>
                            </div>
                            <?php
                            if($l!=3){
                            $k++;
                            }
                            $is_active=false;
                            ?>                                                             
                      @endif
                    @endfor                           
                  </div>                     
                </div>
              @endfor                  
                        
              <div class="btn-section animation-element slide-left">
                <a href="{{ url('/product') }}">
                <button type="btn" class="primary-btn">
                  Go To Shop
                </button></a>
              </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls-product" role="button" data-slide="prev">
              <img src="{{ URL::asset('images/prev.svg') }}" class="arrow-style">
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls-product" role="button" data-slide="next">
              <img src="{{ URL::asset('images/next.svg') }}" class="arrow-style">
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
      </div>
    </div></div>
    </div>

    <div class="container-fluid testimonial-section testimonial-bg">
      <div class="row align-items-center justify-content-md-center wrapper-section" style="padding-top:0%">
        <div class="col-12 col-lg-6">
          <h2 class="animation-element slide-left">Testimonial</h2>
          <div id="carouselExampleControls-testimonial" class="carousel slide testimonial-carousel" data-ride="carousel">
            <div class="carousel-inner">
              @php                      
              $is_active = true;
              @endphp
              @for($k=0; $k< count($testimonial); $k++)
                <div class="carousel-item<?php if ($is_active) echo ' active'?>">
                  <div class="row">
                    <div class="col">
                      <p>{{ $testimonial[$k]->content }}</p>
                      <h3>{{ $testimonial[$k]->name }}</h3>
                    </div>
                  </div>                    
                </div>
                <?php 
                if($k==0)
                  $is_active = false;
                ?>
              @endfor                  
              {{-- <div class="carousel-item active">
                <div class="row">
                  <div class="col">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type
                      specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                    <h3>Hugh Jackm an,Actor</h3>
                  </div>
                </div>
              </div>
              <div class="carousel-item">
                <div class="row">
                  <div class="col">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type
                      specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                    <h3>Hugh Jackm an,Actor</h3>
                  </div>
                </div>
              </div> --}}

            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls-testimonial" role="button" data-slide="prev">
              <img src="images/prev.svg" class="arrow-style">
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls-testimonial" role="button" data-slide="next">
              <img src="images/next.svg" class="arrow-style">
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
      </div>
    </div>

    @extends('layouts.footer')
  
    <!-- <footer>
      <div class="container" id="contact" style="padding-top:50px;padding-bottom:30px">
        <div class="footer-section">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 align-self-end">
              <form action={{ url('subscribe') }} method="post">
                {{ csrf_field() }}
                <div class="input-group">
                  <input type="text" class="form-control" name="email" placeholder="Enter email to stay in touch" aria-label="Recipient's username" aria-describedby="basic-addon2" required>
                  <div class="input-group-append">
                    <button class="input-group-text" id="basic-addon2" type="submit">SUBSCRIBE</button>
                  </div>
                </div>
              </form>
              <ul class="list-unstyled social-media">
                {{-- <li>Facebook</li>
                <li>Instagram</li> --}}
                <div class="copyright"><p class="pull-right resvered"> <b>2018  Copyright All Rights Reserved. Designed By </b><a href="http://www.shade6.com/" style="color: #b1b0b0;">Shade Six Interactive.</a></div>
              </ul>
            </div>
            <div class="col col-lg-2 col-md-6 col-sm-6">
              <h3>Help</h3>
              <ul class="list-unstyled list-section">
                <li>{{ $constants[0]->phone }}</li>
                <li>{{ $constants[0]->email }}</li>
                <li><a href="#" data-toggle="modal" data-target="#exampleModal">Enquiry</a></li>
              </ul>
            </div>
            <div class="col col-lg-2 col-md-6 col-sm-6">
              <h3>Your Account</h3>
              <ul class="list-unstyled list-section">
                  <?php
                  if(!Session::has('user_id')){
                    ?>
                    <li><a href="{{ url('signin') }}">Sign in</a></li>
                  <?php 
                  }
                  ?>
                {{-- <li>Forgot Password</li> --}}
                <li><a href="{{ url('profile') }}">Order History</a></li>
                <li><a href="{{ url('cart') }}">Shopping Bag</a></li>
              </ul>
            </div>
            <div class="col col-lg-2 col-md-6 col-sm-6">
              <h3>Customer Service</h3>
              <ul class="list-unstyled list-section">
                <li><a href="{{ url('terms') }}">Terms & Conditions</a> / <a href="{{ url('privacy') }}">Privacy Policy</a></li>
                <li><a href="{{ url('refund_policy') }}">Shipping and Returns</a></li>
                {{-- <li><a href="{{ url('terms') }}">Terms & Conditions</a></li> --}}
                <li><a href="{{ url('faqs') }}">Faqs</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </footer> -->

  </main>

  <div class="modal fade modal-style" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Have A Question</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body form-section">
          <form id="form" action="{{ url('/enquiry') }}" method="post">
            {{ csrf_field() }}
            <div class="form-group">
              <input type="name" name="name" class="form-control" id="exampleInputname" aria-describedby="emailHelp" placeholder="Name" required>
            </div>
            <div class="form-group">
              <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Email" required>
            </div>
            <div class="form-group">
              <input type="subject" name="subject" class="form-control" id="exampleInputEmail1" placeholder="Subject" required>
            </div>
            <div class="form-group">
              <textarea class="form-control" name="message" id="exampleFormControlTextarea1" placeholder="Message" rows="3" required></textarea>
            </div>
            <span class="msg-error error"></span>
            <div class="g-recaptcha" data-sitekey="6LeKJokUAAAAANZ9WFXmv7QlGC-2_AwY2_yUW-YU"></div>
            <div class="modal-footer">
              <button type="submit" class="btn primary-button" style="padding-top:3%; padding-bottom:3%">SEND</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS-->
  <script src="{{ URL::asset('js/jquery-3.2.1.slim.min.js') }}"></script>
  <script src="{{ URL::asset('js/popper.min.js') }}"></script>
  <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
  <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
  <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
  <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
  <script type="text/javascript" src="slick/slick.min.js"></script>
  <script>

  </script>
  <script>
    window.onscroll = function() {
      myFunction()
    };

    var header = document.getElementById("myHeader");
    var sticky = header.offsetTop;

    function myFunction() {
      if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
      } else {
        header.classList.remove("sticky");
      }
    }
  </script>
  <script>
    $('.header').on('click', '.search-toggle', function(e) {
      var selector = $(this).data('selector');
      $(".search-input").show();
      $(selector).toggleClass('show').find('.search-input').focus();
      $(this).toggleClass('active');
      e.preventDefault();
    });
    $('.message').delay(2000);
  </script>
  <script>
  $('.Carousel').on('slide.bs.carousel', function () {
    // do something…
  })
  </script>
  <script type="text/javascript">
    function validateForm() {

      var $captcha = $( '#recaptcha' ),response = grecaptcha.getResponse();

      if (response.length === 0)
      {
        $( '.msg-error').text( "reCAPTCHA is mandatory" );
        if( !$captcha.hasClass( "error" ) ){
          $captcha.addClass( "error" );
        }
        return false;
      }
      else
      {
        $( '.msg-error' ).text('');
        $captcha.removeClass( "error" );
      }

    }
  </script>
  <script>
    /*Interactivity to determine when an animated element in in view. In view elements trigger our animation*/
    $(document).ready(function() {

      //window and animation items
      var animation_elements = $.find('.animation-element');
      var web_window = $(window);

      //check to see if any animation containers are currently in view
      function check_if_in_view() {
        //get current window information
        var window_height = web_window.height();
        var window_top_position = web_window.scrollTop();
        var window_bottom_position = window_top_position + window_height;

        //iterate through elements to see if its in view
        $.each(animation_elements, function() {

          //get the element sinformation
          var element = $(this);
          var element_height = $(element).outerHeight();
          var element_top_position = $(element).offset().top;
          var element_bottom_position = element_top_position + element_height;

          //check to see if this current container is visible (its viewable if it exists between the viewable space of the viewport)
          if (element_bottom_position >= window_top_position && element_top_position <= window_bottom_position) {
            element.addClass('in-view');
          } else {
            element.removeClass('in-view');
          }
        });

      }

      //on or scroll, detect elements in view
      $(window).on('scroll resize', function() {
        check_if_in_view();
      });
      //trigger our scroll event on initial load
      $(window).trigger('scroll');

    });
    // $(window).on("load", function () {
    //   window.scrollTo(0, 0);
    // });
    $('#home').on("click",function(){
        $(window).scrollTop(0);
        // alert('hi mcoc');
    });
  </script>
  <script type="text/javascript">
    $(document).ready(function(){
      $('.row home-product-section').slick({
        setting-name: setting-value
      });
    });
  </script>

</body>

</html>
<script src="{{ URL::asset('js/jquery.min.js')}}"></script>
   <link rel="stylesheet" href="{{ URL::asset('assets/css/normalize.css')}}">
   <link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap.min.css')}}">
   <link rel="stylesheet" href="{{ URL::asset('assets/css/font-awesome.min.css')}}">
   <link rel="stylesheet" href="{{ URL::asset('assets/css/themify-icons.css')}}">
   <link rel="stylesheet" href="{{ URL::asset('assets/css/flag-icon.min.css')}}">
   <link rel="stylesheet" href="{{ URL::asset('assets/css/cs-skin-elastic.css')}}">
   <link rel="stylesheet" href="{{ URL::asset('assets/css/lib/datatable/dataTables.bootstrap.min.css')}}">
   <link rel="stylesheet" href="{{ URL::asset('assets/scss/style.css')}}">
   <link rel="stylesheet" href="{{ URL::asset('assets/css/lib/vector-map/jqvmap.min.css')}}">
   <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

   <style>
      .header-top p{
      text-align:center;
      margin:5px 0px;
      }
      li.amount {
      font-weight: 700;
      margin: 4px;
      }
      .header-top h1{
      font-size:36px;
      text-align:center;
      margin:0px;
      }
      .header-address p{
      text-align:center;
      margin:5px 0px;
      }
      .border{
      border-bottom:solid 2px #666;
      border-top:solid 2px #666;
      }
      .invoice-address{
      }
      ul li{
      list-style:none;
      }
      .invoice-no ul li{
      list-style:none;
      }
      .Quantity-table{
      padding-top:20px;
      }
      table, th {
      border: 1px solid black;
      text-align:center;
      padding: 15px 15px;
      ;
      }
      table td{
      border-right: 1px solid black;
      border-left: 1px solid black;
      border-top:none !important;
      border-bottom: :none !important;
      padding: 15px 15px;
      font-size: 15px;
      }
      .words{
      border: 1px solid black;
      }
      td.description{
      text-align:left;
      }
      td.border-none {
      border-left: none;
      }
      td.border-none2 {
      border-right: none;
      }
      .dc-add{
      max-width:600px;
      }
      .space td{
      padding-bottom: 300px;
      }
      p {
      font-size: 15px;
      }

      /*customize*/
      
      .col-sm-8.customize {
         border: 1px solid;
         margin-left: 15px;
         margin-top: 15px;
      }

      /**/

   </style>
<div id="right-panel" class="right-panel">

   <!-- /header -->
   <!-- Header-->
   
    <div class="wrapper invoice" style="margin:3%">
      <section class="invoice">

      <div class="breadcrumbs" style="margin-bottom:3%">
              <div class="col-md-4 page-header float-left">
                <div class="page-title float-right" style="margin-top:1%; margin-bottom:1%">
                <img src="{{ URL::asset('images/logo.png') }}" width="100" height="100">
                </div>
              </div>      
              <div class="col-md-8 page-header float-right">
                <div class="page-title" style="margin-top:5%; margin-bottom:3%">
                <h4 style="text-align:left">Tax Invoice/Bill of Supply/Cash Memo</h4>
                <h4 style="text-align:left">(Triplicate for Supplier)</h4>
                </div>
              </div>
          </div>
          @php
            $delivery_state = null;
          @endphp
          <div class="breadcrumbs">
              <div class="col-md-6 page-header float-left">
                <div class="page-title" style="margin-top:1%; margin-bottom:1%">
                  {{-- <h3>R.PRATAP NAIR</h3> --}}
                  <h4>{{ $constants[0]->address }}</h4><br>
                  <h5>Mobile Number: {{ $constants[0]->phone }}</h5>
                  <h5>E-mail: {{ $constants[0]->email }}</h5>
                </div>
              </div>
              <div class="page-header float-right">
                <div class="page-title" style="margin-top:5%">
                <h5>GST NO: 33ABFPN4423L1ZO</h5>
                {{-- <h5>PAN NO: ABFPN4423L</h5>
                <h5>TIN UNDER VAT: 33666180520</h5> --}}
                <br>
                <h6>Today's Date: {{ date('d-m-Y') }}</h6>
                </div>
              </div>
          </div>


          <div class="breadcrumbs">
              <div class="col-md-6 page-header float-left">
                <div class="page-title" style="margin-top:1%; margin-bottom:1%">
                  <h3>Billing Address</h3>
                  <div style="margin-left:1%">               
                  @foreach($billing_address as $add)
                  @if( $orders[0]->billing_address == $add->address_id )
                  <h5>{{ $add->address_fname }} {{ $add->address_lname }}</h5>
                  <h6>{{ $add->address_line_1 }},</h6>
                  <h6>{{ $add->address_line_2 }},</h6>
                  <h6>{{ $add->city}},</h6>
                  <h6>{{ $add->state }}-{{ $add->zip }}.</h6>
                  @break
                  @else
                  @endif
                  @endforeach
    </div>
                </div>
              </div>      
              <div class="col-md-4 page-header float-right">
                <div class="page-title" style="margin-top:1%; margin-bottom:1%;margin-left: 29%">
                <h3>Shipping Address</h3>
                <div style="margin-left:1%">
                @foreach($delivery_address as $add)
                  @if( $orders[0]->delivery_address == $add->address_id )
                  @php
                    $delivery_state = $add->state
                  @endphp
                  <h5>{{ $add->address_fname }} {{ $add->address_lname }}</h5>
                  <h6>{{ $add->address_line_1 }},</h6>
                  <h6>{{ $add->address_line_2 }},</h6>
                  <h6>{{ $add->city}},</h6>
                  <h6>{{ $add->state }}-{{ $add->zip }}.</h6>
                  @break
                  @else
                  @endif
                  @endforeach
                </div>
                </div>
              </div>
          </div>
        

          <div class="order" style="background:white; padding-bottom:18px;">
          <div class="breadcrumbs">
          
              <div class="page-header float-left">
                <div class="page-title" style="margin-top:3%">
                  <h2>Order Details</h2>
                </div>
              </div>      
              
            </div>
          
        
      
      <?php
    //  echo "<pre>"; print_r($orders[0]);exit;
      ?>

      <div class="page-title">
         <div class="col-sm-4 invoice-col">
            <label>User Name :</label> {{$orders[0]->first_name}} <br>
            <label>Order ID :</label> {{$orders[0]->order_id}} <br>
            <label>Order Date:</label> @php
                                  $hijri= date('d-m-Y  h:i A', strtotime($orders[0]->order_time));
                                  @endphp
                                  {{$hijri}}<br>
            <label>Payment Method:</label> {{$orders[0]->payment_mode}} <br>
         </div>
         <div class="pull-right" style="margin-right:1%">
            <a href="{{url('/orderinvoice')}}/{{$orders[0]->order_id}}" target="_blank" class="printinvoice"><button class="printinvoice">Print Proforma Invoice</button></a><br>
            <h5>Order Status</h5>
                    @if ($orders[0]->order_status==0)
                        <div class="label cancel">Cancelled On</div>
                        <div class="label cancel">@php
                                  $hijri= date('d-m-Y  h:i A', strtotime($orders[0]->cancelled_time));
                                  @endphp
                                  {{$hijri}}</div>
                      @elseif ($orders[0]->order_status==1)
                      <div class="label placed">Placed On</div>
                        <div class="label placed">@php
                                  $hijri= date('d-m-Y  h:i A', strtotime($orders[0]->order_time));
                                  @endphp
                                  {{$hijri}}</div>
                      @elseif ($orders[0]->order_status==2)
                      <div class="label confirm">Confirmed On</div>
                        <div class="label confirm">@php
                                  $hijri= date('d-m-Y  h:i A', strtotime($orders[0]->processed_time));
                                  @endphp
                                  {{$hijri}}</div>
                      @elseif ($orders[0]->order_status==3)
                      <div class="label dispatched">Dispatched On</div>
                        <div class="label dispatched">@php
                                  $hijri= date('d-m-Y  h:i A', strtotime($orders[0]->pshipped_time));
                                  @endphp
                                  {{$hijri}}</div>
                      @elseif ($orders[0]->order_status==4)
                      <div class="label delivered">Delivered On</div>
                        <div class="label delivered">@php
                                  $hijri= date('d-m-Y  h:i A', strtotime($orders[0]->delivery_time));
                                  @endphp
                                  {{$hijri}}</div>
                      @elseif ($orders[0]->order_status==5)
                        <div class="label returned">Returned</div>
                      @endif
            
         </div>
      </div>

      <table style="width:98%; margin-left:1%">
         <tr>
           <th>S.No</th>
           <th>Description</th>
           <th>Unit Price</th>
           <th>Qty</th>
           <th>Net Amount</th>
           <th>Tax Rate</th>
           <th>Tax type</th>
           <th>Tax amount</th>
           <th>Total Amount</th>
        </tr>

        @php
        $total_product_tax=0;
        $tax_total=0;
        @endphp
            <?php $sno=1; $total_amt=0;?>
            @for ($i=0; $i < count($orders) ; $i++)
            @php
            $net_amt = 0;
            $tax_rate = $constants[0]->igst_percent;
            $tax_type = ["CGST", "SGST", "IGST"];
            $delivery_cost = $orders[0]->delivery_cost;
            $delivery_tax = 0;
            $tax_amount = 0;
            $sub_total = 0;
            $igst = $constants[0]->igst_percent/100;
            $total = 0;
            @endphp
            <tr>
               <?php 
                  //  echo "<pre>"; print_r($orders);
               ?>
               <td><?php echo $sno; ?></td>
               <td>{{ $orders[$i]->product_name }} ( {{ $orders[$i]->variety }} )</td>
               <td>Rs. {{ $orders[$i]->price - ($orders[$i]->igst_product/$orders[$i]->quantity) }}</td>
               <td>{{ $orders[$i]->quantity }}</td>
               @php
               $net_amt = $orders[$i]->order_price - $orders[$i]->igst_product;
               @endphp
               <td>Rs. {{ $orders[$i]->order_price - $orders[$i]->igst_product }}</td>
               @php
                if($delivery_state != $constants[0]->state) {
                  if($orders[$i]->product_id == 31) {
                    $dummy_tax = 5;
                    echo "<td>$dummy_tax%</td>";
                    echo "<td>$tax_type[2]</td>";
                  } else {
                    $dummy_tax = $tax_rate;
                    echo "<td>$dummy_tax%</td>";
                    echo "<td>$tax_type[2]</td>";
                  }
                } else {
                  if($orders[$i]->product_id == 31) {
                    $dummy_tax = 5 / 2;
                    echo "<td>$dummy_tax%, $dummy_tax%</td>";
                    echo "<td>$tax_type[0] &amp; $tax_type[1]</td>";
                  } else {
                    $dummy_tax = $tax_rate / 2;
                    echo "<td>$dummy_tax%, $dummy_tax%</td>";
                    echo "<td>$tax_type[0] &amp; $tax_type[1]</td>";
                  }
                }
               @endphp
               @php
                  $tax_amount = round($igst*$orders[$i]->order_price,2);
               @endphp
               <td>Rs. {{ $orders[$i]->igst_product }}</td>
               @php
               $total_product_tax += $orders[$i]->igst_product;
               $sub_total= round($orders[$i]->order_price - $orders[$i]->igst_product,2);
               @endphp
               <td>Rs. {{ $orders[$i]->order_price }}</td>
            </tr>
               <?php 
                  $sno++;
                  $total_amt += $sub_total;
               ?>
            @endfor
            <tr>
            <td></td>
            <td>Shipping Cost</td>
            <td>Rs. {{ $delivery_cost }}</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            @php
            $shipping_amount = $delivery_cost
            @endphp
            <td>Rs. {{ $shipping_amount }}</td>
            </tr>

         <tr class="words">
            <td colspan="7" style="text-align:center;padding-right:5px;">
               <ul>
                  <li class="amount">Grand Total------->(Rounded Off)</li>
               </ul>
            </td>
            @php
            $tax_total = round($total_product_tax + $delivery_tax);
            @endphp
            <td>Rs. {{ $tax_total }}</td>
            <td style="text-align:center;padding-right:5px;">
               <ul>
                  <?php
                        $total_amount = $total_amt + $shipping_amount + $tax_total;
                      ?>
                     <li class="amount">Rs. {{ number_format($total_amount) }}</li>
               </ul>
            </td>
         </tr>

         <tr class="words">
           <td colspan="3" style="text-align:center;padding-right:5px;"><h5>Total amount in words</h5></td>
<td colspan="6" style="text-align:center;padding-right:5px;">
        <?php
        function convert_number_to_words($number) {
            $hyphen      = '-';
            $conjunction = ' and ';
            $separator   = ' ';
            $negative    = 'negative ';
            $decimal     = ' point ';
            $dictionary  = array(
                0                   => 'zero',
                1                   => 'one',
                2                   => 'two',
                3                   => 'three',
                4                   => 'four',
                5                   => 'five',
                6                   => 'six',
                7                   => 'seven',
                8                   => 'eight',
                9                   => 'nine',
                10                  => 'ten',
                11                  => 'eleven',
                12                  => 'twelve',
                13                  => 'thirteen',
                14                  => 'fourteen',
                15                  => 'fifteen',
                16                  => 'sixteen',
                17                  => 'seventeen',
                18                  => 'eighteen',
                19                  => 'nineteen',
                20                  => 'twenty',
                30                  => 'thirty',
                40                  => 'fourty',
                50                  => 'fifty',
                60                  => 'sixty',
                70                  => 'seventy',
                80                  => 'eighty',
                90                  => 'ninety',
                100                 => 'hundred',
                1000                => 'thousand',
                100000              => 'lakh'
            );
            if (!is_numeric($number)) {
                return false;
            }
            if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
                // overflow
                trigger_error(
                    'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
                    E_USER_WARNING
                );
                return false;
            }
            if ($number < 0) {
                return $negative . convert_number_to_words(abs($number));
            }
            $string = $fraction = null;
            if (strpos($number, '.') !== false) {
                list($number, $fraction) = explode('.', $number);
            }
            switch (true) {
                case $number < 21:
                    $string = $dictionary[$number];
                    break;
                case $number < 100:
                    $tens   = ((int) ($number / 10)) * 10;
                    $units  = $number % 10;
                    $string = $dictionary[$tens];
                    if ($units) {
                        $string .= $hyphen . $dictionary[$units];
                    }
                    break;
                case $number < 1000:
                    $hundreds  = $number / 100;
                    $remainder = $number % 100;
                    $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                    if ($remainder) {
                        $string .= $conjunction . convert_number_to_words($remainder);
                    }
                    break;
                default:
                    $baseUnit = pow(1000, floor(log($number, 1000)));
                    $numBaseUnits = (int) ($number / $baseUnit);
                    $remainder = $number % $baseUnit;
                    $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                    if ($remainder) {
                        $string .= $remainder < 100 ? $conjunction : $separator;
                        $string .= convert_number_to_words($remainder);
                    }
                    break;
            }
            if (null !== $fraction && is_numeric($fraction)) {
                $string .= $decimal;
                $words = array();
                foreach (str_split((string) $fraction) as $number) {
                    $words[] = $dictionary[$number];
                }
                $string .= implode(' ', $words);
            }
            return ucfirst($string);
        }
        ?>
<h5>{{ convert_number_to_words(round($total_amount)) }} only</h5>
</td>
</tr>
</table>

      
   </section>
  </div>
</div>
    </div>
<script type="text/javascript">
    $(document).ready(function(){

      function myFunction(i)
      {
         var img_path = $("#img_path"+i).val();
         $("#preview-image"+i).attr("src",img_path);

         var left = $("#orientation1left"+i+"").val();
         var top = $("#orientation1top"+i).val();
         var width = $("#img_width"+i).val();
         var height = $("#img_height"+i).val();

         $(".image-preview-div"+i).css({"width":width,"height":height});
         $('#preview-text1'+i).css({ "top" : top,"left":left }).show();
         $('#orientation1left'+i).val(left);
         $('#orientation1top'+i).val(top);
      
         var left = $("#orientation2left"+i).val();
         var top = $("#orientation2top"+i).val();
         var width = $("#img_width"+i).val();
         var height = $("#img_height"+i).val();

/*         if(!isset(left))
         {
            $('#preview-text2'+i).hide();
         }*/

         $(".image-preview-div"+i).css({"width":width,"height":height});
         $('#preview-text2'+i).css({ "top" : top,"left":left }).show();
         $('#orientation2top'+i).val(left);
         $('#orientation2top'+i).val(top);

      }
      for (i=0;i<<?php echo count($orders) ?>;i++) {
         myFunction(i);
      }
   });
</script>

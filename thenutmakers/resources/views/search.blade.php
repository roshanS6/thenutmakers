<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="{{ URL::asset('css/view_bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/view_style.css') }}">
  <link href="https://fonts.googleapis.com/css?family=Bai+Jamjuree|Source+Serif+Pro" rel="stylesheet">
  <script src='https://www.google.com/recaptcha/api.js'></script>
  <title>The Nut Makers</title>
  <style>
    .top{
      padding-top: 20%;
    }
    .top h2{
      color: #b48a34;
    }
  </style>
</head>
 
<body>
  <header class="header-section" id="header-1">
    <!-- Fixed navbar -->
    <nav class="navbar navbar-expand-md navbar-dark fixed-top header" id="myHeader">
      <a class="navbar-brand" href="{{ URL('/') }}">
        <img src="{{ URL::asset('images/logo.png') }}" width="100" height="100">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse justify-content-end" id="navbarCollapse">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="{{ URL('/') }}">Home</a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="{{ url('/product') }}">Shop</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/aboutus') }}">About Us</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/contactus') }}">Contact</a>
          </li>
          <li class="nav-item svg-icon search-button">
            <a href="#" class="search-toggle" data-selector="#header-1">
                  <img src="{{ URL::asset('images/search.svg') }}">
              </a>
          </li>
          <li class="nav-item svg-icon">
            <?php
            if(!Session::has('user_id')){
             ?>
              <a class="nav-link" href="{{ url('/profile') }}">
              <img src="{{ URL::asset('images/user.svg') }}">
            </a>
            <?php
            }
            else{
              ?>            
            <div class="user-area dropdown">
              <a href="#" class="nav-link" data-toggle="dropdown" aria-expanded="false">
                <img src="{{ URL::asset('images/user.svg') }}" >
              </a>
              <div class="user-menu dropdown-menu">
                <a class="nav-link" href="{{ url('/profile') }}" style="color:black">My profile</a>                      
                <a class="nav-link" href="{{ url('/logout') }}" style="color:black">Logout</a>
              </div>
            </div>
            <?php
            }
            ?>
          </li>
          <li class="nav-item svg-icon">
            <a class="nav-link" href="{{ url('/cart') }}">
              <img src="{{ URL::asset('images/shopper.svg') }}">
            </a>
            <?php
            if(Session::has('cart_count')) {
             ?>
            <span class="dot"> </span>
            <?php 
              }
            ?>
          </li>
        </ul>
        <form action="{{ url('search') }}" class="search-box">
          <input type="text" class="text search-input" name="key" placeholder="Type here to search..." />
        </form>
      </div>
    </nav>
  </header>

  <main role="main" class="wrapper">
    <div class="message" style="padding-top:1%">
      @if (Session('message'))
        <div class="alert alert-success alert-dismissible fade show" role="alert" style="font-weight: 400;">
            <span class="badge badge-pill" style="color:black">{{ Session('message') }}</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          {{ Session::forget('message') }}
        </div>
      @endif
      @if (Session('error'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert" style="font-weight: 400;">
            <span class="badge badge-pill" style="color:black">{{ Session('error') }}</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          {{ Session::forget('error') }}
        </div>
      @endif
    </div> 
    <div class="container-fluid products">
    @if(count($products)>0)
      <div class="offset-1 col-10 col-lg-10 col-md-10 col-sm-10 align-self-center">          
        <div class="row tab-content tab-carousel">
          <div id="carouselExampleControls-standard" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">                
              @php                      
                $is_active = true;
                $i = 0;
              @endphp
              @for($k=0; $k< count($products); $k++)                                 
              <?php if ($i % 6 == 0):?>
                <div class="carousel-item<?php if ($is_active) echo ' active'?>">
                  <div class="row product-section">
                    <?php endif?>
                    @for($l=0; $l< 6; $l++)
                      @if(isset($products[$k]->id))                                      
                        <div class="col-4 col-lg-4 col-md-4 col-sm-4">
                          <div class="product-card">                       
                            <a href="{{ url('/product_details') }}/{{ $products[$k]->id }}">
                              <img class="d-block w-100" src="{{ url::asset($products[$k]->image_path) }}" alt="Product">                              
                              <div class="product-content">
                                <div class="product-title">{{  $products[$k]->product_name }}</div>
                              </div>
                            </a>
                          </div>
                        </div>
                        <?php
                        if($l!=5){
                          $k++;
                          }
                          $is_active=false;
                        ?>
                      @endif
                    @endfor                           
                  </div>                     
                </div>
              @endfor
            </div>              
            <a href="#carouselExampleControls-standard" data-slide="prev">
              <img src="{{ url::asset('images/prev.svg') }}" class="carousel-control-prev arrow-style" role="button"> <span class="sr-only">Previous</span>
            </a>
            <a href="#carouselExampleControls-standard"  data-slide="next">
              <img src="{{ url::asset('images/next.svg') }}" role="button" class="carousel-control-next arrow-style">
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
      </div>
      @else             
      <div class="carousel-item active">
        <div class="row col-md-12">                                     
          <div class="col-12 col-lg-12 col-md-12 col-sm-12">
            <div class="product-card col-md-12">
                <div class="product-content col-md-12">
                  <div class="top">
                    <div class="product-title col-md-12"><center><h2>No Such Product</h2></center></div>
                  </div>
                </div>
            </div>
          </div>                           
        </div>                     
      </div>
    @endif   
    </div>
  </div>
    <footer>
      <div class="container" id="contact" style="padding-top:50px;padding-bottom:30px">
        <div class="footer-section">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 align-self-end">
              <form action={{ url('subscribe') }} method="post">
                {{ csrf_field() }}
                <div class="input-group">
                  <input type="text" class="form-control" name="email" placeholder="Enter email to stay in touch" aria-label="Recipient's username" aria-describedby="basic-addon2" required>
                  <div class="input-group-append">
                    <button class="input-group-text" id="basic-addon2" type="submit">SUBSCRIBE</button>
                  </div>
                </div>
              </form>
              <ul class="list-unstyled social-media">
                {{-- <li>Facebook</li>
                <li>Instagram</li> --}}
                <div class="copyright"><p class="pull-right resvered"> <b>2018  Copyright All Rights Reserved. Designed By </b><a href="http://www.shade6.com/" style="color: #b1b0b0;">Shade Six Interactive.</a></div>
                </ul>
            </div>
            <div class="col col-lg-2 col-md-6 col-sm-6">
              <h3>Help</h3>
              <ul class="list-unstyled list-section">
                <li>{{ $constants[0]->phone }}</li>
                <li>{{ $constants[0]->email }}</li>
                <li><a href="#" data-toggle="modal" data-target="#exampleModal">Enquiry</a></li>
              </ul>
            </div>
            <div class="col col-lg-2 col-md-6 col-sm-6">
              <h3>Your Account</h3>
              <ul class="list-unstyled list-section">
                  <?php
                  if(!Session::has('user_id')){
                    ?>
                    <li><a href="{{ url('signin') }}">Sign in</a></li>
                  <?php 
                  }
                  ?>
                {{-- <li>Forgot Password</li> --}}
                <li><a href="{{ url('profile') }}">Order History</a></li>
                <li><a href="{{ url('cart') }}">Shopping Bag</a></li>
              </ul>
            </div>
            <div class="col col-lg-2 col-md-6 col-sm-6">
              <h3>Customer Service</h3>
              <ul class="list-unstyled list-section">
                <li><a href="{{ url('terms') }}">Terms & Conditions</a> / <a href="{{ url('privacy') }}">Privacy Policy</a></li>
                <li><a href="{{ url('refund_policy') }}">Shipping and Returns</a></li>
                {{-- <li><a href="{{ url('terms') }}">Terms & Conditions</a></li> --}}
                <li><a href="{{ url('faqs') }}">Faqs</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </footer>
  </main>
  <div class="modal fade modal-style" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Have A Question</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body form-section">
          <form id="form" action="{{ url('/enquiry') }}" method="post">
            {{ csrf_field() }}
            <div class="form-group">
              <input type="name" name="name" class="form-control" id="exampleInputname" aria-describedby="emailHelp" placeholder="Name" required>
            </div>
            <div class="form-group">
              <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Email" required>
            </div>
            <div class="form-group">
              <input type="subject" name="subject" class="form-control" id="exampleInputEmail1" placeholder="Subject" required>
            </div>
            <div class="form-group">
              <textarea class="form-control" name="message" id="exampleFormControlTextarea1" placeholder="Message" rows="3" required></textarea>
            </div>
            <div class="form-group">
              <span class="msg-error error"></span>
              <div class="g-recaptcha" data-sitekey="6LeKJokUAAAAANZ9WFXmv7QlGC-2_AwY2_yUW-YU"></div>
            </div>            
            <div class="modal-footer">
              <button type="submit" class="btn primary-button" style="padding-top:3%; padding-bottom:3%">SEND</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</body>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="{{ URL::asset('js/jquery-3.2.1.slim.min.js') }}"></script>
  <script src="{{ URL::asset('js/popper.min.js') }}"></script>
  <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
  <script>
    window.onscroll = function() {
      myFunction()
    };

    var header = document.getElementById("myHeader");
    var sticky = header.offsetTop;

    function myFunction() {
      if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
      } else {
        header.classList.remove("sticky");
      }
    }
  </script>
  <script type="text/javascript">
    function validateForm() {

      var $captcha = $( '#recaptcha' ),response = grecaptcha.getResponse();

      if (response.length === 0)
      {
        $( '.msg-error').text( "reCAPTCHA is mandatory" );
        if( !$captcha.hasClass( "error" ) ){
          $captcha.addClass( "error" );
        }
        return false;
      }
      else
      {
        $( '.msg-error' ).text('');
        $captcha.removeClass( "error" );
      }
    }
  </script>  
  <script>
    $(document).ready(function() {

      $(".next-step").click(function(e) {

        var $active = $('.nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);

      });
      $(".prev-step").click(function(e) {

        var $active = $('.nav-tabs li.active');
        prevTab($active);

      });
    });

    function nextTab(elem) {
      $(elem).next().find('a[data-toggle="tab"]').click();
    }

    function prevTab(elem) {
      $(elem).prev().find('a[data-toggle="tab"]').click();
    }
  </script>
  <script>
    $('.header').on('click', '.search-toggle', function(e) {
      var selector = $(this).data('selector');
      $(".search-input").show();
      $(selector).toggleClass('show').find('.search-input').focus();
      $(this).toggleClass('active');

      e.preventDefault();
    });
  </script>
</html>



<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="{{ URL::asset('css/view_bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/view_style.css') }}">
  <link href="https://fonts.googleapis.com/css?family=Bai+Jamjuree|Source+Serif+Pro" rel="stylesheet">
  <title>The Nut Makers</title>
  <style>
  a, a:hover{
    text-decoration: none;
  }
  </style>
</head>

<body>

  <header class="header-section"  id="header-1">
    <!-- Fixed navbar -->
    <nav class="navbar navbar-expand-md navbar-dark fixed-top header" id="myHeader">
      <a class="navbar-brand" href="{{ url('/') }}">
        <img src="{{ url::asset('images/logo.png') }}" width="100" height="100">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      <div class="collapse navbar-collapse justify-content-end" id="navbarCollapse">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/') }}">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/product') }}">Shop</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/aboutus') }}">About Us</a>
          </li>
          <!-- <li class="nav-item">
            <a class="nav-link" href="#">Contact</a>
          </li> -->
          <li class="nav-item svg-icon search-button">
            <a href="#" class="search-toggle" data-selector="#header-1">
                  <img src="{{ URL::asset('images/search.svg') }}">
              </a>
          </li>
          <li class="nav-item svg-icon">
            <?php
            if(!Session::has('user_id')){
             ?>
              <a class="nav-link" href="{{ url('/profile') }}">
              <img src="{{ URL::asset('images/user.svg') }}">
            </a>
            <?php
            }
            else{
              ?>
            <div class="user-area dropdown">
            <a href="#" class="nav-link" data-toggle="dropdown" aria-expanded="false">
                     <img src="{{ URL::asset('images/user.svg') }}" >
                     </a>
                     <div class="user-menu dropdown-menu">
                     <a class="nav-link" href="{{ url('/profile') }}" style="color:black">My profile</a>
                        <a class="nav-link" href="{{ url('/logout') }}" style="color:black">Logout</a>
                     </div>
            </div>
            <?php
            }
            ?>
          </li>
          <li class="nav-item svg-icon active">
            <a class="nav-link" href="{{ url('/cart') }}">
              <img src="{{ URL::asset('images/shopper.svg') }}">
            </a>
            <?php
            if(Session::has('cart_count')) {
             ?>
            <span class="dot"></span>
            <?php
              }
            ?>
          </li>
        </ul>
        <form action="{{ url('search') }}" class="search-box">
          <input type="text" class="text search-input" name="key" placeholder="Type here to search..." />
        </form>
      </div>
    </nav>
  </header>

  <main role="main" class="wrapper">
      <div class="message" style="padding-top:1%">
        @if (Session('message'))
          <div class="alert alert-success alert-dismissible fade show" role="alert" style="font-weight: 400;">
              <span class="badge badge-pill" style="color:black">{{ Session('message') }}</span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            {{ Session::forget('message') }}
          </div>
        @endif
        @if (Session('error'))
          <div class="alert alert-warning alert-dismissible fade show" role="alert" style="font-weight: 400;">
              <span class="badge badge-pill" style="color:black">{{ Session('error') }}</span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            {{ Session::forget('error') }}
          </div>
        @endif
      </div>
    <div class="container-fluid">

      <div class="row align-items-center wrapper-section">

        <div class="col-1 col-lg-2">
          <!--<div id="turn-straight">
            <ol>
              <li><a href="">Healthy and delicious</a></li>
              <li><a href="">All-new fun flavours</a></li>
            </ol>
          </div>-->
        </div>
        <div class="col-10 col-lg-8 col-md-10 col-sm-10">
          <h2>Cart</h2>
          @if($cart_products!=null)
          <div class="row">

            <div class="shopping-cart">

              <div class="column-labels row">
                <label class="product-image">Image</label>
                <label class="product-details">Product</label>
                <label class="product-price">Price</label>
                <label class="product-quantity">QTY</label>
                <label class="product-line-price">Total</label>
                <label class="product-removal">Remove</label>
              </div>

              @php
                $itemcount = 0;
                $amount = 0;
                $count = 0;
                $net_amt = 0;
                $total_price = 0;
                $n=1;
              @endphp
                <form id="checkout" action="{{ url('/checkout') }}" method="post">
                  {{ csrf_field() }}
                  @foreach($cart_products as $key=>$product_array)
                    @php
                      $price = $product_array->price * $product_array->cart_quantity;
                      $total_price = $price;
                    @endphp
                    <div class="product row align-items-center">
                      <div class="product-image">
                        <img src="{{ url::asset($product_array->image_path) }}">
                      </div>
                      <div class="product-details">
                        <div class="product-title"> <a href="{{ url('/product_details') }}/{{ $product_array->product_id }}">{{ $product_array->product_name }}</a></div>
                        <p class="product-description">{{ $product_array->package_size }}</p>
                      </div>
                      <div class="product-price svg-icon price" id="price"><img src="{{ url::asset('images/rupee.svg') }}">{{ $product_array->price }}</div>

                      <div class="product-quantity">
                        <div class="input-group">
                          <div class="input-group-prepend">
                          @if($product_array->cart_quantity <= $product_array->stock)
                          <a href="{{ url('/cart/decreasequantity') }}/{{ $product_array->cart_quantity }}/{{ $product_array->product_id }}/{{ $product_array->variety }}">
                          <div class="input-group-text minus" id="decrease">
                          -</div>
                          </a>
                          @endif
                          </div>
                          <input type="text" class="form-control qty" id="quantity" name="quantity{{$n++}}" min="1" value="{{ $product_array->cart_quantity }}" max="{{ $product_array->stock }}" readonly required>

                          <div class="input-group-prepend">
                          @if($product_array->cart_quantity <= $product_array->stock)
                          <a href="{{ url('/cart/increasequantity') }}/{{ $product_array->cart_quantity }}/{{ $product_array->product_id }}/{{ $product_array->variety }}">
                          <div class="input-group-text add" id="increase">
                          +</div>
                          </a>
                          @endif
                          </div>
                        </div>
                      </div>
                      @if($product_array->cart_quantity <= $product_array->stock)
                      <div class="product-line-price svg-icon total" id="total_price"><img class="svg-icon" src="{{ url::asset('images/rupee.svg') }}"><span class="tolval">{{ $total_price }}</span></div>
                          @php
                            $net_amt += $total_price;
                          @endphp
                      @else

                        <div class="product-line-price svg-icon total" id="total_price" style="color:white; text-align:center">
                        <span>Your cart value is greater than the Product stock. Kindly remove it.</span>
                        </div>
                        @endif
                      <div class="product-removal">
                        <span class="remove-product">
                        <a href="{{ url('removefromcart') }}/{{ $product_array->product_id }}/{{ $product_array->variety }}" style="color:white; font-size:1rem;">
                        Remove
                        </a>
                      </span>
                      </div>
                    </div>
                    @endforeach



              <!-- <div class="product row align-items-center">
                <div class="product-image">
                  <img src="images/Black-Chilli-Cheese.png">
                </div>
                <div class="product-details">
                  <div class="product-title">The NUt Makeers - Chilli Cheese</div>
                  <p class="product-description">80g</p>
                </div>
                <div class="product-price svg-icon"><img src="images/rupee.svg">12.99</div>
                <div class="product-quantity">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <div class="input-group-text">+</div>
                    </div>
                    <input type="text" class="form-control" id="inlineFormInputGroup" value="2">
                    <div class="input-group-prepend">
                      <div class="input-group-text">-</div>
                    </div>
                  </div>
                </div>

                <div class="product-line-price svg-icon"><img src="images/rupee.svg"> 25.98</div>
                <div class="product-removal">
                  <button class="remove-product">
                  Remove
                </button>
                </div>
              </div> -->

              <div class="totals">
                <div class="totals-item">
                  <label>Subtotal</label>
                  <div class="totals-value svg-icon net" id="cart-subtotal"><img class="svg-icon" src="{{ url::asset('images/rupee.svg') }}">
                  {{-- <input type="hidden" class="netval" name="netval{{$n}}"> --}}
                  <span class="netval">{{ $net_amt }}</span></div>
                </div>
              </div>
              <div class="checkout-btn">
                @php
                $n++;
                @endphp
                <button type="submit" class="btn primary-button">
                  <a>Checkout</a></button>
              </div>
              <div class="checkout-btn">
                <a href="{{ url('product') }}">
                  <h5 style="padding: 10px 15px;" class="btn primary-button">
                    <span>Continue Shopping</span>
                  </h5>
                </a>
              </div>
            </form>
            </div>


          </div>


        @else
        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
          <center>
              <h2 class="grey">Put Something in cart</h2>
          </center>
        </div>
        <div class="checkout-btn">
          <center>
            <a href="{{ url('product') }}">
              <h5 style="padding: 10px 15px;" class="btn primary-button">
                <span>Continue Shopping</span>
              </h5>
            </a>
          </center>
        </div>
        @endif



        </div>

      </div>
    </div>
  </main>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="{{ URL::asset('js/jquery-3.2.1.slim.min.js') }}"></script>
  <script src="{{ URL::asset('js/popper.min.js') }}"></script>
  <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
  <script>
    window.onscroll = function() {
      myFunction()
    };

    var header = document.getElementById("myHeader");
    var sticky = header.offsetTop;

    function myFunction() {
      if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
      } else {
        header.classList.remove("sticky");
      }
    }
  </script>
  <script>
    $('.header').on('click', '.search-toggle', function(e) {
      var selector = $(this).data('selector');
      $(".search-input").show();
      $(selector).toggleClass('show').find('.search-input').focus();
      $(this).toggleClass('active');

      e.preventDefault();
    });
  </script>
  <script>
//  let count = 0;
//  let countEl = document.getElementById("quantity");
//  function plus(){
//      count++;
//      countEl.value = count;
//  }
//  function minus(){
//    if (count > 1) {
//      count--;
//      countEl.value = count;
//   }
// }

    $('.add').click(function (e) {

        var quantity = parseInt($(this).closest('div.product-quantity').find('.qty').val());
        var price = parseInt($(this).closest('div.product-quantity').prev().text());
        var limit = parseInt($(this).closest('div.product-quantity').find('.qty').attr("max"));

        if(quantity<limit){
          quantity++;
          $(this).closest('div.product-quantity').find('.qty').val(quantity);
          var quantity = parseInt($(this).closest('div.product-quantity').find('.qty').val());
          var total = quantity*price;

          parseInt($(this).closest('div.product-quantity').next().find(".tolval").text(total));
          // $('.total').text(quantity*price);
          var net_amt = parseInt($('.netval').text());
          var add= parseInt(net_amt + price);
          $('.netval').text(add);
        }
});

$('.minus').click(function (e) {

        var quantity = parseInt($(this).closest('div.product-quantity').find('.qty').val());
        var price = parseInt($(this).closest('div.product-quantity').prev().text());
        var limit = parseInt($(this).closest('div.product-quantity').find('.qty').attr("min"));

        if(quantity>limit){
          quantity--;
          parseInt($(this).closest('div.product-quantity').find('.qty').val(quantity));
          var quantity = parseInt($(this).closest('div.product-quantity').find('.qty').val());
          var total = quantity*price;

          parseInt($(this).closest('div.product-quantity').next().find(".tolval").text(total));
          var net_amt = parseInt($('.netval').text());
          var minus = parseInt(net_amt - price);
          $('.netval').text(minus);
        }
});
 </script>

</body>

</html>

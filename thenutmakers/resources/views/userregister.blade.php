<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Register</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />

</head>
<body>
<div class="message">
@if(Session('error'))
<div class="alert alert-warning" style="margin-top:20px;font-weight: 400;">
{{ Session('error') }}
{{ Session::forget('error') }}
@endif
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<form id="form" action="{{ url('register') }}" method="post">
{{ csrf_field() }}
First Name:<input type="text" name="first_name"/><br><br>
Last Name:<input type="text" name="last_name"/><br><br>
Email*<input type="text" id="email" name="email" required/><br>
<span id="error" style="color:red"></span><br>
Password*<input type="password" id="password" name="password" pattern=".{6,}" required title="6 characters minimum"/><br><br>
Confirm Password*<input type="password" id="cpassword" pattern=".{6,}" name="confirm_password" required title="6 characters minimum"/><br><br>
<span class="error" style="color:red"></span><br/>
<button type="submit" name="submit" class="btn btn-default">Submit</button>
</form>


<script>
var allowsubmit = false;
		$(function(){
			//on keypress 
			$('#cpassword').keyup(function(e){
				//get values 
				var pass = $('#password').val();
				var confpass = $(this).val();
				
				//check the strings
				if(pass == confpass){
					//if both are same remove the error and allow to submit
					$('.error').text('');
					allowsubmit = true;
				}else{
					//if not matching show error and not allow to submit
					$('.error').text('Password not matching');
					allowsubmit = false;
				}
			});
			
            $("#email").blur(function () {
         if($("#email").val()==""){
                  $("#email").css("border-color","red");
        }
         else if(!/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test($("#email").val())){
            $('#error').text('enter the Valid mail-id');
			allowsubmit = false;
         }else{
            $('#error').text('');
			allowsubmit = true;
         }
       });

			//jquery form submit
			$('#form').submit(function(){
			
				var pass = $('#password').val();
				var confpass = $('#cpassword').val();

				//just to make sure once again during submit
				//if both are true then only allow submit
				if(pass == confpass){
                    if(!/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test($("#email").val()))
                    {
                        $('#error').text('enter the Valid mail-id');
                        allowsubmit = false;
                    }
                    else{
                        allowsubmit = true;
				        }
                }
				if(allowsubmit){
					return true;
				}else{
					return false;
				}
			});
		});
	</script>


</html>
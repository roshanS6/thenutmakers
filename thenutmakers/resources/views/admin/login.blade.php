<!DOCTYPE html>
<html lang="en">
<head>
  <title>The Nut Makers</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
  <link rel="icon" type="image/png" href="{{ URL::asset('logincss/images/icons/favicon.ico')}}">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('logincss/vendor/bootstrap/css/bootstrap.min.css')}}">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('logincss/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('logincss/vendor/animate/animate.css')}}">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('logincss/vendor/css-hamburgers/hamburgers.min.css')}}">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('logincss/vendor/select2/select2.min.css')}}">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('logincss/css/util.css')}}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('logincss/css/main.css')}}">
<!--===============================================================================================-->
</head>
<style type="text/css">
  .copyright {
    border-radius: 25px;
    padding: 0 15px;
}
</style>
<body>

  <div class="limiter">
    <div class="container-login100">
      <div class="wrap-login100">
        <div class="login100-pic js-tilt" data-tilt>
        <img src="{{ URL::asset('/images/logo.png')}}" alt="The nut makers" width=80% height=60%>
        </div>

        <form class="login100-form validate-form" role="form" method="get" action="{{ url('/admin/processlogin') }}">
            

          <div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
            <input class="input100" type="text" name="mail" placeholder="Login ID">
            <span class="focus-input100"></span>
            <span class="symbol-input100">
              <i class="fa fa-envelope" aria-hidden="true"></i>
            </span>
          </div>

          <div class="wrap-input100 validate-input" data-validate = "Password is required">
            <input class="input100" type="password" name="password" placeholder="Password">
            <span class="focus-input100"></span>
            <span class="symbol-input100">
              <i class="fa fa-lock" aria-hidden="true"></i>
            </span>
          </div>

          <div class="container-login100-form-btn">
            <button class="login100-form-btn">
              Login
            </button>
          </div>

          <div class="text-center p-t-12">
            <span class="txt1">
            </span>
            <a class="txt2" href="#">
            </a>
          </div>

          <div class="text-center p-t-136">
            <a class="txt2" href="#">
            </a>
          </div>
        </form>
          <div class="copyright"><p class="pull-right resvered"> <b>2018  Copyright All Rights Reserved. Designed By </b><a href="http://www.shade6.com/" target="_blank" style="color:#0e5cab; font-weight:bold"> <img src="http://www.shade6.com/images/logo_white.png" class="img-reasponsive" width='100' height='30'></a></div>
      </div>
    </div>
  </div>




<!--===============================================================================================-->
  <script src="{{ URL::asset('logincss/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
<!--===============================================================================================-->
  <script src="{{ URL::asset('logincss/vendor/bootstrap/js/popper.js')}}"></script>
  <script src="{{ URL::asset('logincss/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<!--===============================================================================================-->
  <script src="{{ URL::asset('logincss/vendor/select2/select2.min.js')}}"></script>
<!--===============================================================================================-->
  <script src="{{ URL::asset('logincss/vendor/tilt/tilt.jquery.min.js')}}"></script>
  <script >
    $('.js-tilt').tilt({
      scale: 1.1
    })
  </script>
<!--===============================================================================================-->
  <script src="{{ URL::asset('logincss/js/main.js')}}"></script>
</body>
</html>

@extends('admin.layout.masterlayout')
@section('content')
<!-- Right Panel -->
<div id="right-panel" class="right-panel">
   <!-- Header-->
   <header id="header" class="header">
      <div class="header-menu">
         <div class="col-sm-7">
            <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
         </div>
         <div class="col-sm-5">
            <div class="user-area dropdown float-right">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
               <img class="user-avatar rounded-circle" src="{{ URL::asset('images/admin.png') }}" alt="User Avatar">
               </a>
               <div class="user-menu dropdown-menu">
                  <!-- <a class="nav-link" href="#"><i class="fa fa- user"></i>My Profile</a>
                     <a class="nav-link" href="#"><i class="fa fa- user"></i>Notifications <span class="count">13</span></a>
                     <a class="nav-link" href="#"><i class="fa fa -cog"></i>Settings</a> -->
                  <a class="nav-link" href="{{ url('/admin/logout') }}"><i class="fa fa-power -off"></i>Logout</a>
               </div>
            </div>
            <div class="language-select dropdown" id="language-select">
               <a class="dropdown-toggle" href="#" data-toggle="dropdown"  id="language" aria-haspopup="true" aria-expanded="true">
               <i class="flag-icon flag-icon-us"></i>
               </a>
               <div class="dropdown-menu" aria-labelledby="language" >
                  <div class="dropdown-item">
                     <span class="flag-icon flag-icon-fr"></span>
                  </div>
                  <div class="dropdown-item">
                     <i class="flag-icon flag-icon-es"></i>
                  </div>
                  <div class="dropdown-item">
                     <i class="flag-icon flag-icon-us"></i>
                  </div>
                  <div class="dropdown-item">
                     <i class="flag-icon flag-icon-it"></i>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </header>
   <!-- /header -->
   <!-- Header-->
   <div class="breadcrumbs">
      <div class="col-sm-4">
         <div class="page-header float-left">
            <div class="page-title">
               <h1>Constants</h1>
            </div>
         </div>
      </div>
      <div class="col-sm-8">
         <div class="page-header float-right">
            <div class="page-title">
               <ol class="breadcrumb text-right">
                  <li><a href="#">Constants</a></li>
                  @if(count($constant)>0)
                  <li class="active">Update Constants</li>
                  @else
                  <li class="active">Insert Constants</li>
                  @endif
               </ol>
            </div>
         </div>
      </div>
   </div>
   @if(session('message'))
   <div class="col-sm-12">
      <div class="alert  alert-success alert-dismissible fade show" role="alert">
        <span class="badge badge-pill badge-success">Success</span>{{session('message')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
   </div>
   @endif
   <div class="col-lg-12">
      <div class="card">
         <div class="card-header">
         @if(count($constant)>0)
            <strong>Update Constants</strong>
            @else
            <strong>Insert Constants</strong>
            @endif
         </div>
         <div class="card-body card-block">

          @if(count($constant)>0)
            <form action="{{ url('/admin/updateconstants') }}/{{ $constant[0]->id }}" method="post" enctype="multipart/form-data">

            <div class="row form-group">
               <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">E-Mail</label></div>
                  <div class="col-12 col-md-9"><input name="email"class="form-control" value="{{  $constant[0]->email }}" required/></div>
               </div>
               <div class="row form-group">
                  <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Phone Number</label></div>
                  <div class="col-12 col-md-9"><input type="text" name="phone" value="{{  $constant[0]->phone }}" class="form-control" required/></div>
               </div>
               <div class="row form-group">
                  <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Address</label></div>
                  <div class="col-12 col-md-9"><textarea name="address" rows="3" class="form-control" required>{{  $constant[0]->address }}</textarea></div>
               </div>
               <div class="row form-group">
                  <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">State</label></div>
                  <div class="col-12 col-md-9"><input type="text" name="state" value="{{  $constant[0]->state }}" class="form-control" required></div>
               </div>
               <div class="row form-group">
                  <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Shipping Cost</label></div>
                  <div class="col-12 col-md-9"><input type="number" step="0.01" name="shipping" value="{{  $constant[0]->shipping_cost }}" class="form-control" required></div>
               </div>
               <div class="row form-group">
                  <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">IGST Percentage (In percent)</label></div>
                  <div class="col-12 col-md-9"><input type="number" name="igst_percentage" value="{{  $constant[0]->igst_percent }}" class="form-control" required></div>
               </div>

               <!-- <div class="card-footer"> -->
                  <button type="submit" class="btn btn-success btn-sm">
                  <i class="fa fa-dot-circle-o"></i> Submit
                  </button>
                   <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
               <!-- </div> -->

            </form>

          @else<form action="{{ url('/admin/insertconstants') }}" method="post" enctype="multipart/form-data">
          <div class="row form-group">
                  <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">E-Mail</label></div>
                  <div class="col-12 col-md-9"><input name="email"class="form-control" required/></div>
               </div>
               <div class="row form-group">
                  <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Phone Number</label></div>
                  <div class="col-12 col-md-9"><input type="text" name="phone" class="form-control" required/></div>
               </div>
               <div class="row form-group">
                  <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Address</label></div>
                  <div class="col-12 col-md-9"><textarea name="address" rows="3" class="form-control" required></textarea></div>
               </div>
               <div class="row form-group">
                  <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">State</label></div>
                  <div class="col-12 col-md-9"><input type="text" name="state" class="form-control" required></div>
               </div>
               <div class="row form-group">
                  <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Shipping Cost</label></div>
                  <div class="col-12 col-md-9"><input type="number" step="0.01" name="shipping" class="form-control" required></div>
               </div>           
               <div class="row form-group">
                  <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">IGST Percentage (In number)</label></div>
                  <div class="col-12 col-md-9"><input type="number" name="igst_percentage" class="form-control" required></div>
               </div> 
               <!-- <div class="card-footer"> -->
                  <button type="submit" class="btn btn-success btn-sm">Insert</button>
                   <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
               <!-- </div> -->

            </form>     
          @endif

         </div>
      </div>
   </div>



</div>
<!-- Right Panel -->
@endsection

@extends('admin.layout.masterlayout')
@section('content')
<!-- Right Panel -->
<div id="right-panel" class="right-panel">
   <!-- Header-->
   <header id="header" class="header">
      <div class="header-menu">
         <div class="col-sm-7">
            <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
         </div>
         <div class="col-sm-5">
            <div class="user-area dropdown float-right">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
               <img class="user-avatar rounded-circle" src="{{ URL::asset('images/admin.png') }}" alt="User Avatar">
               </a>
               <div class="user-menu dropdown-menu">
                  <!-- <a class="nav-link" href="#"><i class="fa fa- user"></i>My Profile</a>
                     <a class="nav-link" href="#"><i class="fa fa- user"></i>Notifications <span class="count">13</span></a>
                     <a class="nav-link" href="#"><i class="fa fa -cog"></i>Settings</a> -->
                  <a class="nav-link" href="{{ url('/admin/logout') }}"><i class="fa fa-power -off"></i>Logout</a>
               </div>
            </div>
            <div class="language-select dropdown" id="language-select">
               <a class="dropdown-toggle" href="#" data-toggle="dropdown"  id="language" aria-haspopup="true" aria-expanded="true">
               <i class="flag-icon flag-icon-us"></i>
               </a>
               <div class="dropdown-menu" aria-labelledby="language" >
                  <div class="dropdown-item">
                     <span class="flag-icon flag-icon-fr"></span>
                  </div>
                  <div class="dropdown-item">
                     <i class="flag-icon flag-icon-es"></i>
                  </div>
                  <div class="dropdown-item">
                     <i class="flag-icon flag-icon-us"></i>
                  </div>
                  <div class="dropdown-item">
                     <i class="flag-icon flag-icon-it"></i>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </header>
   <!-- /header -->
   <!-- Header-->
   <div class="breadcrumbs">
      <div class="col-sm-4">
         <div class="page-header float-left">
            <div class="page-title">
               <h1>Products</h1>
            </div>
         </div>
      </div>
      <div class="col-sm-8">
         <div class="page-header float-right">
              <div class="page-title">
                 <ol class="breadcrumb text-right">
                    <li><a href="#">Products</a></li>
                    <li class="active">Add Products</li>
                 </ol>
              </div>
         </div>
      </div>
   </div>
   <div class="col-lg-12">
      <div class="card">
         <div class="card-header">
            <strong>Add Products</strong>
         </div>
         <div class="card-body card-block">
            <form action="{{url('admin/products/insert')}}" method="post" enctype="multipart/form-data">

                <div class="row form-group">
                  <div class="col col-md-3"><label for="select" class=" form-control-label">Product Category</label></div>
                  <div class="col-12 col-md-3">
                    <select name="category_name" class="form-control" required>
                        <option value="">Please select</option>
                        @foreach ($category as $cat)
                          <option value="{{ $cat->category_name }}" >{{ $cat->category_name }}</option>
                          </optgroup>
                        @endforeach
                    </select>
                  </div>
                </div>

                <div class="row form-group">
                  <div class="col col-md-3"><label for="text-input" class=" form-control-label">Product Name</label></div>
                  <div class="col-12 col-md-9"><input type="text" name="product_name" class="form-control" required></div>
                </div>

                <div class="row form-group">
                  <div class="col col-md-3"><label for="text-input" class=" form-control-label">Short Description</label></div>
                  <div class="col-12 col-md-9"><input type="text" name="short_description" class="form-control" minlength="5" maxlength="57" required></div>
                </div>

                <div class="row form-group">
                  <div class="col col-md-3"><label for="text-input" class=" form-control-label">Long Description</label></div>
                  <div class="col-12 col-md-9"><textarea name="long_description" rows="5" class="form-control" minlength="5" maxlength="740" required></textarea></div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Package Size</label></div>
                    <div class="col-12 col-md-9"><input type="text" name="package_size" class="form-control" required></div>
                  </div>
                  
               <div class="row form-group">
                  <div class="col col-md-3"><label for="text-input" class=" form-control-label">Priority</label></div>
                  <div class="col-12 col-md-9"><input type="text" name="priority" class="form-control" required>
               </div>

               <div class="row form-group">
                  <div class="col col-md-3"><label for="text-input" class=" form-control-label">Price</label></div>
                  <div class="col-12 col-md-9"><input type="text" name="price" class="form-control" required></div>
                </div>         

                <div class="row form-group">
                  <div class="col col-md-3"><label for="text-input" class=" form-control-label">Stock</label></div>
                  <div class="col-12 col-md-9"><input type="text" name="stock" class="form-control" required></div>
                </div>

                <div class="row form-group size_fields_wrap">
                </div>
                <button type="button" class="btn btn-primary add_size_field_button" name="button">Add more package size</button>

                 <div class="row form-group">
                  <div class="col col-md-3"><label for="select" class=" form-control-label">Featured Product</label></div>
                  <div class="col-12 col-md-3">
                    <select name="featured_product" class="form-control" required>
                          <option value="0" selected >None</option>
                          <option value="2" >Slider</option>
                    </select>
                  </div>
                </div>

                {{-- <div class="row form-group">
                  <div class="col col-md-3"><label for="file-input" class=" form-control-label">Banner Image</label></div>
                  <div class="col-12 col-md-9"><input type="file" accept="image/*" name="banner_image" class="form-control-file"></div>
               </div>
                --}}
                <div class="row form-group">
                  <div class="col col-md-3"><label for="file-input" class=" form-control-label">Image</label></div>
                  <div class="col-12 col-md-9"><input type="file" accept="image/*" name="image" class="form-control-file" required></div>
               </div>

               <div class="row form-group">
                  <div class="col col-md-3"><label for="file-input" class=" form-control-label">Other Related Images(Optional)</label></div>
                  <div class="col-12 col-md-9">
                      <div class="form-group"></div>
                      <div class="form-group image_fields_wrap"></div>
                      <button type="button" class="btn btn-primary add_image_field_button" name="button">Add More Images</button>
                 </div>
               </div>

              <div class="card-footer">
                  <button type="submit" class="btn btn-success btn-sm">
                  <i class="fa fa-dot-circle-o"></i> Submit
                  </button>
                  <button type="reset" class="btn btn-danger btn-sm" onclick='window.location.reload();'>
                  <i class="fa fa-ban"></i> Reset
                  </button>
                   <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<!-- Right Panel -->

<script type="text/javascript">

  $(document).ready(function() {

    var img_max_fields      = 4; //maximum input boxes allowed
    
    var img_wrapper         = $(".image_fields_wrap"); //Fields wrapper
    

    var x = 1; //initlal text box count
    $(".add_image_field_button").click(function(e){ //on add input button click
        e.preventDefault();
        if(x < img_max_fields){ //max input box allowed
            $(img_wrapper).append('<div><input type="file" accept="image/*" name="image'+x+'"><a href="#" class="remove_field"><span aria-hidden="true">&times;</span></a></div>'); //add input box
            x++; //text box increment
        }
    });

    $(img_wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault();
        $(this).parent('div').remove();
        x--;
    });
    
    var size_max_fields      = 5; //maximum input boxes allowed
    
    var size_wrapper         = $(".size_fields_wrap"); //Fields wrapper
    var price_wrapper         = $(".price_fields_wrap"); //Fields wrapper
    var x = 1; //initlal text box count
    $(".add_size_field_button").click(function(e){ //on add input button click
        e.preventDefault();
        if(x < size_max_fields){ //max input box allowed
            $(size_wrapper).append('<div class="col col-md-12">'+
                                      '<div class="col col-md-3">'+
                                        '<label for="text-input" class="form-control-label">Package Size'+x+'</label>'+
                                      '</div>'+
                                      '<div class="col-12 col-md-9">'+
                                        '<input type="text" name="package_size_'+x+'" class="form-control" required>'+
                                      '</div>'+
                                      '<br>'+
                                      '<br>'+
                                      '<div class="col col-md-3">'+
                                        '<label class="form-control-label">Price'+x+'</label>'+
                                      '</div>'+
                                      '<div class="col-12 col-md-9">'+
                                        '<input type="text" class="form-control" name="price_'+x+'" required>'+
                                        '</div>'+
                                        '<br>'+
                                        '<br>'+
                                        '<div class="col col-md-3">'+
                                          '<label class="form-control-label">Stock'+x+'</label>'+
                                        '</div>'+
                                        '<div class="col-12 col-md-9">'+
                                          '<input type="text" class="form-control" name="stock_'+x+'" required>'+
                                        '</div>'+
                                      '<a href="#" class="offset-3 remove_size_field"><i class="fa fa-times" aria-hidden="true" style="color:red"></i></a>'+
                                    '</div>');
            x++; //text box increment
        }
    });

    $(size_wrapper).on("click",".remove_size_field", function(e){ //user click on remove text
        e.preventDefault();
        $(this).parent('div').remove();
        x--;
    });
  });

</script>

<script type="text/javascript">
  $("#main_img").select2({
  tags: true,
  minimumInputLength: 2,
  minimumResultsForSearch: 10,
  ajax: {
    url: "{{ url('/admin/getimages') }}",
    dataType: 'json',
    delay: 250,
    data: function (params) {
      return {
        image_name: params.term,
      };
    },
    processResults: function (data) {
              return {
                  results: $.map(data, function (item) {
                      return {
                          text: item.image_name,
                          id: item.image_path
                      }
                  })
              };
          },
    cache: true
  },
  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
  minimumInputLength: 1,
  });

  $("#custom_img").select2({
  tags: true,
  minimumInputLength: 2,
  minimumResultsForSearch: 10,
  ajax: {
    url: "{{ url('/admin/getimages') }}",
    dataType: 'json',
    delay: 250,
    data: function (params) {
      return {
        image_name: params.term,
      };
    },
    processResults: function (data) {
              return {
                  results: $.map(data, function (item) {
                      return {
                          text: item.image_name,
                          id: item.image_path
                      }
                  })
              };
          },
    cache: true
  },
  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
  minimumInputLength: 1,
  });

  $("#image1").select2({
  tags: true,
  minimumInputLength: 2,
  minimumResultsForSearch: 10,
  ajax: {
    url: "{{ url('/admin/getimages') }}",
    dataType: 'json',
    delay: 250,
    data: function (params) {
      return {
        image_name: params.term,
      };
    },
    processResults: function (data) {
              return {
                  results: $.map(data, function (item) {
                      return {
                          text: item.image_name,
                          id: item.image_path
                      }
                  })
              };
          },
    cache: true
  },
  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
  minimumInputLength: 1,
  });

  $("#image2").select2({
  tags: true,
  minimumInputLength: 2,
  minimumResultsForSearch: 10,
  ajax: {
    url: "{{ url('/admin/getimages') }}",
    dataType: 'json',
    delay: 250,
    data: function (params) {
      return {
        image_name: params.term,
      };
    },
    processResults: function (data) {
              return {
                  results: $.map(data, function (item) {
                      return {
                          text: item.image_name,
                          id: item.image_path
                      }
                  })
              };
          },
    cache: true
  },
  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
  minimumInputLength: 1,
  });

  $("#image3").select2({
  tags: true,
  minimumInputLength: 2,
  minimumResultsForSearch: 10,
  ajax: {
    url: "{{ url('/admin/getimages') }}",
    dataType: 'json',
    delay: 250,
    data: function (params) {
      return {
        image_name: params.term,
      };
    },
    processResults: function (data) {
              return {
                  results: $.map(data, function (item) {
                      return {
                          text: item.image_name,
                          id: item.image_path
                      }
                  })
              };
          },
    cache: true
  },
  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
  minimumInputLength: 1,
  });

  $("#image4").select2({
  tags: true,
  minimumInputLength: 2,
  minimumResultsForSearch: 10,
  ajax: {
    url: "{{ url('/admin/getimages') }}",
    dataType: 'json',
    delay: 250,
    data: function (params) {
      return {
        image_name: params.term,
      };
    },
    processResults: function (data) {
      return {
        results: $.map(data, function (item) {
          return {
            text: item.image_name,
            id: item.image_path
          }
        })
      };
    },
    cache: true
  },
  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
  minimumInputLength: 1,
  });

</script>
@endsection

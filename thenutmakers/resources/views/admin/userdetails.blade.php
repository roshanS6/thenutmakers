@extends('admin.layout.masterlayout')
@section('content')
<!-- Right Panel -->
<div id="right-panel" class="right-panel">
   <!-- Header-->
   <header id="header" class="header">
      <div class="header-menu">
         <div class="col-sm-7">
            <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
         </div>
         <div class="col-sm-5">
            <div class="user-area dropdown float-right">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
               <img class="user-avatar rounded-circle" src="{{ URL::asset('images/admin.png') }}" alt="User Avatar">
               </a>
               <div class="user-menu dropdown-menu">
                  <!-- <a class="nav-link" href="#"><i class="fa fa- user"></i>My Profile</a>
                     <a class="nav-link" href="#"><i class="fa fa- user"></i>Notifications <span class="count">13</span></a>
                     <a class="nav-link" href="#"><i class="fa fa -cog"></i>Settings</a> -->
                  <a class="nav-link" href="{{ url('/admin/logout') }}"><i class="fa fa-power -off"></i>Logout</a>
               </div>
            </div>
            <div class="language-select dropdown" id="language-select">
               <a class="dropdown-toggle" href="#" data-toggle="dropdown"  id="language" aria-haspopup="true" aria-expanded="true">
               <i class="flag-icon flag-icon-us"></i>
               </a>
               <div class="dropdown-menu" aria-labelledby="language" >
                  <div class="dropdown-item">
                     <span class="flag-icon flag-icon-fr"></span>
                  </div>
                  <div class="dropdown-item">
                     <i class="flag-icon flag-icon-es"></i>
                  </div>
                  <div class="dropdown-item">
                     <i class="flag-icon flag-icon-us"></i>
                  </div>
                  <div class="dropdown-item">
                     <i class="flag-icon flag-icon-it"></i>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </header>
   <!-- /header -->
   <!-- Header-->
   <div class="breadcrumbs">
      <div class="col-sm-4">
         <div class="page-header float-left">
            <div class="page-title">
               <h1>User Details</h1>
            </div>
         </div>
      </div>
      <div class="col-sm-8">
         <div class="page-header float-right">
            <div class="page-title">
               <ol class="breadcrumb text-right">
                  <li><a href="#">User</a></li>
                  <li class="active">User Details</li>
               </ol>
            </div>
         </div>
      </div>
   </div>
   @if(session('message'))
   <div class="col-sm-12">
      <div class="alert  alert-success alert-dismissible fade show" role="alert">
        <span class="badge badge-pill badge-success">Success</span>{{session('message')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
   </div>
   @endif
   <div class="content mt-3">
      <div class="animated fadeIn">
         <div class="row">
            <div class="col-md-12">
               <div class="card">
                
                  <div class="card-body">
                     <table class="table table-striped table-bordered">
                        <thead>
                           <tr>
                              <th>User ID</th>
                              <th>First Name</th>
                              <th>Last Name</th>
                              <th>E-Mail</th>
                              <th>Phone</th>

                           </tr>
                        </thead>
                        <tbody>
                          @php
                          $sno = 1;
                          @endphp
                          @foreach ($users as $user)
                              <tr>
                                <td>{{ $user->user_id }}</td>
                                <td>{{ $user->first_name }}</td>
                                <td> {{ $user->last_name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->phone }}</td>
                               
                              </tr>
                          @endforeach
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
            <div class="col-md-12">
              
                <div class="page-title">
                <h4>User Address</h4>
              
              </div>
               <div style="margin-top:2%" class="card">
                
                  <div class="card-body">
                     <table class="table table-striped table-bordered">
                        <thead>
                           <tr>
                              <th>S.No</th>
                              <th>Address</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if(count($user_address)>0)
                          @php
                          $sno = 1;
                          @endphp
                          @foreach ($user_address as $address)
                              <tr>                                
                                <td>{{ $sno++ }}</td>
                                <td>{{ $address->address_line_1 }}, {{ $address->address_line_2 }}, {{ $address->city }}, {{ $address->state }}-{{ $address->zip }}. </td>
                              </tr>
                          @endforeach
                          @else
                          <td valign="top" colspan="8" style="text-align:center">No data available in table</td>
                          @endif
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
            <div class="col-md-12">
              
                <div class="page-title">
                <h4>User Orders</h4>
              
              </div>
               <div style="margin-top:2%" class="card">
                
                  <div class="card-body">
                     <table  id="bootstrap-data-table" class="table table-striped table-bordered">
                        <thead>
                           <tr>
                              <th>S.No</th>
                              <th>Order ID</th>
                              <th>Product_name</th>
                              <th>Quantity</th>
                              <th>Delivery Address</th>
                              <th>Total</th>
                              <th>Order Time</th>
                              <th>Order Status</th>
                            </tr>
                        </thead>
                        <tbody>
                          @php
                          $sno = 1;
                          @endphp
                          @foreach ($orderdetails as $orderdetail)
                              <tr>
                                
                                <td>{{ $sno++ }}</td>
                                <td><a href="{{ url('admin/orderdetails') }}/{{ $orderdetail->order_id }}">{{ $orderdetail->order_id }}</td>
                                 @if($orderdetail->NumberOfOrders > 1)
                                    <td>{{ $orderdetail->group_product }}</td>
                                    <td>{{ $orderdetail->group_quantity }}</td>
                                 @else
                                    <td>{{ $orderdetail->product_name }}
                                    <td>{{ $orderdetail->quantity }}</td>
                                 @endif
                                 
                                    @foreach($delivery_address as $deli_address)
                    
                                    @if ($deli_address->address_id == $orderdetail->delivery_address)
                                    <td>{{ $deli_address->address_line_1 }},<br>{{ $deli_address->address_line_2 }}</td>
                                    
                                    @break
                                    
                                    @endif
                            
                                    @endforeach
                                    
                                    {{-- @if(count($billing_address)>0)
                                      @foreach($billing_address as $bill_address)
                                        @if($orderdetail->billing_address != "")
                                          @if ($bill_address->address_id == $orderdetail->billing_address)
                                            <td>{{ $bill_address->address_line_1 }},<br>{{ $bill_address->address_line_2 }}</td>                                        
                                          @break
                                          @endif
                                        @else
                                        <td>-</td>
                                        @break
                                        @endif
                                      @endforeach
                                    @else
                                    <td></td>
                                    @endif              --}}
                                    <td>{{$orderdetail->grand_total}}</td>
                                <td>
                                @php
                                  $hijri= date('d-m-Y  h:i A', strtotime($orderdetail->order_time));
                                  @endphp
                                  {{$hijri}}
                                </td>
                                @if( $orderdetail->order_status == 0 )
                                  <td>Cancelled</td>
                                  @elseif( $orderdetail->order_status == 1 )
                                  <td>Placed</td>
                                  @elseif( $orderdetail->order_status == 2 )
                                  <td>Confirmed</td>
                                  @elseif( $orderdetail->order_status == 3 )
                                  <td>Dispatched</td>
                                  @elseif( $orderdetail->order_status == 4 )
                                  <td>Delivered</td>
                                  @elseif( $orderdetail->order_status == 5 )
                                  <td>Returned</td>
                                  @endif
                              </tr>
                          @endforeach
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- .animated -->
   </div>
</div>
<!-- Right Panel -->
@endsection

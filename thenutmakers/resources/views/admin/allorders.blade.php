@extends('admin.layout.masterlayout')
@section('content')
<!-- Right Panel -->
<div id="right-panel" class="right-panel">
   <!-- Header-->
   <header id="header" class="header">
      <div class="header-menu">
         <div class="col-sm-7">
            <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
         </div>
         <div class="col-sm-5">
            <div class="user-area dropdown float-right">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
               <img class="user-avatar rounded-circle" src="{{ URL::asset('images/admin.png') }}" alt="User Avatar">
               </a>
               <div class="user-menu dropdown-menu">
                  <!-- <a class="nav-link" href="#"><i class="fa fa- user"></i>My Profile</a>
                     <a class="nav-link" href="#"><i class="fa fa- user"></i>Notifications <span class="count">13</span></a>
                     <a class="nav-link" href="#"><i class="fa fa -cog"></i>Settings</a> -->
                  <a class="nav-link" href="{{ url('/admin/logout') }}"><i class="fa fa-power -off"></i>Logout</a>
               </div>
            </div>
            <div class="language-select dropdown" id="language-select">
               <a class="dropdown-toggle" href="#" data-toggle="dropdown"  id="language" aria-haspopup="true" aria-expanded="true">
               <i class="flag-icon flag-icon-us"></i>
               </a>
               <div class="dropdown-menu" aria-labelledby="language" >
                  <div class="dropdown-item">
                     <span class="flag-icon flag-icon-fr"></span>
                  </div>
                  <div class="dropdown-item">
                     <i class="flag-icon flag-icon-es"></i>
                  </div>
                  <div class="dropdown-item">
                     <i class="flag-icon flag-icon-us"></i>
                  </div>
                  <div class="dropdown-item">
                     <i class="flag-icon flag-icon-it"></i>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </header>
   <!-- /header -->
   <!-- Header-->
   <div class="breadcrumbs">
      <div class="col-sm-4">
         <div class="page-header float-left">
            <div class="page-title">
               <h1>{{ $sub_title }}</h1>
            </div>
         </div>
      </div>
      <div class="col-sm-8">
         <div class="page-header float-right">
            <div class="page-title">
               <ol class="breadcrumb text-right">
                  <li><a href="#">Orders</a></li>
                  <li class="active">{{ $sub_title }}</li>
               </ol>
            </div>
         </div>
      </div>
   </div>
   @if(session('message'))
   <div class="col-sm-12">
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <span class="badge badge-pill badge-success">Success</span>{{session('message')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
   </div>
   @endif
   <div class="col-sm-12">
      <div class="alert alert-warning alert-dismissible fade show" style="display: none" role="alert">
        <span class="badge badge-pill badge-warning">error:</span>Must, Enter the Declined reason
        <button type="button" class="close" data-dismiss="alert" aria-label="Close" aria-hidden="true>
        <span ">&times;</span>
        </button>
      </div>
   </div>
   <div class="content mt-3">
      <div class="animated fadeIn">
         <div class="row">
            <div class="col-md-12">
               <div class="card">
                  <div class="card-header">
                     <strong class="card-title">Order List</strong>
                  </div>
                  <div class="card-body">
                     <table id="bootstrap-data-table" class="table table-striped table-bordered">
                        <thead>
                           <tr>
                              <th>S.No</th>
                              <th>Order_Id</th>
                              <th>Product_name</th>
                              <th>Quantity</th>
                              <th>Order Time</th>
                              <th>Order Status</th>
                              <th>Action</th>
                             
                           </tr>
                        </thead>
                        <tbody>
                          @php
                          $sno=1;
                          @endphp
                            @foreach ($orders as $order)
                             <tr>
                                <td>{{ $sno++ }}  
                                <td><a href="{{ url('admin/orderdetails') }}/{{ $order->order_id }}">{{ $order->order_id }}</td>
                                 @if($order->NumberOfOrders > 1)
                                    <td>{{ $order->group_product }}</td>
                                    <td>{{ $order->group_quantity }}</td>
                                 @else
                                    <td>{{ $order->product_name }}
                                    <td>{{ $order->quantity }}</td>
                                 @endif
                                <td>
                                  @php
                                  $hijri= date('d-m-Y  h:i A', strtotime($order->order_time));
                                  @endphp
                                  {{$hijri}}
                                </td>
                                <td> 
                      @if ($order->order_status==0)
                        <div class="label cancel" >Cancelled</div>
                      @elseif ($order->order_status==1)
                        <div class="label placed">Placed</div>
                      @elseif ($order->order_status==2)
                        <div class="label confirm">Confirmed</div>
                      @elseif ($order->order_status==3)
                        <div class="label dispatched">Dispatched</div>
                      @elseif ($order->order_status==4)
                        <div class="label delivered">Delivered</div>
                      @elseif ($order->order_status==5)
                        <div class="label returned">Returned</div>
                      @endif
                    </td>
                    @if ($order->order_status!=5 && $order->order_status!=4 && $order->order_status!=0) 
                     <td>                   
                           <select id='statusSelect' onchange='fncatapprove(this.value,"<?php echo $order->order_id; ?>");' data-toggle='tooltip' class='btn' data-original-title='Pending' data-container='body'>

                              @if ($order->order_status=='1')
                                 <option value='1'>Placed</option> 
                                 <option value='2'>Confirm</option> 
                                 <option value='0'>Cancel</option>
                              @elseif ($order->order_status=='2')
                                 <option value='2'>Confirm</option> 
                                 <option value='3'>Dispatch</option> 
                                 <option value='0'>Cancel</option>
                              @elseif ($order->order_status=='3')
                                 <option value='3'>Dispatch</option> 
                                 <option value='4'>Delivery</option>  
                                 <option value='0'>Cancel</option>
                              @endif

                           </select>                      
                     </td>
                    @else
                     <td></td>
                    @endif
                  </tr>
               @endforeach
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- .animated -->
   </div>
</div>
<!-- Right Panel -->
<script type="text/javascript">
    fncatapprove = function(level, id) {
      
      var approveopt=confirm('Are you sure, do you want to change the status?');
      if (approveopt) {
         if (level ==0) {
            var reason = prompt('Enter the declined reason');
            if (!reason) {
               $('.alert').show()
               setTimeout(function(){ location.reload(); }, 2000);
               return false;
            } else {
               window.location = '<?php echo url('admin/allorders/cancel'); ?>/'+level+ "/" +id+ "/" +reason;
            }
         } else {
            window.location = '<?php echo url('admin/allorders/update'); ?>/'+level+ "/" +id;
         }
        return true;
      } else {
        location.reload();
        return false;
      }
   }

$(function(){
      $(".alert").bind('closed.bs.alert', function () {
        location.reload();
      });
   });

$('div.alert').delay(4000).slideUp(300);

</script>
@endsection

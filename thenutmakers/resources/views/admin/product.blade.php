@extends('admin.layout.masterlayout')
@section('content')
<!-- Right Panel -->
<style>
   h5 {
    text-align: center;
   }
</style>
<div id="right-panel" class="right-panel">
   <!-- Header-->
   <header id="header" class="header">
      <div class="header-menu">
         <div class="col-sm-7">
            <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
         </div>
         <div class="col-sm-5">
            <div class="user-area dropdown float-right">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
               <img class="user-avatar rounded-circle" src="{{ URL::asset('images/admin.png') }}" alt="User Avatar">
               </a>
               <div class="user-menu dropdown-menu">
                  <!-- <a class="nav-link" href="#"><i class="fa fa- user"></i>My Profile</a>
                     <a class="nav-link" href="#"><i class="fa fa- user"></i>Notifications <span class="count">13</span></a>
                     <a class="nav-link" href="#"><i class="fa fa -cog"></i>Settings</a> -->
                  <a class="nav-link" href="{{ url('/admin/logout') }}"><i class="fa fa-power -off"></i>Logout</a>
               </div>
            </div>
            <div class="language-select dropdown" id="language-select">
               <a class="dropdown-toggle" href="#" data-toggle="dropdown"  id="language" aria-haspopup="true" aria-expanded="true">
               <i class="flag-icon flag-icon-us"></i>
               </a>
               <div class="dropdown-menu" aria-labelledby="language" >
                  <div class="dropdown-item">
                     <span class="flag-icon flag-icon-fr"></span>
                  </div>
                  <div class="dropdown-item">
                     <i class="flag-icon flag-icon-es"></i>
                  </div>
                  <div class="dropdown-item">
                     <i class="flag-icon flag-icon-us"></i>
                  </div>
                  <div class="dropdown-item">
                     <i class="flag-icon flag-icon-it"></i>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </header>
   <!-- /header -->
   <!-- Header-->
   <div class="breadcrumbs">
      <div class="col-sm-4">
         <div class="page-header float-left">
            <div class="page-title">
               <h1>Products</h1>
            </div>
         </div>
      </div>
      <div class="col-sm-8">
         <div class="page-header float-right">
            <div class="page-title">
               <ol class="breadcrumb text-right">
                  <li><a href="#">Products</a></li>
                  <li class="active">Products Edit</li>
               </ol>
            </div>
         </div>
      </div>
   </div>
   @if(session('message'))
   <div class="col-sm-12">
      <div class="alert  alert-success alert-dismissible fade show" role="alert">
        <span class="badge badge-pill badge-success">Success</span>{{session('message')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
   </div>
   @endif
   <div class="col-lg-12">
      <div class="card">
         <div class="card-header">
            <strong>Products Edit</strong>
         </div>
         <div class="card-body card-block">


            <form action="{{ url('admin/products/update') }}" method="post" enctype="multipart/form-data">
               <div class="row form-group">
                  <div class="col col-md-3"><label for="select" class=" form-control-label">Product Category</label></div>
                     <div class="col-12 col-md-3">
                        <select name="category_name" class="form-control" required>
                           <option value="">Please select</option>
                           @foreach ($category as $cat)
                              @if ($products->category_name==$cat->category_name)
                                 <option value="{{$cat->category_name}}" selected>{{$cat->category_name}}</option>
                              @else
                                 <option value="{{$cat->category_name}}" >{{$cat->category_name}}</option>
                              @endif
                           @endforeach
                        </select>
                     </div>
                  </div>
                  <div class="row form-group">
                     <div class="col col-md-3"><label for="text-input" class=" form-control-label">Product Name</label></div>
                     <div class="col-12 col-md-9"><input type="text" name="product_name" class="form-control" value="{{ $products->product_name }}" required>
                     <input type="hidden" name="id" value="{{ $id }}"></div>
                  </div>
                  <div class="row form-group">
                     <div class="col col-md-3"><label for="text-input" class=" form-control-label">Short Description</label></div>
                     <div class="col-12 col-md-9"><input type="text" name="short_description" class="form-control" minlength="5" maxlength="300" value="{{ $products->short_description }}" required></div>
                  </div>
                  <div class="row form-group">
                     <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Long Description</label></div>
                     <div class="col-12 col-md-9"><textarea name="long_description" class="form-control" rows="3" maxlength="740" required>{{ $products->long_description }}</textarea></div>
                  </div>
                  <div class="row form-group">
                     <div class="col col-md-3"><label for="text-input" class=" form-control-label">Price</label></div>
                     <div class="col-12 col-md-9"><input type="text" name="price" class="form-control" value="{{ $products->price }}" required>
                  </div>
               </div>
               <div class="row form-group">
                  <div class="col col-md-3"><label for="text-input" class=" form-control-label">Stock</label></div>
                  <div class="col-12 col-md-9"><input type="text" name="stock" class="form-control" value="{{ $products->stock }}" required>
                  </div>
               </div>
               <div class="row form-group">
                  <div class="col col-md-3"><label for="text-input" class=" form-control-label">Priority</label></div>
                  <div class="col-12 col-md-9"><input type="text" name="priority" class="form-control" value="{{ $products->priority }}" required>
                  </div>
               </div>
                <div class="row form-group">
                  <div class="col col-md-3"><label for="text-input" class=" form-control-label">Package Size</label></div>
                  <div class="col-12 col-md-9"><input type="text" name="package_size" class="form-control" value="{{ $products->package_size }}" required>
                  </div>
               </div>
               <div class="row form-group">
                  <div class="col col-md-3"><label for="select" class=" form-control-label">Featured Product</label></div>
                  <div class="col-12 col-md-3">
                     <select name="featured_product" class="form-control" required>
                        <option value="{{ $products->featured_product }}" selected>
                        @if( $products->featured_product == 0)
                           None
                           {{-- @elseif( $products->featured_product == 1)
                           Banner --}}
                        @elseif($products->featured_product ==2)
                           Slider
                        @endif
                           </option>
                        @if( $products->featured_product == 0)
                        {{-- <option value="1">Banner</option> --}}
                        <option value="2">Slider</option>    
                        {{-- @elseif( $products->featured_product == 1)
                        <option value="0">None</option>
                        <option value="2">Slider</option>   --}}
                        @elseif($products->featured_product ==2)
                        {{-- <option value="1">Banner</option> --}}
                           <option value="0">None</option>  
                        @endif
                     </select>
                  </div>
               </div>
                {{-- <div class="row form-group">
                  <div class="col col-md-3"><label for="file-input" class=" form-control-label">Upload only if want to change the product's banner image.</label></div>
                  <div class="col-12 col-md-9">
                     @if ($products->banner_image != NULL)
                        <img src="{{ URL::asset($products->banner_image) }}" style="height: 100px;">
                     @else
                        <p> No Banner Image</p>
                     @endif
                  </div>
               </div>
               <div class="row form-group">
                  <div class="col col-md-3"></div><div class="col-12 col-md-9"><input type="file" accept="image/*" name="banner_image" value="{{ URL::asset($products->banner_image) }}" class="form-control-file"></div>
               </div> --}}
               <div class="row form-group">
                  <div class="col col-md-3"><label for="file-input" class=" form-control-label">Upload only if want to change the product image.</label></div>
                  <div class="col-12 col-md-9"><img src="{{ URL::asset($products->image_path) }}" style="height: 100px;"></div>
               </div>
               <div class="row form-group">
                  <div class="col col-md-3"></div><div class="col-12 col-md-9"><input type="file" accept="image/*" name="category_image" value="{{ URL::asset($products->image_path) }}" class="form-control-file"></div>
               </div>
               <div class="row form-group">
                  <div class="col col-md-3"><label for="file-input" class=" form-control-label">Other Related Images</label></div>
                  @if ($products->image_1 != NULL)
                    <div class="col-12 col-md-2"><img src="{{ URL::asset($products->image_1) }}" style="height: 100px;"></div><div class="col-12 col-md-1"><a href="{{ url('/admin/product/delete') }}/{{$products->id}}/image_1" style="color: #fff;" title="Delete"><i class="fa fa-remove" style="font-size:30px;color:red"></i></a></div>
                  @else
                    <p>No image</p>
                  @endif
               </div>
               <div class="row form-group">
                  <div class="col col-md-3"></div><div class="col-12 col-md-9"><input type="file" name="image_1" accept="image/*" value="{{ URL::asset($products->image_1) }}" class="form-control-file"></div>
               </div>

               <div class="row form-group">
                  <div class="col col-md-3"></div>
                  @if ($products->image_2 != NULL)
                    <div class="col-12 col-md-2"><img src="{{ URL::asset($products->image_2) }}" style="height: 100px;"></div><div class="col-12 col-md-1"><a href="{{ url('/admin/product/delete') }}/{{$products->id}}/image_2" style="color: #fff;" title="Delete"><i class="fa fa-remove" style="font-size:30px;color:red"></i></a></div>
                  @else
                    <p>No image</p>
                  @endif
               </div>
               <div class="row form-group">
                  <div class="col col-md-3"></div><div class="col-12 col-md-9"><input type="file" name="image_2" accept="image/*" value="{{ URL::asset($products->image_2) }}" class="form-control-file"></div>
               </div>

               <div class="row form-group">
                  <div class="col col-md-3"></div>
                  @if ($products->image_3 != NULL)
                  <div class="col-12 col-md-2"><img src="{{ URL::asset($products->image_3) }}" style="height: 100px;"></div><div class="col-12 col-md-1"><a href="{{ url('/admin/product/delete') }}/{{$products->id}}/image_3" style="color: #fff;" title="Delete"><i class="fa fa-remove" style="font-size:30px;color:red"></i></a></div>
                  @else
                    <p>No image</p>
                  @endif
               </div>
               <div class="row form-group">
                  <div class="col col-md-3"></div><div class="col-12 col-md-9"><input type="file" name="image_3" accept="image/*" value="{{ URL::asset($products->image_3) }}" class="form-control-file"></div>
               </div>
               <!-- <div class="card-footer"> -->
                  <button type="submit" class="btn btn-success btn-md">
                  <i class="fa fa-dot-circle-o"></i> Submit
                  </button>
                  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
               <!-- </div> -->

            </form>
            <a href="{{ url('admin/products/delete') }}/{{ $products->id }}" style="float:right"><button class="btn btn-danger btn-md">
              <i class="fa fa-window-close"></i> Delete
            </button></a>
            <br><br>
            @for($i = 0; $i < sizeof($product_variety); $i++ )
            <h5>Variety {{$i+1}}</h5>
            <br>
            <form action="{{ url('admin/products/update_variety') }}/{{ $product_variety[$i]->id }}" method="post">
               <div class="row form-group">
                  <div class="col col-md-3"><label for="text-input" class=" form-control-label">Price {{$i+1}}</label></div>
               <div class="col-12 col-md-9"><input type="text" name="price_1" class="form-control" value="{{ $product_variety[$i]->price }}" required>
                  </div>
               </div>
               <input type="hidden" name="product_id" value="{{ $id }}">
               <div class="row form-group">
                  <div class="col col-md-3"><label for="text-input" class=" form-control-label">Stock {{$i+1}}</label></div>
                  <div class="col-12 col-md-9"><input type="text" name="stock_1" class="form-control" value="{{ $product_variety[$i]->stock }}" required>
                  </div>
               </div>
               <div class="row form-group">
                  <div class="col col-md-3"><label for="text-input" class=" form-control-label">Package Size {{$i+1}}</label></div>
                  <div class="col-12 col-md-9"><input type="text" name="package_size_1" class="form-control" value="{{ $product_variety[$i]->package_size }}" required>
                  </div>
               </div>
               <button type="submit" class="btn offset-3 btn-success btn-md">
               <i class="fa fa-upload"> Update Variety {{$i+1}}</i></button>
               <a href="{{ url('admin/products/delete_variety') }}/{{ $product_variety[$i]->id }}" class="btn btn-danger btn-md" style="float:right">
                     <i class="fa fa-window-close"></i> Delete Variety {{$i+1}}</a>                     
                  <br><br>
                  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            </form>
         @endfor
         <div class="col col-md-12 row form-group size_fields_wrap">
            </div>
            <button type="button" class="btn btn-primary add_size_field_button" name="button">Add more package size</button>
         </div>
      </div>
   </div>
</div>
@php
$size = sizeof($product_variety);
$maxSize = 4;
$remainingSize = $maxSize - $size;
@endphp
<script>

   $(document).ready(function() {
      var size_max_fields      = '<?php echo $remainingSize; ?>'; //maximum input boxes allowed
    
    var size_wrapper         = $(".size_fields_wrap"); //Fields wrapper
    var price_wrapper         = $(".price_fields_wrap"); //Fields wrapper
    var x = '<?php echo $remainingSize; ?>'; //initlal text box count
    console.log(x,x <= size_max_fields)
    $(".add_size_field_button").click(function(e){ //on add input button click
        e.preventDefault();
        if(x <= size_max_fields && x > 0){ //max input box allowed
            $(size_wrapper).append('<div class="col col-md-12">'+
                                       '<form action={{ url("admin/products/insert_variety") }} method="post">'+
                                          '<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">'+
                                             '<div class="col col-md-3">'+
                                                '<label for="text-input" class="form-control-label">Package Size</label>'+
                                             '</div>'+
                                             '<div class="col-12 col-md-9">'+
                                                '<input type="text" name="package_size" class="form-control" required>'+                                        
                                                '<input type="hidden" name="product_id" value="{{ $id }}">'+
                                             '</div>'+
                                             '<br>'+
                                             '<br>'+
                                             '<div class="col col-md-3">'+
                                                '<label class="form-control-label">Price</label>'+
                                             '</div>'+
                                             '<div class="col-12 col-md-9">'+
                                                '<input type="text" class="form-control" name="price" required>'+
                                             '</div>'+
                                                '<br>'+
                                                '<br>'+
                                             '<div class="col col-md-3">'+
                                                '<label class="form-control-label">Stock</label>'+
                                             '</div>'+
                                             '<div class="col-12 col-md-9">'+
                                                '<input type="text" class="form-control" name="stock" required>'+
                                             '</div>'+
                                             '<br><br><button type="submit" class="offset-3 btn btn-success btn-sm"><i class="fa fa-dot-circle-o"></i>Add</button>'+
                                          '</form>'+
                                          '<a href="#" class="float-right remove_size_field"><i class="fa fa-times" aria-hidden="true" style="color:red"></i></a>'+
                                       '</div>');
            x--; //text box increment
        }
    });

    $(size_wrapper).on("click",".remove_size_field", function(e){ //user click on remove text
        e.preventDefault();
        $(this).parent('div').remove();
        x++;
    });
   })

</script>
<!-- Right Panel -->
@endsection

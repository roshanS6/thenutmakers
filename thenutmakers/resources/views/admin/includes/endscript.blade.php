<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(".button").click(function(){
  if($(this).next().css("display") == "none"){
    $(this).next().slideDown();
  }else{
    $(this).next().slideUp();
  }
});

function change(input){
        if (input.files && input.files[0]){
           var reader = new FileReader();
           reader.onload = function (e){
               $(input).next()
                   .attr('src', e.target.result)
                   .width(50)
                   .height(100);
           };
           reader.readAsDataURL(input.files[0]);
       }}

    $( function(){
    $('#datepicker').datepicker();
    $('#datepicker').datepicker('option',{dateFormat: 'yy-mm-d'});
    });

    $('#mainc').change(function()
    {   if($('#mainc').val() =='3')
       {
         $('#audiofile').prop("disabled",false)
         $('#bookbv').prop("disabled",true)
         $('#bookpreview1').prop("disabled",true)
         $('#bookpreview2').prop("disabled",true)
         $('#bookpreview3').prop("disabled",true)
         $('#bookpreview4').prop("disabled",true)
         $('#bookpreview5').prop("disabled",true)
         }
       else {
         $('#audiofile').prop("disabled",true)
         $('#bookbv').prop("disabled",false)
         $('#bookpreview1').prop("disabled",false)
         $('#bookpreview2').prop("disabled",false)
         $('#bookpreview3').prop("disabled",false)
         $('#bookpreview4').prop("disabled",false)
         $('#bookpreview5').prop("disabled",false)
       }
         $.ajax(
         {
               url: "{{url('admin/books')}}",
               method: "POST",
               data:{
                 id:$(this).val()
               },
               success:function (response) {
                 $("#subc").html(response);

               },
               error:function () {
                 location.reload();
               }
         });

 });



 $('#prod_type').change(function()
 {
      $.ajax(
      {
            url: "{{url('admin/featured')}}",
            method: "POST",
            data:{
              product_type: $("#prod_type").val()
            },
            success:function (response) {
              $("#prod_name").html(response);

            },
            error:function () {

            }
      });
      console.log($("#prod_type").find(":selected").text());

  });
         $('#accessoriesmainc').change(function()
         {
              $.ajax(
              {
                    url: "{{url('admin/accessories')}}",
                    method: "POST",
                    data:{
                      id:$(this).val()
                    },
                    success:function (response) {
                      $("#accessoriesubc").html(response);
                    },
                    error:function () {
                     location.reload();
                    }
              });
            });






</script>

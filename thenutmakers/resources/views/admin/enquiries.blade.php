@extends('admin.layout.masterlayout')
@section('content')
<!-- Right Panel -->
<div id="right-panel" class="right-panel">
   <!-- Header-->
   <header id="header" class="header">
      <div class="header-menu">
         <div class="col-sm-7">
            <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
         </div>
         <div class="col-sm-5">
            <div class="user-area dropdown float-right">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
               <img class="user-avatar rounded-circle" src="{{ URL::asset('images/admin.png') }}" alt="User Avatar">
               </a>
               <div class="user-menu dropdown-menu">
                  <!-- <a class="nav-link" href="#"><i class="fa fa- user"></i>My Profile</a>
                     <a class="nav-link" href="#"><i class="fa fa- user"></i>Notifications <span class="count">13</span></a>
                     <a class="nav-link" href="#"><i class="fa fa -cog"></i>Settings</a> -->
                  <a class="nav-link" href="{{ url('/admin/logout') }}"><i class="fa fa-power -off"></i>Logout</a>
               </div>
            </div>
            <div class="language-select dropdown" id="language-select">
               <a class="dropdown-toggle" href="#" data-toggle="dropdown"  id="language" aria-haspopup="true" aria-expanded="true">
               <i class="flag-icon flag-icon-us"></i>
               </a>
               <div class="dropdown-menu" aria-labelledby="language" >
                  <div class="dropdown-item">
                     <span class="flag-icon flag-icon-fr"></span>
                  </div>
                  <div class="dropdown-item">
                     <i class="flag-icon flag-icon-es"></i>
                  </div>
                  <div class="dropdown-item">
                     <i class="flag-icon flag-icon-us"></i>
                  </div>
                  <div class="dropdown-item">
                     <i class="flag-icon flag-icon-it"></i>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </header>
   <!-- /header -->
   <!-- Header-->
   <div class="breadcrumbs">
      <div class="col-sm-4">
         <div class="page-header float-left">
            <div class="page-title">
               <h1>Enquiries</h1>
            </div>
         </div>
      </div>
      {{-- <div class="col-sm-8">
         <div class="page-header float-right">
            <div class="page-title">
               <ol class="breadcrumb text-right">
                  <li><a href="#">Orders</a></li>
                  <li class="active">{{ $sub_title }}</li>
               </ol>
            </div>
         </div>
      </div> --}}
   </div>
   @if(session('message'))
   <div class="col-sm-12">
      <div class="alert  alert-success alert-dismissible fade show" role="alert">
        <span class="badge badge-pill badge-success">Success</span>{{session('message')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
   </div>
   @endif
   <div class="col-sm-12">
      <div class="warn-alert alert-warning alert-dismissible fade show" style="display: none" role="alert">
        <span class="badge badge-pill badge-warning">error:</span>Must, Enter the Declined reason
        <button type="button" class="close" data-dismiss="alert" aria-label="Close" aria-hidden="true>
        <span ">&times;</span>
        </button>
      </div>
   </div>
   <div class="content mt-3">
      <div class="animated fadeIn">
         <div class="row">
            <div class="col-md-12">
               <div class="card">
                  <div class="card-header">
                     <strong class="card-title">Order List</strong>
                  </div>
                  <div class="card-body">
                     <table id="bootstrap-data-table" class="table table-striped table-bordered">
                        <thead>
                           <tr>
                              <th>S.No</th>
                              <th>Name</th>
                              <th>E-mail</th>
                              <th>Subject</th>
                              <th>Message</th>
                              <th>Resolved</th>                             
                           </tr>
                        </thead>
                        <tbody>
                          @php
                          $sno=1;
                          @endphp
                            @foreach ($enquiries as $enquiry)
                             <tr>
                                <td>{{ $sno++ }}  
                                <td>{{ $enquiry->name }}</td>
                                <td>{{ $enquiry->email }}</td>
                                <td>{{ $enquiry->subject }}</td>
                                <td>{{ $enquiry->message }}</td>
                                <td>
                                 <select id='statusSelect' onchange="fncatapprove(this.value,'<?php echo $enquiry->id; ?>');" data-toggle='tooltip' class='btn' data-original-title='Pending' data-container='body'>
                                    @if ($enquiry->status == 0)
                                       <option value='0'>No</option> 
                                       <option value='1'>Yes</option>
                                    @elseif ($enquiry->status == 1)
                                       <option value='1'>Yes</option> 
                                       <option value='0'>No</option>
                                    @endif              
                                 </select>
                                </td>
                              </tr>
                          @endforeach
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- .animated -->
   </div>
</div>
<!-- Right Panel -->
<script>
   fncatapprove = function(status, id) {
      var approveopt=confirm('Are you sure, want to change this enquiry status?');
      if(approveopt) {
         window.location = '<?php echo url('admin/enquiry/update'); ?>/'+status+ "/" +id;
         return true;
      } else {
         location.reload();
         return false;
      }
   }

$(function(){
      $(".warn-alert").bind('closed.bs.alert', function () {
        location.reload();
      });
   });

$('div.alert').delay(4000).slideUp(300);

</script>
@endsection

@extends('admin.layout.masterlayout')
@section('content')
<!-- Right Panel -->
<div id="right-panel" class="right-panel">
   <!-- Header-->
   <header id="header" class="header">
      <div class="header-menu">
         <div class="col-sm-7">
            <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
         </div>
         <div class="col-sm-5">
            <div class="user-area dropdown float-right">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
               <img class="user-avatar rounded-circle" src="{{ URL::asset('images/admin.png') }}" alt="User Avatar">
               </a>
               <div class="user-menu dropdown-menu">
                  <!-- <a class="nav-link" href="#"><i class="fa fa- user"></i>My Profile</a>
                     <a class="nav-link" href="#"><i class="fa fa- user"></i>Notifications <span class="count">13</span></a>
                     <a class="nav-link" href="#"><i class="fa fa -cog"></i>Settings</a> -->
                  <a class="nav-link" href="{{ url('/admin/logout') }}"><i class="fa fa-power -off"></i>Logout</a>
               </div>
            </div>
            <div class="language-select dropdown" id="language-select">
               <a class="dropdown-toggle" href="#" data-toggle="dropdown"  id="language" aria-haspopup="true" aria-expanded="true">
               <i class="flag-icon flag-icon-us"></i>
               </a>
               <div class="dropdown-menu" aria-labelledby="language" >
                  <div class="dropdown-item">
                     <span class="flag-icon flag-icon-fr"></span>
                  </div>
                  <div class="dropdown-item">
                     <i class="flag-icon flag-icon-es"></i>
                  </div>
                  <div class="dropdown-item">
                     <i class="flag-icon flag-icon-us"></i>
                  </div>
                  <div class="dropdown-item">
                     <i class="flag-icon flag-icon-it"></i>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </header>
   <!-- /header -->
   <!-- Header-->
   <div class="breadcrumbs">
      <div class="col-sm-4">
         <div class="page-header float-left">
            <div class="page-title">
               <h1>Products</h1>
            </div>
         </div>
      </div>
      <div class="col-sm-8">
         <div class="page-header float-right">
            <div class="page-title">
               <ol class="breadcrumb text-right">
                  <li><a href="#">Products</a></li>
                  <li class="active">View Products</li>
               </ol>
            </div>
         </div>
      </div>
   </div>
   @if(session('message'))
   <div class="col-sm-12">
      <div class="alert  alert-success alert-dismissible fade show" role="alert">
        <span class="badge badge-pill badge-success">Success</span>{{session('message')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
   </div>
   @endif
   <div class="content mt-3">
      <div class="animated fadeIn">
         <div class="row">
            <div class="col-md-12">
               <div class="card">
                  <div class="card-header">
                     <strong class="card-title">Products List</strong>
                  </div>
                  <div class="card-body">
                     <table id="bootstrap-data-table" class="table table-striped table-bordered">
                        <thead>
                           <tr>
                              <th>S.No</th>
                              <th>Product Name</th>
                              <th>Price</th>
                              <th>Stock</th>
                              <th>Image</th>
                              <th>Featured</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                        @php
                          $sno = 1;
                          @endphp
                          @foreach ($products as $product)
                              <tr>
                                <td>{{ $sno++ }}</td>
                                <td>{{ $product->product_name }}</td>
                                <td>{{ $product->price }}</td>
                                <td>{{ $product->stock }}</td>
                                @if($product->image_path=="null")
                                  <td>No Image to this product</td>
                                @else
                                  <td><img src="{{ URL::asset($product->image_path) }}" height="100" width="100"></td>
                                @endif
                                <td>
                                <select id='statusSelect' onchange='fncatapprove(this.value,"<?php echo $product->id; ?>");' data-toggle='tooltip' class='btn' data-original-title='Pending' data-container='body'>
                                  <option value="{{ $product->featured_product }}" selected>
                                    @if( $product->featured_product == 0)
                                    None
                                    {{-- @elseif( $product->featured_product == 1)
                                    Banner --}}
                                    @elseif($product->featured_product ==2)
                                    Slider
                                    @endif
                                    </option>
                                    @if( $product->featured_product == 0)
                                    {{-- <option value="1">Banner</option> --}}
                                    <option value="2">Slider</option>
                                    {{-- @elseif( $product->featured_product == 1)
                                    <option value="2">Slider</option>
                                    <option value="0">None</option>   --}}
                                    @elseif($product->featured_product ==2)
                                    {{-- <option value="1">Banner</option> --}}
                                    <option value="0">None</option>  
                                    @endif                         
                                </select> 
                                </td>
                                <td>
                                  <a class="btn btn-warning btn-icon btn-circle icon-1x fa fa-pencil" data-toggle="tooltip" href="{{ url('admin/product') }}/{{ $product->id }}" data-original-title="Edit" title="Edit" data-container="body"></a>
                                  <a class="btn btn-danger btn-icon btn-circle icon-1x fa fa-window-close" data-toggle="tooltip" href="{{ url('admin/products/delete') }}/{{ $product->id }}" data-original-title="Delete" title="Delete" data-container="body" onclick="return confirm('Are you sure you want to delete this Product..?');"></a>

                                  <!-- <a href="javascript:void(0);" class="btn btn-danger btn-icon btn-circle icon-1x fa fa-times" data-toggle="tooltip"  data-original-title="Delete" data-container="body" ></a> -->
                                </td>

                              </tr>
                          @endforeach
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- .animated -->
   </div>
</div>
<!-- Right Panel -->
<script type="text/javascript">
    fncatapprove = function(level, id){

      var approveopt=confirm('Are you sure, do you want to change the status?');
      if(approveopt)  
      {
        window.location = '<?php echo url('/admin/product/updatefeatures'); ?>/'+level+ "/" +id;
        return true;
      }  
      else  
      {
        return false;
      }
}


</script>
@endsection

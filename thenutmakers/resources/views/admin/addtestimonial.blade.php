@extends('admin.layout.masterlayout')
@section('content')
<!-- Right Panel -->
<div id="right-panel" class="right-panel">
   <!-- Header-->
   <header id="header" class="header">
      <div class="header-menu">
         <div class="col-sm-7">
            <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
         </div>
         <div class="col-sm-5"> 
            <div class="user-area dropdown float-right">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
               <img class="user-avatar rounded-circle" src="{{ URL::asset('images/admin.png') }}" alt="User Avatar">
               </a>
               <div class="user-menu dropdown-menu">
                  <!-- <a class="nav-link" href="#"><i class="fa fa- user"></i>My Profile</a>
                     <a class="nav-link" href="#"><i class="fa fa- user"></i>Notifications <span class="count">13</span></a>
                     <a class="nav-link" href="#"><i class="fa fa -cog"></i>Settings</a> -->
                  <a class="nav-link" href="{{ url('/admin/logout') }}"><i class="fa fa-power -off"></i>Logout</a>
               </div>
            </div>
            <div class="language-select dropdown" id="language-select">
               <a class="dropdown-toggle" href="#" data-toggle="dropdown"  id="language" aria-haspopup="true" aria-expanded="true">
               <i class="flag-icon flag-icon-us"></i>
               </a>
               <div class="dropdown-menu" aria-labelledby="language" >
                  <div class="dropdown-item">
                     <span class="flag-icon flag-icon-fr"></span>
                  </div>
                  <div class="dropdown-item">
                     <i class="flag-icon flag-icon-es"></i>
                  </div>
                  <div class="dropdown-item">
                     <i class="flag-icon flag-icon-us"></i>
                  </div>
                  <div class="dropdown-item">
                     <i class="flag-icon flag-icon-it"></i>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </header>
   <!-- /header -->
   <!-- Header-->
   <div class="breadcrumbs">
      <div class="col-sm-4">
         <div class="page-header float-left">
            <div class="page-title">
               <h1>Add Testimonial</h1>
            </div>
         </div>
      </div>
      <div class="col-sm-8">
         <div class="page-header float-right">
              <div class="page-title">
                 <ol class="breadcrumb text-right">
                    <li><a href="#">Testimonial</a></li>
                    <li class="active">Add Testimonial</li>
                 </ol>
              </div>
         </div>
      </div>
   </div>
   @if(session('message'))
   <div class="col-sm-12">
      <div class="alert  alert-success alert-dismissible fade show" role="alert">
        <span class="badge badge-pill badge-success">Success</span>{{session('message')}}
        {{ Session::forget('message') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
   </div>
   @endif
   <div class="col-lg-12">
      <div class="card">
         <div class="card-header">
            <strong>Add Testimonial</strong>
         </div>
         <div class="card-body card-block">
            <form action="{{url('admin/addtestimonial')}}" method="post" enctype="multipart/form-data">
               <div class="row form-group">
                  <div class="col col-md-3"><label for="text-input" class=" form-control-label">Show in homepage?</label></div>
                  <div class="col-12 col-md-9">
                     <select name="status" class="form-control" required>
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                     </select>
                  </div>
               </div>

               <div class="row form-group">
                  <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Content</label></div>
                  <div class="col-12 col-md-9"><textarea name="content" rows="3" class="form-control" required></textarea></div>
               </div>
               <div class="row form-group">
                  <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Reviewer Name</label></div>
                  <div class="col-12 col-md-9"><input name="name" rows="3" class="form-control" required></div>
               </div>
               {{-- <div class="row form-group">
                  <div class="col col-md-3"><label for="number-input" class=" form-control-label">Rating</label></div>
                  <div class="col-12 col-md-9"><input type="number" name="rating" min="0" max="5" class="form-control-file" required></div>
               </div> --}}
               <div class="card-footer">
                  <button type="submit" class="btn btn-success btn-sm">
                  <i class="fa fa-dot-circle-o"></i> Submit
                  </button>
                  <button type="reset" class="btn btn-danger btn-sm" onclick='window.location.reload();'>
                  <i class="fa fa-ban"></i> Reset
                  </button>
                   <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<!-- Right Panel -->
@endsection

<!doctype html>
<html class="no-js" lang="">
   @include('admin.layout.header')
   <style>
    a.title{color:#9496a1}
    a.title:hover{ color: #fff;}
    
   </style>

   <style media="screen">
    .active{
      display: block;
    }
    .form-control[disabled], fieldset[disabled] .form-control {
    cursor: default;
    }
    </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
   <body>
      <!-- Left Panel -->
      <aside id="left-panel" class="left-panel">
         <nav class="navbar navbar-expand-sm navbar-default">
            <div class="navbar-header">
               <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
               <i class="fa fa-bars"></i>
               </button>
               <a class="navbar-brand" href="{{ url('admin/dashboard') }}"><img src="{{ url::asset('images/logo.png') }}" alt="Logo" style="padding-top:1%; padding-bottom:2%;"></a>
               <a class="navbar-brand hidden" href="{{ url('admin/dashboard') }}"><img src="{{ url::asset('images/logo.png') }}" alt="Logo"></a>
            </div>
            <div id="main-menu" class="main-menu collapse navbar-collapse">
               <ul class="nav navbar-nav">
                  <li class="tab">
                     <a href="{{ url('admin/dashboard') }}"> <i class="menu-icon fa fa-dashboard" title="Dashboard"></i>Dashboard </a>
                  </li>
              
                  <h3 class="menu-title">Main Categories</h3>
                  <li class="tab">
                     <a href="{{url('/admin/maincategory/add')}}"> <i class="menu-icon fa fa-plus-circle" title="Add Main Category"></i>Add Main Category </a>
                  </li>
                  <li class="tab">
                     <a href="{{url('admin/maincategory')}}"> <i class="menu-icon fa fa-tasks" title="Main Categories"></i>Main Categories </a>
                  </li>

                  <h3 class="menu-title">Products</h3>
                  <li class="tab">
                     <a href="{{ url('admin/products/add') }}"> <i class="menu-icon fa fa-plus-square"></i>Add Products </a>
                  </li>
                  <li class="tab">
                     <a href="{{ url('admin/products') }}"> <i class="menu-icon fa fa-sort-amount-asc"></i>View Products </a>
                  </li>

                  <h3 class="menu-title">Users</h3>
                  <li class="tab">
                     <a href="{{ url('admin/user') }}"> <i class="menu-icon fa fa-user"></i>View Users </a>
                  </li>
                  <li class="tab">
                     <a href="{{ url('admin/subscribers') }}"> <i class="menu-icon fa fa-bell"></i>Subscribers </a>
                  </li>
                  <h3 class="menu-title">Orders</h3>
                  <li class="tab">
                     <a href="{{ url('admin/allorders') }}"> <i class="menu-icon fa fa-first-order"></i>All Orders </a>
                  </li>
                  <li class="tab">
                     <a href="{{ url('admin/placedorders') }}"> <i class="menu-icon fa fa-plus"></i>Placed Orders </a>
                  </li>
                  <li class="tab">
                     <a href="{{ url('admin/confirmedorders') }}"> <i class="menu-icon fa fa-check"></i>Confirmed Orders </a>
                  </li>
                  <li class="tab">
                     <a href="{{ url('admin/dispatchedorders') }}"> <i class="menu-icon fa fa-outdent"></i>Dispatched Orders </a>
                  </li>
                  <li class="tab">
                     <a href="{{ url('admin/deliveredorders') }}"> <i class="menu-icon fa fa-address-card-o"></i>Delivered Orders </a>
                  </li>
                  <li class="tab">
                     <a href="{{ url('admin/cancelledorders') }}"> <i class="menu-icon fa fa-ban"></i>Cancelled Orders </a>
                  </li>
                  {{-- <li class="tab">
                     <a href="{{ url('admin/returnedorders') }}"> <i class="menu-icon fa fa-retweet"></i>Returned Orders </a>
                  </li> --}}
                  {{-- <li class="tab">
                     <a href="{{ url('admin/failedorders') }}"> <i class="menu-icon fa fa-exclamation-circle"></i>Failed Transactions </a>
                  </li> --}}
                  <h3 class="menu-title">Banner</h3>
                  <li class="tab">
                        <a class="title" href="{{ url('admin/add_banner') }}"><i class="menu-icon fa fa-plus"></i> Add Banner</a>
                  </li>
                  <li class="tab">
                        <a class="title" href="{{ url('admin/banners') }}"><i class="menu-icon fa fa-image"></i> Banners</a>
                  </li>
                  <h3 class="menu-title"><a class="title" href="{{ url('admin/enquiries') }}"><i class="fa fa-envelope"></i> Enquiries</a></h3>
                  <h3 class="menu-title"><a class="title" href="{{ url('admin/aboutus') }}"><i class="fa fa-envelope-o"></i> About us</a></h3>
                  <h3 class="menu-title"><a class="title" href="{{ url('admin/constants') }}"><i class="fa fa-contao"></i> constants</a></h3>
                  <h3 class="menu-title">Testimonials</h3>
                  <li class="tab">
                     <a href="{{ url('admin/addtestimonial') }}"> <i class="menu-icon fa fa-plus-square-o"></i>Add </a>
                  </li>
                  <li class="tab">
                     <a href="{{ url('admin/testimonial') }}"> <i class="menu-icon fa fa-commenting-o"></i>View </a>
                  </li>

               </ul>
            </div>
         </nav>
      </aside>
      
      <!-- Left Panel -->

      <!-- Right Panel -->
        @yield('content')
      <!-- Right Panel -->

      @include('admin.layout.script')

</body>
</html>

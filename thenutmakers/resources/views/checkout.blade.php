<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="{{ URL::asset('css/view_bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/view_style.css') }}">
  <link href="https://fonts.googleapis.com/css?family=Bai+Jamjuree|Source+Serif+Pro" rel="stylesheet">
  <title>The Nut Makers</title>
  <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
  <style>
  input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    margin: 0; 
}
input::placeholder {
  color: white;
}
.state select > option, .country select > option {
  background: #495057;
}
</style>
</head>

<body>
  <header class="header-section">
    <!-- Fixed navbar -->
    <nav class="navbar navbar-expand-md navbar-dark fixed-top header" id="myHeader">
      <a class="navbar-brand" href="{{ url('/') }}">
        <img src="{{ URL::asset('images/logo.png') }}" width="100" height="100">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      <div class="collapse navbar-collapse justify-content-end" id="navbarCollapse">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/') }}">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/product') }}">Shop</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/aboutus') }}">About Us</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/contactus') }}">Contact</a>
          </li>
          <li class="nav-item svg-icon">
            <a class="nav-link" href="#">
              <img src="{{ URL::asset('images/search.svg') }}">
            </a>
          </li>
          <li class="nav-item svg-icon">
            <?php
            if(!Session::has('user_id')){
             ?>
              <a class="nav-link" href="{{ url('/profile') }}">
              <img src="{{ URL::asset('images/user.svg') }}">
            </a>
            <?php
            }
            else {
              ?>
            <div class="user-area dropdown">
            <a href="#" class="nav-link" data-toggle="dropdown" aria-expanded="false">
                     <img src="{{ URL::asset('images/user.svg') }}" >
                     </a> 
                     <div class="user-menu dropdown-menu">
                     <a class="nav-link" href="{{ url('/profile') }}" style="color:black">My profile</a>                      
                        <a class="nav-link" href="{{ url('/logout') }}" style="color:black">Logout</a>
                     </div>
            </div>
            <?php
            }
            ?>
          </li>
          <li class="nav-item svg-icon active">
            <a class="nav-link" href="{{ url('/cart') }}">
              <img src="{{ URL::asset('images/shopper.svg') }}">
            </a>
            <?php
            if(Session::has('cart_count')) {
             ?>
            <span class="dot"> </span>
            <?php 
              }
            ?>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  @php
  $total_price=0;
  $net_amt=0;
  $igst = $constants[0]->igst_percent/100;
  $delivery_charge= $constants[0]->shipping_cost;
  $igst_product = 0;
  @endphp
  <main role="main" class="wrapper">
    <div class="message" style="padding-top:1%">
        @if (Session('message'))
          <div class="alert alert-success alert-dismissible fade show" role="alert" style="font-weight: 400;">
              <span class="badge badge-pill" style="color:black">{{ Session('message') }}</span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            {{ Session::forget('message') }}
          </div>
        @endif
        @if (Session('error'))
          <div class="alert alert-warning alert-dismissible fade show" role="alert" style="font-weight: 400;">
              <span class="badge badge-pill" style="color:black">{{ Session('error') }}</span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            {{ Session::forget('error') }}
          </div>
        @endif
      </div>
    <div class="container-fluid">

      <div class="row align-items-center wrapper-section">

        <div class="col-md-1">

        </div>
        <div class="col-md-10 col-sm-10 checkout-form">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Shopping Cart</a></li>
            <li class="breadcrumb-item active" aria-current="page">Checkout Details</li>
          </ol>
          <div class="row">
            <div class="col-md-7 align-self-start" style="padding-left: 15px">
              <div class="row">
                <div class="col" style="padding-left: 15px">
                
                  <div class="checkout-details">
                    Delivery Address
                    <a href="#" data-toggle="modal" data-target="#select">Select</a>
                    <a href="#" data-toggle="modal" data-target="#newaddress">New Address</a>
                  </div>
                  @php 
                    $billing_address = null;
                    $delivery_state = null;
                  @endphp
                @if(count($addresses)>0)
                  @foreach ($addresses as $address)
                    @if($address->active_address==1)
                      @php 
                        $billing_address = $address;
                        $delivery_address = $address;
                        $delivery_state = $address->state;
                      @endphp
                      <div class="checkout-details-section">
                        <div>{{ $address->address_line_1 }}</div>
                        <div>{{ $address->address_line_2 }}</div>
                        <div>{{ $address->city }} - {{$address->zip}}</div>
                        <div>{{$address->state}}</div>
                      </div>
                    @endif
                  @endforeach
                @else
                  <p>You have no address. Please, kindly add new address</p>
                @endif
                </div>
              </div>
              <h2>Billing Details</h2>
              <form id="checkout" action="{{ url('/paywithrazorpay') }}" method="post">
              {{ csrf_field() }}
                <div class="form-row">
                  <div class="form-group">
                    <input class="checkbox" type="checkbox" name="billing_checkbox" checked>
                    <label for="checkbox">Billing Address same as delivery address?</label>
                  </div>
                </div>
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="FirstName">First Name*</label>
                    <input type="text" class="form-control" name="billing_first_name" id="FirstName" value="{{ $user_details[0]->first_name }}" required>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="LastName">Last Name*</label>
                    <input type="text" class="form-control"  name="billing_last_name" id="LastName" value="{{ $user_details[0]->last_name }}" required>
                  </div>
                </div>
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="FirstName">Phone*</label>
                    <input type="text" class="form-control" pattern="\d*" maxlength="10" name="billing_phone" id="Phone" value="{{ $user_details[0]->phone }}" required>
                  </div>
                  <div class="form-group col-md-6" id="email">
                    <label for="Email">Email*</label>
                    <input type="text" class="form-control" name="billing_email" id="email" value="{{ $user_details[0]->email }}" disabled required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputAddress">Address*</label>
                  @if($billing_address!=null)
                  <input type="text" class="form-control" name="billing_address_line_1" id="address_line_1" value="{{ $billing_address->address_line_1 }}" placeholder="House/Apartment Number" style="margin-bottom:20px;" required>
                  <input type="text" class="form-control" name="billing_address_line_2" id="address_line_2" value="{{ $billing_address->address_line_2 }}" placeholder="Street Address" required>
                  @else
                  <input type="text" class="form-control" name="billing_address_line_1" id="address_line_1" placeholder="House/Apartment Number" style="margin-bottom:20px;" required>
                  <input type="text" class="form-control" name="billing_address_line_2" id="address_line_2" placeholder="Street Address" required>
                  @endif
                </div>

                <div class="form-group country">
                  <label for="inputState">Country*</label>
                  @if($billing_address!=null)
                  <select id="inputState" name="billing_country" class="form-control" required>
                    <option value="" selected>Choose...</option>
                    <option <?php if ($billing_address->country == "India" ) echo 'selected' ; ?> value="India">India</option>
                  </select>
                  @else
                    <select id="inputState" name="billing_country" class="form-control" required>
                      <option value="" selected>Choose...</option>
                      <option value="India">India</option>
                    </select>
                  @endif
                </div>

                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="inputCity">City*</label>
                    @if($billing_address!=null)
                    <input type="text" class="form-control" name="billing_city" id="inputCity" value="{{ $billing_address->city }}" required>
                    @else
                    <input type="text" class="form-control" name="billing_city" id="inputCity" required>
                    @endif
                  </div>
                  <div class="form-group state col-md-4">
                    <label for="inputState">State*</label>
                    @if($billing_address!=null)
                      <select id="inputState" name="billing_state" class="form-control" required>
                        <option value="" selected>Choose...</option>
                        <option <?php if ($billing_address->state == "Andaman and Nicobar Islands" ) echo 'selected' ; ?> value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                        <option <?php if ($billing_address->state == "Andhra Pradesh" ) echo 'selected' ; ?> value="Andhra Pradesh">Andhra Pradesh</option>
                        <option <?php if ($billing_address->state == "Arunachal Pradesh" ) echo 'selected' ; ?> value="Arunachal Pradesh">Arunachal Pradesh</option>
                        <option <?php if ($billing_address->state == "Assam" ) echo 'selected' ; ?> value="Assam">Assam</option>
                        <option <?php if ($billing_address->state == "Bihar" ) echo 'selected' ; ?> value="Bihar">Bihar</option>
                        <option <?php if ($billing_address->state == "Chandigarh" ) echo 'selected' ; ?> value="Chandigarh">Chandigarh</option>
                        <option <?php if ($billing_address->state == "Chhattisgarh" ) echo 'selected' ; ?> value="Chhattisgarh">Chhattisgarh</option>
                        <option <?php if ($billing_address->state == "Dadra and Nagar Haveli" ) echo 'selected' ; ?> value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                        <option <?php if ($billing_address->state == "Daman and Diu" ) echo 'selected' ; ?> value="Daman and Diu">Daman and Diu</option>
                        <option <?php if ($billing_address->state == "Delhi" ) echo 'selected' ; ?> value="Delhi">Delhi</option>
                        <option <?php if ($billing_address->state == "Goa" ) echo 'selected' ; ?> value="Goa">Goa</option>
                        <option <?php if ($billing_address->state == "Gujarat" ) echo 'selected' ; ?> value="Gujarat">Gujarat</option>
                        <option <?php if ($billing_address->state == "Haryana" ) echo 'selected' ; ?> value="Haryana">Haryana</option>
                        <option <?php if ($billing_address->state == "Himachal Pradesh" ) echo 'selected' ; ?> value="Himachal Pradesh">Himachal Pradesh</option>
                        <option <?php if ($billing_address->state == "Jammu and Kashmir" ) echo 'selected' ; ?> value="Jammu and Kashmir">Jammu and Kashmir</option>
                        <option <?php if ($billing_address->state == "Jharkhand" ) echo 'selected' ; ?> value="Jharkhand">Jharkhand</option>
                        <option <?php if ($billing_address->state == "Karnataka" ) echo 'selected' ; ?> value="Karnataka">Karnataka</option>
                        <option <?php if ($billing_address->state == "Kerala" ) echo 'selected' ; ?> value="Kerala">Kerala</option>
                        <option <?php if ($billing_address->state == "Lakshadweep" ) echo 'selected' ; ?> value="Lakshadweep">Lakshadweep</option>
                        <option <?php if ($billing_address->state == "Madhya Pradesh" ) echo 'selected' ; ?> value="Madhya Pradesh">Madhya Pradesh</option>
                        <option <?php if ($billing_address->state == "Maharashtra" ) echo 'selected' ; ?> value="Maharashtra">Maharashtra</option>
                        <option <?php if ($billing_address->state == "Manipur" ) echo 'selected' ; ?> value="Manipur">Manipur</option>
                        <option <?php if ($billing_address->state == "Meghalaya" ) echo 'selected' ; ?> value="Meghalaya">Meghalaya</option>
                        <option <?php if ($billing_address->state == "Mizoram" ) echo 'selected' ; ?> value="Mizoram">Mizoram</option>
                        <option <?php if ($billing_address->state == "Nagaland" ) echo 'selected' ; ?> value="Nagaland">Nagaland</option>
                        <option <?php if ($billing_address->state == "Orissa" ) echo 'selected' ; ?> value="Orissa">Orissa</option>
                        <option <?php if ($billing_address->state == "Pondicherry" ) echo 'selected' ; ?> value="Pondicherry">Pondicherry</option>
                        <option <?php if ($billing_address->state == "Punjab" ) echo 'selected' ; ?> value="Punjab">Punjab</option>
                        <option <?php if ($billing_address->state == "Rajasthan" ) echo 'selected' ; ?> value="Rajasthan">Rajasthan</option>
                        <option <?php if ($billing_address->state == "Sikkim" ) echo 'selected' ; ?> value="Sikkim">Sikkim</option>
                        <option <?php if ($billing_address->state == "Tamil Nadu" ) echo 'selected' ; ?> value="Tamil Nadu">Tamil Nadu</option>
                        <option <?php if ($billing_address->state == "Tripura" ) echo 'selected' ; ?> value="Tripura">Tripura</option>
                        <option <?php if ($billing_address->state == "Uttaranchal" ) echo 'selected' ; ?> value="Uttaranchal">Uttaranchal</option>
                        <option <?php if ($billing_address->state == "Uttar Pradesh" ) echo 'selected' ; ?> value="Uttar Pradesh">Uttar Pradesh</option>
                        <option <?php if ($billing_address->state == "West Bengal" ) echo 'selected' ; ?> value="West Bengal">West Bengal</option>
                      </select>
                    @else
                      <select id="inputState" class="form-control" name="billing_state" required>
                        <option value="" selected>Choose...</option>
                        <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                        <option value="Andhra Pradesh">Andhra Pradesh</option>
                        <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                        <option value="Assam">Assam</option>
                        <option value="Bihar">Bihar</option>
                        <option value="Chandigarh">Chandigarh</option>
                        <option value="Chhattisgarh">Chhattisgarh</option>
                        <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                        <option value="Daman and Diu">Daman and Diu</option>
                        <option value="Delhi">Delhi</option>
                        <option value="Goa">Goa</option>
                        <option value="Gujarat">Gujarat</option>
                        <option value="Haryana">Haryana</option>
                        <option value="Himachal Pradesh">Himachal Pradesh</option>
                        <option value="Jammu and Kashmir">Jammu and Kashmir</option>
                        <option value="Jharkhand">Jharkhand</option>
                        <option value="Karnataka">Karnataka</option>
                        <option value="Kerala">Kerala</option>
                        <option value="Lakshadweep">Lakshadweep</option>
                        <option value="Madhya Pradesh">Madhya Pradesh</option>
                        <option value="Maharashtra">Maharashtra</option>
                        <option value="Manipur">Manipur</option>
                        <option value="Meghalaya">Meghalaya</option>
                        <option value="Mizoram">Mizoram</option>
                        <option value="Nagaland">Nagaland</option>
                        <option value="Orissa">Orissa</option>
                        <option value="Pondicherry">Pondicherry</option>
                        <option value="Punjab">Punjab</option>
                        <option value="Rajasthan">Rajasthan</option>
                        <option value="Sikkim">Sikkim</option>                      
                        <option value="Tamil Nadu">Tamil Nadu</option>
                        <option value="Telangana">Telangana</option>
                        <option value="Tripura">Tripura</option>
                        <option value="Uttar Pradesh">Uttar Pradesh</option>
                        <option value="Uttarakhand ">Uttarakhand</option>
                        <option value="West Bengal">West Bengal</option>
                      </select>
                    @endif
                  </div>
                  <div class="form-group col-md-2">
                    <label for="inputZip">Zip*</label>
                    @if($billing_address!=null)
                      <input type="text" class="form-control"  pattern="\d*" maxlength="6" name="billing_zip" id="inputZip" value="{{ $billing_address->zip }}" required>
                    @else
                      <input type="text" class="form-control"  pattern="\d*" maxlength="6" name="billing_zip" id="inputZip" required>
                    @endif
                  </div>
                </div>
                {{-- <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="shipping">Shipping</label>
                    <input type="text" class="form-control" name="shipping" id="shipping" placeholder="Same Day">
                  </div>
                </div> --}}
                {{-- <div class="form-group">
                  <div class="form-group">
                    <label for="shipping">Instructions</label>
                    <input type="text" class="form-control" name="instruction" id="instruction" placeholder="Instructions">
                  </div>
                </div> --}}
                {{-- <button type="submit" class="btn primary-btn-sm">SAVE</button>
                <button type="reset" class="btn sec-btn-sm">Cancel</button>
              </form> --}}

              {{-- <h2>Payment Details</h2>
              <form>
                <div class="form-row align-items-end">
                  <div class="form-group col-md-6">
                    <label for="number">Card Number</label>
                    <input type="number" class="form-control" id="number">
                  </div>
                  <div class="form-group col-md-6">
                    <div class="card-section">
                      <ul class="list-unstyled">
                        <li><img src="{{ URL::asset('images/visa.svg') }}" width="30" height="30"></li>
                        <li><img src="{{ URL::asset('images/mastercard.svg') }}" width="30" height="30"></li>
                        <li><img src="{{ URL::asset('images/american-express.svg') }}" width="30" height="30"></li>
                      </ul>
                    </div>
                  </div>
                </div>

                <div class="form-row">
                  <div class="form-group col-md-6 align-self-end">
                    <label for="inputCity">Expiration Date</label>
                    <input type="text" class="form-control" id="inputCity" placeholder="Month">
                  </div>
                  <div class="form-group col-md-4 align-self-end">
                    <input type="text" class="form-control" id="inputCity" placeholder="Year">
                  </div>
                  <div class="form-group col-md-2 align-self-end">
                    <label for="CVC">CVC</label>
                    <input type="text" class="form-control" id="CVC">
                  </div>
                </div>

                <div class="form-group">
                  <div class="form-check">

                    <label class="container-checkbox">Save credit card inform ation for the next time
                      <input type="checkbox">
                      <span class="checkmark"></span>
                    </label>
                  </div>
                </div>
              </form> --}}

            </div>
            <div class="col-md-5 align-self-start">
              <div class="form-section margin-auto">
                <div class="row">
                  <div class="col-md-12 no-padding">
                    <h2>Order Summary</h2>
                  </div>
                </div>
                {{-- {{ csrf_field() }} --}}
                <div class="row td-header">
                  @foreach($cart_products as $cart_product)
                    @if($cart_product->cart_quantity <= $cart_product->stock)
                    <div class="col-md-12 no-padding">
                      <h2>{{ $cart_product->product_name }}</h2></div>
                    <div class="col no-padding" style="padding-bottom:2%">{{ $cart_product->package_size }}</div>
                    <div class="col svg-icon no-padding align-end">
                      @php
                      $total_price = $cart_product->cart_quantity * $cart_product->price;
                      @endphp
                      <img src="{{ url::asset('images/rupee.svg') }}">{{ $total_price }} </div>
                      @php
                        $net_amt += $total_price;
                      @endphp
                      @php
                        if($cart_product->product_id == 31) {
                          // $igst_product = $igst_product + round($total_price * (5/100),2);
                        } else {
                          // $igst_product = $igst_product + round($total_price * $igst,2);
                        }
                    @endphp
                    @endif
                  @endforeach
                </div>
                <div class="td-body">
                  <div class="row">
                    <div class="col no-padding">Subtotal</div>
                    <div class="col svg-icon no-padding align-end">
                      <img src="{{ url::asset('images/rupee.svg')}}">{{ $net_amt }}</div>
                  </div>
                  @if($delivery_state == $constants[0]->state)
                    <div class="row">
                      <div class="col no-padding">*Inclusive of CGST</div>
                      {{-- <div class="col svg-icon no-padding align-end">
                        <img src="{{ url::asset('images/rupee.svg') }}">{{ $igst_product / 2 }}
                      </div> --}}
                    </div>
                    <div class="row">
                      <div class="col no-padding">*Inclusive of SGST</div>
                      {{-- <div class="col svg-icon no-padding align-end">
                        <img src="{{ url::asset('images/rupee.svg') }}">{{ $igst_product / 2 }}
                      </div> --}}
                    </div>
                  @else
                    <div class="row">
                      <div class="col no-padding">*Inclusive of IGST</div>
                      {{-- <div class="col svg-icon no-padding align-end">
                        <img src="{{ url::asset('images/rupee.svg') }}">{{ $igst_product }}
                      </div> --}}
                    </div>
                  @endif
                  @php
                    if($net_amt > 450) {
                      $delivery_charge = 0;
                    }
                  @endphp
                  <div class="row">
                    <div class="col no-padding">Delivery Charge</div>
                    <div class="col svg-icon no-padding align-end">
                      <img src="{{ url::asset('images/rupee.svg') }}">{{ $delivery_charge }}</div>
                      <input type="hidden" name="delivery_cost" value={{$delivery_charge}}>
                  </div>
                  {{-- <div class="row">
                    <div class="col no-padding">IGST Delivery Charge</div>
                    <div class="col svg-icon no-padding align-end">
                    @php
                    $igst_delivery_charge = round($delivery_charge * $igst,2);
                    @endphp
                      <img src="{{ url::asset('images/rupee.svg') }}">{{ $igst_delivery_charge }}</div>
                      <input type="hidden" name="igst_delivery" value="{{$igst_delivery_charge}}">
                  </div> --}}
                  <div class="row">
                    <div class="col no-padding">Total</div>
                    <div class="col svg-icon no-padding align-end">
                    @php
                    // $total_amt = round($net_amt + $igst_product + $delivery_charge);
                    $total_amt = round($net_amt + $delivery_charge);
                    @endphp
                      <img src="{{ url::asset('images/rupee.svg') }}">{{ $total_amt }}</div>
                      <input type="hidden" name="page_total_amt" value="{{$total_amt}}">
                    </div>
                </div>
                <div class="row">
                  <button type="submit" class="btn primary-button">
                    <a>Checkout</a></button>
                </div>
              </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
  </main>
  
  <div class="modal fade modal-style" id="select" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Delivery Address</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <form id="selectaddress" action="{{ url('/changeaddress') }}" method="post">
          {{ csrf_field() }}
          <div class="modal-body form-section">
            @if(count($addresses)>=1)
            @foreach($addresses as $address)
            <input type="radio" value="{{ $address->address_id }}" id="del_address" name="del_address" required>
            <div class="checkout-details-section edit-left">
              <div>{{ $address->address_line_1 }}</div>
              <div>{{ $address->address_line_2 }}</div>
              <div>{{ $address->city }}-{{$address->zip}}</div>
              <div>{{$address->state}}</div>
              @if($address->phone != null)
              <div>{{ $address->phone }}</div>
              @endif
            </div>
            @endforeach
            <div class="modal-footer">
            <button type="submit" class="btn primary-button">
                <a>Change</a> 
                </button>
            @else
            <div class="checkout-details-section edit-left">
              You have no Address
            </div>
            @endif
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>


  <div class="modal fade modal-style" id="newaddress" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">New Address</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <form class="newaddress" action="{{ url('/saveaddress') }}" method="post">
              {{ csrf_field() }}
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="FirstName">First Name</label>
                    <input type="text" class="form-control" name="first_name" id="FirstName" value="{{ $user_details[0]->first_name }}" required>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="LastName">Last Name</label>
                    <input type="text" class="form-control"  name="last_name" id="LastName" value="{{ $user_details[0]->last_name }}" required>
                  </div>
                </div>
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="FirstName">Phone</label>
                    <input type="text" class="form-control" name="phone" pattern="\d*" maxlength="10" id="Phone" value="{{ $user_details[0]->phone }}" required>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="Email">Email</label>
                    <input type="text" class="form-control" name="email" id="Email" value="{{ $user_details[0]->email }}" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputAddress">Address</label>                  
                  <input type="text" class="form-control" name="address_line_1" id="address_line_1" placeholder="House/Apartment Number" style="margin-bottom:20px;" required>                  
                  <input type="text" class="form-control" name="address_line_2" id="address_line_2" placeholder="Street Address" required>
                </div>

                <div class="form-group">
                  <label for="inputState">Country</label>
                  <select id="inputState" class="form-control" name="country" required>
                    <option selected>Choose...</option>
                    <option value="India">India</option>
                  </select>
                </div>

                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="inputCity">City</label>
                    <input type="text" class="form-control" name="city" id="inputCity" required>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="inputState">State</label>
                    <select id="inputState" class="form-control" name="state" required>
                      <option selected>Choose...</option>
                      <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                      <option value="Andhra Pradesh">Andhra Pradesh</option>
                      <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                      <option value="Assam">Assam</option>
                      <option value="Bihar">Bihar</option>
                      <option value="Chandigarh">Chandigarh</option>
                      <option value="Chhattisgarh">Chhattisgarh</option>
                      <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                      <option value="Daman and Diu">Daman and Diu</option>
                      <option value="Delhi">Delhi</option>
                      <option value="Goa">Goa</option>
                      <option value="Gujarat">Gujarat</option>
                      <option value="Haryana">Haryana</option>
                      <option value="Himachal Pradesh">Himachal Pradesh</option>
                      <option value="Jammu and Kashmir">Jammu and Kashmir</option>
                      <option value="Jharkhand">Jharkhand</option>
                      <option value="Karnataka">Karnataka</option>
                      <option value="Kerala">Kerala</option>
                      <option value="Lakshadweep">Lakshadweep</option>
                      <option value="Madhya Pradesh">Madhya Pradesh</option>
                      <option value="Maharashtra">Maharashtra</option>
                      <option value="Manipur">Manipur</option>
                      <option value="Meghalaya">Meghalaya</option>
                      <option value="Mizoram">Mizoram</option>
                      <option value="Nagaland">Nagaland</option>
                      <option value="Orissa">Orissa</option>
                      <option value="Pondicherry">Pondicherry</option>
                      <option value="Punjab">Punjab</option>
                      <option value="Rajasthan">Rajasthan</option>
                      <option value="Sikkim">Sikkim</option>                      
                      <option value="Tamil Nadu">Tamil Nadu</option>
                      <option value="Telangana">Telangana</option>
                      <option value="Tripura">Tripura</option>
                      <option value="Uttar Pradesh">Uttar Pradesh</option>
                      <option value="Uttarakhand ">Uttarakhand</option>
                      <option value="West Bengal">West Bengal</option>
                    </select>
                  </div>
                  <div class="form-group col-md-4">
                    <label for="inputZip">Zip</label>
                    <input type="text" class="form-control" pattern="\d*" maxlength="6" name="zip" id="inputZip" required>
                  </div>
                </div>
                <button type="submit" class="btn primary-btn-sm">SAVE</button>
                <button class="btn sec-btn-sm" data-dismiss="modal" aria-label="Close">Cancel</button>
              </form>
      </div>
    </div>
  </div>

 <script src="{{ URL::asset('js/jquery-3.2.1.slim.min.js') }}"></script>
  <script src="{{ URL::asset('js/popper.min.js') }}"></script>
  <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
  <script>
    window.onscroll = function() {
      myFunction()
    };

    var header = document.getElementById("myHeader");
    var sticky = header.offsetTop;

    function myFunction() {
      if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
      } else {
        header.classList.remove("sticky");
      }
    }
    $(".checkbox").change(function() {
      if(!this.checked) {
        $("input:text").not("#email input:text").not(".newaddress input:text").val("");
        $("select").val("");
      } else {
        $("#checkout")[0].reset();
      }
    });
  </script>

</body>

</html>
<footer>
      <div class="container" id="contact" style="padding-top:50px;padding-bottom:30px">
        <div class="footer-section">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 align-self-end">
              <form action={{ url('subscribe') }} method="post">
                {{ csrf_field() }}
                <div class="input-group">
                  <input type="text" class="form-control" name="email" placeholder="Enter email to stay in touch" aria-label="Recipient's username" aria-describedby="basic-addon2" required>
                  <div class="input-group-append">
                    <button class="input-group-text" id="basic-addon2" type="submit">SUBSCRIBE</button>
                  </div>
                </div>
              </form>
              <ul class="list-unstyled social-media">
                <a href="https://www.facebook.com/thenutmakers/?modal=admin_todo_tour" target="_blank"><img src="https://img.icons8.com/color/100/000000/facebook.png" style="width: 7%"></a>
                <!-- <a href="" target="_blank" style= color:#ffffff; font-size: small;>Facebook</a> -->
                <div class="copyright"><p class="pull-right resvered"> <b>2018  Copyright All Rights Reserved. Designed By </b><a href="http://www.shade6.com/" style="color: #b1b0b0;">Shade6.</a></div>
                </ul>
            </div>
            <div class="col col-lg-2 col-md-6 col-sm-6">
              <h3>Help</h3>
              <ul class="list-unstyled list-section">
              <li>{{ $constants[0]->phone }}</li>
                <li>{{ $constants[0]->email }}</li>
                <li><a href="#" data-toggle="modal" data-target="#exampleModal">Enquiry</a></li>
              </ul>
            </div>
            <div class="col col-lg-2 col-md-6 col-sm-6">
              <h3>Your Account</h3>
              <ul class="list-unstyled list-section">
                  <?php
                  if(!Session::has('user_id')){
                    ?>
                    <li><a href="{{ url('signin') }}">Sign in</a></li>
                  <?php
                  }
                  ?>
                {{-- <li>Forgot Password</li> --}}
                <li><a href="{{ url('profile') }}">Order History</a></li>
                <li><a href="{{ url('cart') }}">Shopping Bag</a></li>
              </ul>
            </div>
            <div class="col col-lg-2 col-md-6 col-sm-6">
              <h3>Customer Service</h3>
              <ul class="list-unstyled list-section">
                <li><a href="{{ url('terms') }}">Terms & Conditions</a></li>
                <li> <a href="{{ url('privacy') }}">Privacy Policy</a></li>
                <li><a href="{{ url('refund_policy') }}">Shipping and Returns</a></li>
                <!-- <li><a href="{{ url('disclaimer') }}">Disclaimer</a></li> -->
                <li><a href="{{ url('faqs') }}">Faqs</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </footer>

   <script src="{{ URL::asset('js/jquery.min.js')}}"></script>
   <link rel="stylesheet" href="{{ URL::asset('assets/css/normalize.css')}}">
   <link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap.min.css')}}">
   <link rel="stylesheet" href="{{ URL::asset('assets/css/font-awesome.min.css')}}">
   <link rel="stylesheet" href="{{ URL::asset('assets/css/themify-icons.css')}}">
   <link rel="stylesheet" href="{{ URL::asset('assets/css/flag-icon.min.css')}}">
   <link rel="stylesheet" href="{{ URL::asset('assets/css/cs-skin-elastic.css')}}">
   <link rel="stylesheet" href="{{ URL::asset('assets/css/lib/datatable/dataTables.bootstrap.min.css')}}">
   <link rel="stylesheet" href="{{ URL::asset('assets/scss/style.css')}}">
   <link rel="stylesheet" href="{{ URL::asset('assets/css/lib/vector-map/jqvmap.min.css')}}">
   <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
   <script src="{{ URL::asset('js/print.js') }}" charset="utf-8"></script>  

    <style>
        .amount {
            font-weight: 700;
            margin: 4px;
        }
        .border{
            border-bottom:solid 2px #666;
            border-top:solid 2px #666;
        }
        table, th {
            /* border: 1px solid black; */
            text-align:center;
            padding: 15px 15px;
        }
        table td{
            border-right: 1px solid black;
            border-left: 1px solid black;
            border-top:none !important;
            border-bottom: :none !important;
            padding: 10px;
            font-size: 15px;
        }
        .words{
            border: 1px solid black;
        }
        p {
            font-size: 15px;
        }
    </style>
    <body>   
        <div class="wrapper invoice">
            <section class="invoice">
                <table style="width:98%; margin-left:1%;margin-right:1%">
                    <tr><td colspan="9" style="border: none; padding:0%"><img src="{{ URL::asset('images/logo.png') }}" width="100" height="100"></td></tr>
                    <tr><td colspan="9" style="border: none; padding:0%"><h5>Tax Invoice/Bill of Supply/Cash Memo<br>(Triplicate for Supplier)</h5></td></tr>
                    <tr>
                        <td colspan="4" style="border: none; border-right: none; border-left: none; padding-left:3%; text-align:left">
                            {{-- <h5>R.PRATAP NAIR</h5> --}}
                            <h5>{{ $constants[0]->address }}</h5>
                            <h6>Mobile Number: {{ $constants[0]->phone }}</h6>
                            <h6>E-mail: {{ $constants[0]->email }}</h6>
                        </td>
                        <td colspan="1" style="border-right: none; border-left: none;"></td>
                        <td colspan="4" style="border:none; text-align:left">
                            <h6>GST NO: 33ABFPN4423L1ZO</h6>
                            {{-- <h6>PAN NO: ABFPN4423L</h6>
                            <h6>TIN UNDER VAT: 33666180520</h6> --}}
                            <br>
                            <h6>Today's Date: {{ date('d-m-Y') }}</h6>
                        </td>
                    </tr>
                    @php
                        $delivery_state = null;
                    @endphp
                    <tr>
                        <td colspan="4" style="border: none; padding-left:3%; text-align:left">
                            <h5>Billing Address</h5>
                            @foreach($billing_address as $add)
                                @if( $orders[0]->billing_address == $add->address_id )
                                    <h6>{{ $add->address_fname }} {{ $add->address_lname }}</h6>
                                    <h6>{{ $add->address_line_1 }},</h6>
                                    <h6>{{ $add->address_line_2 }},</h6>
                                    <h6>{{ $add->city}},</h6>
                                    <h6>{{ $add->state }}-{{ $add->zip }}.</h6>
                                @break
                                @endif
                            @endforeach
                        </td>
                        <td colspan="1" style="border-right: none; border-left: none;"></td>
                        <td colspan="4" style="border: none;text-align:left">
                            <h5>Shipping Address</h5>
                            @foreach($delivery_address as $add)
                                @if( $orders[0]->delivery_address == $add->address_id )
                                    @php
                                        $delivery_state = $add->state
                                    @endphp
                                    <h6>{{ $add->address_fname }} {{ $add->address_lname }}</h6>
                                    <h6>{{ $add->address_line_1 }},</h6>
                                    <h6>{{ $add->address_line_2 }},</h6>
                                    <h6>{{ $add->city}},</h6>
                                    <h6>{{ $add->state }}-{{ $add->zip }}.</h6>
                                @break
                                @endif
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="border: none; padding-left:3%; text-align:left">
                            <h4>Order Details</h5>
                            <label>User Name :</label> {{$orders[0]->first_name}} <br>
                            <label>Order ID :</label> {{$orders[0]->order_id}} <br>
                            <label>Order Date:</label>
                            @php
                                $hijri= date('d-m-Y  h:i A', strtotime($orders[0]->order_time));
                            @endphp
                            {{$hijri}}<br>
                            <label>Payment Method:</label> {{$orders[0]->payment_mode}} <br>
                        </td>
                        <td colspan="1" style="border-right: none; border-left: none;"></td>
                        <td colspan="4" style="border: none; padding-bottom:5%; padding-top:0%; text-align:left">
                                <h5>Order Status</h5>
                                @if ($orders[0]->order_status==0)
                                    <div class="label cancel"><b>Cancelled On:
                                                @php
                                                $hijri= date('d-m-Y  h:i A', strtotime($orders[0]->cancelled_time));
                                                @endphp
                                                {{$hijri}}</b></div>
                                @elseif ($orders[0]->order_status==1)
                                    <div class="label placed"><b>Placed On:
                                                @php
                                                $hijri= date('d-m-Y  h:i A', strtotime($orders[0]->order_time));
                                                @endphp
                                                {{$hijri}}</b></div>
                                @elseif ($orders[0]->order_status==2)
                                    <div class="label confirm"><b>Confirmed On:                                            @php
                                                $hijri= date('d-m-Y  h:i A', strtotime($orders[0]->processed_time));
                                                @endphp
                                            {{$hijri}}</b></div>
                                @elseif ($orders[0]->order_status==3)
                                    <div class="label dispatched"><b>Dispatched On:
                                                @php
                                                $hijri= date('d-m-Y  h:i A', strtotime($orders[0]->pshipped_time));
                                                @endphp
                                            {{$hijri}}</b></div>
                                @elseif ($orders[0]->order_status==4)
                                    <div class="label delivered"><b>Delivered On:                                            @php
                                                $hijri= date('d-m-Y  h:i A', strtotime($orders[0]->delivery_time));
                                                @endphp
                                                {{$hijri}} </b></div>
                                @elseif ($orders[0]->order_status==5)
                                    <div class="label returned">Returned</div>
                                @endif 
                        </td>
                    </tr>
                    <tr>
                    <th class="words">S.No</th>
                    <th class="words">Description</th>
                    <th class="words">Unit Price</th>
                    <th class="words">Qty</th>
                    <th class="words">Net Amount</th>
                    <th class="words">Tax Rate</th>
                    <th class="words">Tax type</th>
                    <th class="words">Tax amount</th>
                    <th class="words">Total Amount</th>
                    </tr>
                    @php
                    $total_product_tax=0;
                    $tax_total=0;
                    @endphp
                        <?php $sno=1; $total_amt=0;?>
                        @for ($i=0; $i < count($orders) ; $i++)
                        @php
                        $net_amt = 0;
                        $tax_rate = $constants[0]->igst_percent;
                        $tax_type = ["CGST", "SGST", "IGST"];
                        $delivery_cost = $orders[0]->delivery_cost;
                        $delivery_tax = 0;
                        $tax_amount = 0;
                        $sub_total = 0;
                        $igst = $constants[0]->igst_percent/100;
                        $total = 0;
                        @endphp
                        <tr>
                        <?php 
                            //  echo "<pre>"; print_r($orders);
                        ?>
                        <td><?php echo $sno; ?></td>
                            <td>{{ $orders[$i]->product_name }} ( {{ $orders[$i]->variety }} )</td>
                        <td>Rs. {{ $orders[$i]->price - ($orders[$i]->igst_product/$orders[$i]->quantity) }}</td>
                        <td>{{ $orders[$i]->quantity }}</td>
                        @php
                        $net_amt = $orders[$i]->order_price - $orders[$i]->igst_product;
                        @endphp
                        <td>Rs. {{ $orders[$i]->order_price - $orders[$i]->igst_product }}</td>
                        @php
                            if($delivery_state != $constants[0]->state) {
                                if($orders[$i]->product_id == 31) {
                                    $dummy_tax = 5;
                                    echo "<td>$dummy_tax%</td>";
                                    echo "<td>$tax_type[2]</td>";
                                } else {
                                    $dummy_tax = $tax_rate;
                                    echo "<td>$dummy_tax%</td>";
                                    echo "<td>$tax_type[2]</td>";
                                }
                            } else {
                                if($orders[$i]->product_id == 31) {
                                    $dummy_tax = 5 / 2;
                                    echo "<td>$dummy_tax%, $dummy_tax%</td>";
                                    echo "<td>$tax_type[0] &amp; $tax_type[1]</td>";
                                } else {
                                    $dummy_tax = $tax_rate / 2;
                                    echo "<td>$dummy_tax%, $dummy_tax%</td>";
                                    echo "<td>$tax_type[0] &amp; $tax_type[1]</td>";
                                }
                            }
                        @endphp
                        @php
                            $tax_amount = round($igst*$orders[$i]->order_price,2);
                        @endphp
                            <td>Rs. {{ $orders[$i]->igst_product }}</td>
                        @php
                            $total_product_tax += $orders[$i]->igst_product;
                            $sub_total= round($orders[$i]->order_price - $orders[$i]->igst_product,2);
                        @endphp
                            <td>Rs. {{ $orders[$i]->order_price }}</td>
                        </tr>
                        <?php 
                            $sno++;
                            $total_amt += $sub_total;
                        ?>
                        @endfor
                        <tr>
                        <td></td>
                        <td>Shipping Cost</td>
                        <td>Rs. {{ $delivery_cost }}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        @php
                        $shipping_amount = $delivery_cost
                        @endphp
                        <td>Rs. {{ $shipping_amount }}</td>
                        </tr>

                        <tr class="words">                    
                            <td colspan="7" style="text-align:center;padding-right:5px;">
                            <ul>                  
                                <li class="amount">Grand Total------->(Rounded Off)</li>
                            </ul>
                            </td>
                            @php
                            $tax_total = round($total_product_tax + $delivery_tax);
                            @endphp
                            <td>Rs. {{ $tax_total }}</td>
                            <td class="amount" style="text-align:center;padding-right:5px;">
                                <?php
                                        $total_amount = $total_amt + $shipping_amount + $tax_total;
                                    ?>
                                    Rs. {{ number_format($total_amount) }}</li>
                            </td>
                        </tr>

                    <tr class="words">
                    <td colspan="3" style="text-align:center;padding-right:5px;"><h6>Total amount in words</h6></td>
                        <td colspan="6" style="text-align:center;padding-right:5px;">
                    <?php
                        function convert_number_to_words($number) {
                            $hyphen      = '-';
                            $conjunction = ' and ';
                            $separator   = ' ';
                            $negative    = 'negative ';
                            $decimal     = ' point ';
                            $dictionary  = array(
                                0                   => 'zero',
                                1                   => 'one',
                                2                   => 'two',
                                3                   => 'three',
                                4                   => 'four',
                                5                   => 'five',
                                6                   => 'six',
                                7                   => 'seven',
                                8                   => 'eight',
                                9                   => 'nine',
                                10                  => 'ten',
                                11                  => 'eleven',
                                12                  => 'twelve',
                                13                  => 'thirteen',
                                14                  => 'fourteen',
                                15                  => 'fifteen',
                                16                  => 'sixteen',
                                17                  => 'seventeen',
                                18                  => 'eighteen',
                                19                  => 'nineteen',
                                20                  => 'twenty',
                                30                  => 'thirty',
                                40                  => 'fourty',
                                50                  => 'fifty',
                                60                  => 'sixty',
                                70                  => 'seventy',
                                80                  => 'eighty',
                                90                  => 'ninety',
                                100                 => 'hundred',
                                1000                => 'thousand',
                                100000              => 'lakh'
                            );
                            if (!is_numeric($number)) {
                                return false;
                            }
                            if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
                                // overflow
                                trigger_error(
                                    'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
                                    E_USER_WARNING
                                );
                                return false;
                            }
                            if ($number < 0) {
                                return $negative . convert_number_to_words(abs($number));
                            }
                            $string = $fraction = null;
                            if (strpos($number, '.') !== false) {
                                list($number, $fraction) = explode('.', $number);
                            }
                            switch (true) {
                                case $number < 21:
                                    $string = $dictionary[$number];
                                    break;
                                case $number < 100:
                                    $tens   = ((int) ($number / 10)) * 10;
                                    $units  = $number % 10;
                                    $string = $dictionary[$tens];
                                    if ($units) {
                                        $string .= $hyphen . $dictionary[$units];
                                    }
                                    break;
                                case $number < 1000:
                                    $hundreds  = $number / 100;
                                    $remainder = $number % 100;
                                    $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                                    if ($remainder) {
                                        $string .= $conjunction . convert_number_to_words($remainder);
                                    }
                                    break;
                                default:
                                    $baseUnit = pow(1000, floor(log($number, 1000)));
                                    $numBaseUnits = (int) ($number / $baseUnit);
                                    $remainder = $number % $baseUnit;
                                    $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                                    if ($remainder) {
                                        $string .= $remainder < 100 ? $conjunction : $separator;
                                        $string .= convert_number_to_words($remainder);
                                    }
                                    break;
                            }
                            if (null !== $fraction && is_numeric($fraction)) {
                                $string .= $decimal;
                                $words = array();
                                foreach (str_split((string) $fraction) as $number) {
                                    $words[] = $dictionary[$number];
                                }
                                $string .= implode(' ', $words);
                            }
                            return ucfirst($string);
                        }
                    ?>
                    <h6>{{ convert_number_to_words(round($total_amount)) }} only</h6>
                    </td>
                    </tr>
                </table>
            </section>
        </div>
    </body>
    <script type="text/javascript">
        $("html").printThis({
        importCSS: true,
        importStyle: true,
        pageTitle: "The Nut Makers Invoice",
        /* header: "The Nut Makers",
        footer: "The Nut Makers",*/
        });
    </script>

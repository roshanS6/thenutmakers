<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/testsms','Controller@testsms');
Route::get('/test','CartController@test');

Route::get('/','Controller@home');
Route::get('/terms','Controller@terms');
Route::get('/privacy','Controller@privacy');
Route::get('/disclaimer','Controller@disclaimer');
Route::get('/faqs','Controller@faqs');
Route::get('/refund_policy','Controller@refundpolicy');
Route::post('/enquiry','Controller@enquiry');
Route::post('/subscribe','Controller@subscribe');
Route::get('/search','Controller@search');

Route::get('/product','Controller@product');
Route::get('/aboutus','Controller@aboutus');
Route::get('/contactus','Controller@contactus');
Route::get('/product/{category_name}', 'Controller@category');
Route::post('/saveaddress','Controller@saveaddress');



Route::get('/cart','CartController@viewcart');
Route::get('/cart/increasequantity/{value}/{id}','CartController@increaseCartQuantity');
Route::get('/cart/increasequantity/{value}/{id}/{variety}','CartController@increaseCartVarietyQuantity');
Route::get('/cart/decreasequantity/{value}/{id}','CartController@decreaseCartQuantity');
Route::get('/cart/decreasequantity/{value}/{id}/{variety}','CartController@decreaseCartVarietyQuantity');
Route::get('/addtocart/{id}','CartController@addProductToCart');
Route::GET('/removefromcart/{id}',  'CartController@deleteCart');
Route::GET('/removefromcart/{id}/{variety}',  'CartController@deleteCartVariety');
Route::get('/placedorders', 'CartController@placedorders');
Route::get('/checkout', 'CartController@checkout');
Route::post('/paywithrazorpay', 'CartController@payWithRazorpay');
Route::post('/payment', 'CartController@payment');
Route::post('/checkout', 'CartController@checkout');
Route::get('/refreshcart/{id}', 'CartController@refreshCart');

Route::post('/ordercheckout', 'CartController@ordercheckout');
Route::post('/changeaddress','CartController@changedeliveryaddress');

Route::get('/product/{id}','Controller@product');
Route::get('/contact','Controller@contact');
Route::GET('/orderdetails/{id}', 'Controller@orderInvoice');
Route::GET('orderinvoice/{id}', 'Controller@invoice');
// Route::get('/signup', 'Controller@signup');
Route::post('/register', 'Controller@register'); 
Route::get('/signin', 'Controller@signin');
Route::post('/login', 'Controller@login');
Route::post('/generateotp', 'Controller@generateotp');
Route::get('/resendotp/{value}', 'Controller@resendotp');
Route::post('/verifyotp', 'Controller@changepassword');
Route::get('/logout', 'Controller@logout');

Route::get('/profile', 'Controller@profile');
// Route::get('/product_details','Controller@product_details');
Route::get('/product_details/{id}','Controller@product_details');
Route::get('/track_order/{id}','Controller@trackOrder');

Route::get('/products', 'ProductController@products'); 
Route::get('/getallproducts', 'ProductController@getProducts');

Route::GET('/admin', 'AdminUserController@LoginPage');
Route::GET('/admin/login', 'AdminUserController@LoginPage');
Route::GET('/admin/processlogin', 'AdminUserController@processlogin');
Route::GET('/admin/logout', 'AdminUserController@logout');

Route::GET('/admin/dashboard', 'AdminController@MainPage');

Route::GET('/admin/product/{id}', 'AdminProductController@getProduct');
Route::GET('/admin/products', 'AdminProductController@products');
Route::GET('/admin/product/updatefeatures/{value}/{id}', 'AdminProductController@updateFeatures');
Route::GET('/admin/products/add', 'AdminProductController@addProduct');
Route::POST('/admin/products/insert', 'AdminProductController@insertProduct');
Route::POST('/admin/products/update', 'AdminProductController@updateProduct');

Route::POST('admin/products/insert_variety', 'AdminProductController@insertProductVariety');
Route::POST('admin/products/update_variety/{id}', 'AdminProductController@updateProductVariety');
Route::get('/admin/products/delete/{id}', 'AdminProductController@deleteProduct');
Route::get('/admin/products/delete_variety/{id}', 'AdminProductController@deleteProductVariety');
Route::get('admin/product/delete/{id}/{name}', 'AdminProductController@deleteProductImage');

Route::GET('/admin/maincategory', 'AdminController@categories');
Route::GET('/admin/maincategory/add', 'AdminController@addmaincategory');
Route::POST('/admin/maincategory/add', 'AdminController@insertmaincategory');
Route::get('admin/maincategory/{id}', 'AdminController@getCategory');
Route::POST('admin/maincategory/{id}','AdminController@updateCategory');
Route::get('admin/maincategory/delete/{id}', 'AdminController@deleteCategory');

Route::GET('/admin/user', 'AdminController@viewUser');
Route::GET('/admin/subscribers', 'AdminController@viewSubscribers');
Route::GET('/admin/user/{id}', 'AdminController@userDetails');
Route::POST('/admin/user/insert', 'AdminController@insertUser');

Route::GET('/admin/allorders','AdminOrderController@allOrders');
Route::GET('/admin/orderdetails/{id}', 'AdminOrderController@orderInvoice');
Route::GET('admin/orderinvoice/{id}', 'AdminOrderController@invoice');
Route::GET('/admin/constants','AdminController@constant');
Route::POST('/admin/updateconstants/{id}','AdminController@updateConstant');
Route::POST('/admin/insertconstants','AdminController@insertConstant');


Route::GET('/admin/placedorders','AdminOrderController@placedOrders');

Route::GET('/admin/confirmedorders','AdminOrderController@confirmedOrders');
Route::GET('/admin/dispatchedorders','AdminOrderController@dispatchedOrders');
Route::GET('/admin/cancelledorders','AdminOrderController@cancelledOrders');
Route::GET('/admin/deliveredorders','AdminOrderController@deliveredOrders'); 
Route::GET('/admin/returnedorders','AdminOrderController@returnedOrders');
Route::GET('/admin/failedorders','AdminOrderController@failedOrders');

Route::GET('/admin/allorders/cancel/{value}/{id}/{reason}','AdminOrderController@cancelOrder');
Route::GET('/admin/allorders/update/{value}/{id}','AdminOrderController@updateOrderStatus');

Route::GET('/admin/aboutus','AdminController@aboutUs');

Route::GET('/admin/add_banner','AdminController@add_banner');
Route::POST('admin/banner/insert','AdminController@insert_banner');
Route::GET('/admin/banners','AdminController@banners');
Route::GET('admin/banner/get/{id}','AdminController@get_banner');
Route::POST('admin/banner/update','AdminController@update_banner');
Route::GET('admin/banner/delete/{id}','AdminController@delete_banner');

Route::GET('/admin/enquiries','AdminController@enquiries');
Route::GET('/admin/enquiry/update/{value}/{id}','AdminController@updateEnquiryStatus');
Route::POST('/admin/updateabout/{id}','AdminController@updateAboutUs');

Route::GET('/admin/testimonial','AdminController@viewtestimonial');
Route::GET('/admin/addtestimonial','AdminController@addTestimonial');
Route::POST('/admin/addtestimonial','AdminController@insertTestimonial');
Route::GET('/admin/updatetestimonial/{value}/{id}','AdminController@updateTestimonial');
/*Route::get('/admin/aboutus', 'AdminController@aboutUs');
Route::POST('/admin/aboutus/{id}', 'AdminController@updateAboutUs');*/

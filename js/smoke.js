
$(function() {
  "use strict";
  var a = 0;
  $('#tv').hide();
  for (; a < 50; a += 1) {
    setTimeout(function b() {
      var a = Math.random() * 1e3 + 5e3,
        c = $("<div />", {
          "class": "smoke",
          css: {
            right: Math.random() * 800,
            backgroundSize: "contain",
            width: Math.random() * 800,
            height: Math.random() * 600
          }
        });
      $(c).appendTo("#viewport");
      $.when($(c).animate({}, {
          duration: a / 6,
          easing: "linear",
          queue: false,
          complete: function() {
            $(c).animate({}, {
              duration: a / 6,
              easing: "linear",
              queue: false
            })
          }
        }),
        $(c).animate({
          left: $("#viewport").height()
        }, {
          duration: a,
          easing: "linear",
          queue: false
        })).then(
        function() {
          $(c).remove();
          b()
        })
    }, Math.random() * 33)
  }
}())

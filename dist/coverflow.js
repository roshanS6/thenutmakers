/**
*
*  CoverFlow using CSS3
*
*  @author: Hjörtur Elvar Hilmarsson
*
**/
(function() {

    // Local variables
    var _index = 0,
    _coverflow = null,
    _prevLink = null,
    _nextLink = null,
    _albums = [],
    _transformName = Modernizr.prefixed('transform'),

    // Constants
    OFFSET = 30; // pixels
    ROTATION = 0; // degrees
    BASE_ZINDEX = 10; //
    MAX_ZINDEX = 42; //

    /**
     * Get selector from the dom
     **/
    function get( selector ) {
        return document.querySelector( selector );
    };

    /**
     * Renders the CoverFlow based on the current _index
     **/
    function render() {

        // loop through albums & transform positions
        for( var i = 0; i < _albums.length; i++ ) {

            // before
            if( i === _index-2 ) {
                _albums[i].style[_transformName] = "rotateY( "+ ROTATION +"deg ) translateX( -"+ ( OFFSET * ( _index - i  ) ) +"% ) translateZ( 210px )";
                _albums[i].style.zIndex = BASE_ZINDEX + i;
                _albums[i].style.opacity = 0.5;
                _albums[i].style.boxShadow = "rgba(0,100,100,0.2) 0px 0px 0px 2000px inset";
            }
            if( i === _index-1 ) {
                _albums[i].style[_transformName] = "rotateY( "+ ROTATION +"deg ) translateX( -"+ ( OFFSET * ( _index - i  ) ) +"% ) translateZ( 270px )";
                _albums[i].style.zIndex = BASE_ZINDEX + i;
                _albums[i].style.opacity = 0.75;
                _albums[i].style.boxShadow = "rgba(0,100,100,0.2) 0px 0px 0px 2000px inset";
            }

            // current
             if( i === _index ) {
                _albums[i].style[_transformName] = "rotateY( 0deg ) translateZ( 330px )";
                _albums[i].style.zIndex = MAX_ZINDEX;
                _albums[i].style.opacity = 1;
                _albums[i].style.boxShadow = "rgba(0,100,100,0.2) 0px 0px 0px 2000px inset";

            }

             // after
            if( i === _index+1 ) {
                _albums[i].style[_transformName] = "rotateY( -"+ ROTATION +"deg ) translateX( "+ ( OFFSET * ( i - _index  ) ) +"% ) translateZ( 270px )";
                _albums[i].style.zIndex = BASE_ZINDEX + ( _albums.length - i  );
                _albums[i].style.opacity = 0.75;
                _albums[i].style.boxShadow = "rgba(0,100,100,0.2) 0px 0px 0px 2000px inset";
            }
            if( i === _index+2 ) {
                _albums[i].style[_transformName] = "rotateY( -"+ ROTATION +"deg ) translateX( "+ ( OFFSET * ( i - _index  ) ) +"% ) translateZ( 210px )";
                _albums[i].style.zIndex = BASE_ZINDEX + ( _albums.length - i  );
                _albums[i].style.opacity = 0.5;
                _albums[i].style.boxShadow = "rgba(0,100,100,0.2) 0px 0px 0px 2000px inset";
            }

        }

    };

    /**
     * Flow to the right
     **/
    function flowRight() {

       // check if has albums
       // on the right side
       if( _index ) {
            _index--;
            render();
       }

    };

    /**
     * Flow to the left
     **/
    function flowLeft() {

        // check if has albums
       // on the left side
       if( _albums.length > ( _index + 1)  ) {
            _index++;
            render();
       }

    };

    /**
     * Enable left & right keyboard events
     **/
    function keyDown( event ) {

        switch( event.keyCode ) {
            case 37: flowRight(); break; // left
            case 39: flowLeft(); break; // right
        }

    };

    /**
     * Register all events
     **/
    function registerEvents() {
        _prevLink.addEventListener('click', flowRight, false);
        _nextLink.addEventListener('click', flowLeft, false);
        document.addEventListener('keydown', keyDown, false);
    };

    /**
     * Initalize
     **/
    function init() {

        // get albums & set index on the album in the middle
        _albums = Array.prototype.slice.call( document.querySelectorAll( 'section' ));
        _index = Math.floor( _albums.length / 2 );

        // get dom stuff
        _coverflow = get('#coverflow');
        _prevLink = get('#nextPro');
        _nextLink = get('#prevPro');

        // display covers
        for( var i = 0; i < _albums.length; i++ ) {
            var url = _albums[i].getAttribute("data-cover");
            _albums[i].style.backgroundImage = "url("+ url  +")";
        }

        // do important stuff
        registerEvents();
        render();

   };

    // go!
    init();

}());
